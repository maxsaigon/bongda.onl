<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nmthanh
 * Date: 7/25/13
 * Time: 11:27 AM
 * To change this template use File | Settings | File Templates.
 */

// Set configurations based on environment
if( isset( $_SERVER['APPLICATION_ENV'] ) )
{
    $env = $_SERVER['APPLICATION_ENV']; // development or production
}else{
    // Set environment variable
    $env = 'production';
}

// Change the following paths if necessary
require_once(dirname(__FILE__).'/protected/config/environment.php');
switch($env){
    case 'development':
        //For development
        $environment = new Environment(Environment::DEVELOPMENT);
        break;
    case 'test':
        //For test
        $environment = new Environment(Environment::TEST);
        break;
    case 'stage':
        //For stage
        $environment = new Environment(Environment::STAGE);
        break;
    default:
        //For production
        $environment = new Environment(Environment::PRODUCTION);
        break;
}

// change the following paths if necessary
$yii=dirname(__FILE__).'/library/yii/framework/yii.php';

defined('YII_DEBUG') or define('YII_DEBUG',$environment->getDebug());
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', $environment->getTraceLevel());


require_once($yii);
Yii::createWebApplication($environment->getConfig())->run();

