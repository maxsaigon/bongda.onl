<?php
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set("Etc/UTC");
// Set environment variable
$env = 'production';
// Set configurations based on environment
if( isset( $_SERVER['APPLICATION_ENV'] ) )
{
    $env = $_SERVER['APPLICATION_ENV']; // development or production
}

// Change the following paths if necessary
require_once(dirname(__FILE__).'/protected/config/environment.php');
switch($env){
    case 'development1':
        //For development
        $environment = new Environment(Environment::DEVELOPMENT1);
        break;
    case 'development2':
        //For development
        $environment = new Environment(Environment::DEVELOPMENT2);
        break;
    case 'test':
        //For test
        $environment = new Environment(Environment::TEST);
        break;
    case 'stage':
        //For stage
        $environment = new Environment(Environment::STAGE);
        break;
    default:
        //For production
        $environment = new Environment(Environment::PRODUCTION);
        break;
}

// change the following paths if necessary
$yii=dirname(__FILE__).'/library/yii/framework/yii.php';

defined('YII_DEBUG') or define('YII_DEBUG',$environment->getDebug());
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', $environment->getTraceLevel());


require_once($yii);
Yii::createWebApplication($environment->getConfig())->run();

