<?php

/**
 * This is the model class for table "comment_like".
 *
 * The followings are the available columns in table 'comment_like':
 * @property string $like_id
 * @property integer $user_id
 * @property string $comment_id
 * @property integer $status
 * @property string $like_created
 */
class BaseCommentLike extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BaseCommentLike the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comment_like';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, comment_id, status', 'required'),
			array('user_id, status', 'numerical', 'integerOnly'=>true),
			array('comment_id', 'length', 'max'=>20),
			array('like_created', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('like_id, user_id, comment_id, status, like_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'like_id' => 'Like',
			'user_id' => 'User',
			'comment_id' => 'Comment',
			'status' => 'Status',
			'like_created' => 'Like Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('like_id',$this->like_id,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('comment_id',$this->comment_id,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('like_created',$this->like_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}