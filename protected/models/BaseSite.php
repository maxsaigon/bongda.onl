<?php

/**
 * This is the model class for table "site".
 *
 * The followings are the available columns in table 'site':
 * @property string $Id
 * @property string $SiteName
 * @property string $Url
 * @property integer $IsActived
 * @property integer $Interval
 * @property string $url_class
 * @property string $content_class
 * @property string $tag_class
 * @property string $title_class
 * @property string $title_xpath
 * @property string $content_xpath
 * @property string $tag_xpath
 * @property string $url_except
 * @property string $description_xpath
 * @property string $description_class
 */
class BaseSite extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BaseSite the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'site';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('SiteName, Url, IsActived, Interval, url_class, content_class, tag_class, title_class, title_xpath, content_xpath, tag_xpath, url_except, description_xpath, description_class', 'required'),
			array('IsActived, Interval', 'numerical', 'integerOnly'=>true),
			array('SiteName, Url', 'length', 'max'=>100),
			array('url_class, content_class, tag_class, title_class, title_xpath, content_xpath, tag_xpath, url_except, description_xpath, description_class', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('Id, SiteName, Url, IsActived, Interval, url_class, content_class, tag_class, title_class, title_xpath, content_xpath, tag_xpath, url_except, description_xpath, description_class', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'SiteName' => 'Site Name',
			'Url' => 'Url',
			'IsActived' => 'Is Actived',
			'Interval' => 'Interval',
			'url_class' => 'Url Class',
			'content_class' => 'Content Class',
			'tag_class' => 'Tag Class',
			'title_class' => 'Title Class',
			'title_xpath' => 'Title Xpath',
			'content_xpath' => 'Content Xpath',
			'tag_xpath' => 'Tag Xpath',
			'url_except' => 'Url Except',
			'description_xpath' => 'Description Xpath',
			'description_class' => 'Description Class',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id,true);
		$criteria->compare('SiteName',$this->SiteName,true);
		$criteria->compare('Url',$this->Url,true);
		$criteria->compare('IsActived',$this->IsActived);
		$criteria->compare('Interval',$this->Interval);
		$criteria->compare('url_class',$this->url_class,true);
		$criteria->compare('content_class',$this->content_class,true);
		$criteria->compare('tag_class',$this->tag_class,true);
		$criteria->compare('title_class',$this->title_class,true);
		$criteria->compare('title_xpath',$this->title_xpath,true);
		$criteria->compare('content_xpath',$this->content_xpath,true);
		$criteria->compare('tag_xpath',$this->tag_xpath,true);
		$criteria->compare('url_except',$this->url_except,true);
		$criteria->compare('description_xpath',$this->description_xpath,true);
		$criteria->compare('description_class',$this->description_class,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}