<?php

/**
 * This is the model class for table "tag".
 *
 * The followings are the available columns in table 'tag':
 * @property string $id
 * @property string $Name
 * @property string $Tag
 * @property string $tag_image
 * @property integer $tag_order
 * @property integer $tag_level
 */
class BaseTag extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BaseTag the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tag';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Name, Tag', 'required'),
			array('tag_order, tag_level', 'numerical', 'integerOnly'=>true),
			array('Name, Tag', 'length', 'max'=>100),
			array('tag_image', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Name, Tag, tag_image, tag_order, tag_level', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Name' => 'Name',
			'Tag' => 'Tag',
			'tag_image' => 'Tag Image',
			'tag_order' => 'Tag Order',
			'tag_level' => 'Tag Level',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Tag',$this->Tag,true);
		$criteria->compare('tag_image',$this->tag_image,true);
		$criteria->compare('tag_order',$this->tag_order);
		$criteria->compare('tag_level',$this->tag_level);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}