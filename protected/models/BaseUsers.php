<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $user_id
 * @property string $password
 * @property string $password_salt
 * @property string $provider
 * @property string $object_sha
 * @property string $identifier
 * @property string $profileurl
 * @property string $websiteurl
 * @property string $photourl
 * @property string $displayname
 * @property string $description
 * @property string $firstname
 * @property string $lastname
 * @property string $gender
 * @property string $language
 * @property string $age
 * @property string $birthday
 * @property string $birthmonth
 * @property string $birthyear
 * @property string $email
 * @property string $emailverified
 * @property string $phone
 * @property string $address
 * @property string $country
 * @property string $region
 * @property string $city
 * @property string $zip
 * @property string $user_role
 * @property integer $rememberMe
 */
class BaseUsers extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BaseUsers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('password, password_salt, provider, object_sha, identifier, profileurl, websiteurl, photourl, displayname, description, firstname, lastname, gender, language, age, birthday, birthmonth, birthyear, email, emailverified, phone, address, country, region, city, zip', 'required'),
			array('rememberMe', 'numerical', 'integerOnly'=>true),
			array('password, password_salt, provider, object_sha, identifier, profileurl, websiteurl, photourl, displayname, description, firstname, lastname, gender, language, age, birthday, birthmonth, birthyear, email, emailverified, phone, address, country, region, city, zip', 'length', 'max'=>255),
			array('user_role', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('user_id, password, password_salt, provider, object_sha, identifier, profileurl, websiteurl, photourl, displayname, description, firstname, lastname, gender, language, age, birthday, birthmonth, birthyear, email, emailverified, phone, address, country, region, city, zip, user_role, rememberMe', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'password' => 'Password',
			'password_salt' => 'Password Salt',
			'provider' => 'Provider',
			'object_sha' => 'Object Sha',
			'identifier' => 'Identifier',
			'profileurl' => 'Profileurl',
			'websiteurl' => 'Websiteurl',
			'photourl' => 'Photourl',
			'displayname' => 'Displayname',
			'description' => 'Description',
			'firstname' => 'Firstname',
			'lastname' => 'Lastname',
			'gender' => 'Gender',
			'language' => 'Language',
			'age' => 'Age',
			'birthday' => 'Birthday',
			'birthmonth' => 'Birthmonth',
			'birthyear' => 'Birthyear',
			'email' => 'Email',
			'emailverified' => 'Emailverified',
			'phone' => 'Phone',
			'address' => 'Address',
			'country' => 'Country',
			'region' => 'Region',
			'city' => 'City',
			'zip' => 'Zip',
			'user_role' => 'User Role',
			'rememberMe' => 'Remember Me',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('password_salt',$this->password_salt,true);
		$criteria->compare('provider',$this->provider,true);
		$criteria->compare('object_sha',$this->object_sha,true);
		$criteria->compare('identifier',$this->identifier,true);
		$criteria->compare('profileurl',$this->profileurl,true);
		$criteria->compare('websiteurl',$this->websiteurl,true);
		$criteria->compare('photourl',$this->photourl,true);
		$criteria->compare('displayname',$this->displayname,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('age',$this->age,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('birthmonth',$this->birthmonth,true);
		$criteria->compare('birthyear',$this->birthyear,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('emailverified',$this->emailverified,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('region',$this->region,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('user_role',$this->user_role,true);
		$criteria->compare('rememberMe',$this->rememberMe);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}