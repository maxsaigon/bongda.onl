<?php

/**
 * This is the model class for table "url".
 *
 * The followings are the available columns in table 'url':
 * @property integer $id
 * @property integer $category_id
 * @property string $tile
 * @property string $type
 * @property string $content
 * @property string $feature_image
 * @property string $created
 * @property integer $is_hot
 * @property integer $is_reviewed
 * @property string $url
 * @property integer $status
 * @property string $description
 * @property string $slug
 * @property integer $is_deleted
 */
class BaseUrl extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BaseUrl the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'url';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, tile, content, feature_image, created, url, status, description, slug', 'required'),
			array('category_id, is_hot, is_reviewed, status, is_deleted', 'numerical', 'integerOnly'=>true),
			array('tile, type', 'length', 'max'=>500),
			array('feature_image, url, description, slug', 'length', 'max'=>255),
			array('created', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, category_id, tile, type, content, feature_image, created, is_hot, is_reviewed, url, status, description, slug, is_deleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
			'tile' => 'Tile',
			'type' => 'Type',
			'content' => 'Content',
			'feature_image' => 'Feature Image',
			'created' => 'Created',
			'is_hot' => 'Is Hot',
			'is_reviewed' => 'Is Reviewed',
			'url' => 'Url',
			'status' => 'Status',
			'description' => 'Description',
			'slug' => 'Slug',
			'is_deleted' => 'Is Deleted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('tile',$this->tile,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('feature_image',$this->feature_image,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('is_hot',$this->is_hot);
		$criteria->compare('is_reviewed',$this->is_reviewed);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('is_deleted',$this->is_deleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}