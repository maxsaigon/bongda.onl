<?php

/**
 * This is the model class for table "comments".
 *
 * The followings are the available columns in table 'comments':
 * @property string $comment_id
 * @property integer $comment_parent
 * @property string $comment_content
 * @property integer $comment_user_id
 * @property integer $comment_like
 * @property integer $comment_dislike
 * @property string $comment_created
 * @property string $comment_updated
 */
class BaseComments extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BaseComments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('comment_parent, comment_content, comment_user_id, comment_like, comment_dislike', 'required'),
			array('comment_parent, comment_user_id, comment_like, comment_dislike', 'numerical', 'integerOnly'=>true),
			array('comment_content', 'length', 'max'=>500),
			array('comment_created, comment_updated', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('comment_id, comment_parent, comment_content, comment_user_id, comment_like, comment_dislike, comment_created, comment_updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'comment_id' => 'Comment',
			'comment_parent' => 'Comment Parent',
			'comment_content' => 'Comment Content',
			'comment_user_id' => 'Comment User',
			'comment_like' => 'Comment Like',
			'comment_dislike' => 'Comment Dislike',
			'comment_created' => 'Comment Created',
			'comment_updated' => 'Comment Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('comment_id',$this->comment_id,true);
		$criteria->compare('comment_parent',$this->comment_parent);
		$criteria->compare('comment_content',$this->comment_content,true);
		$criteria->compare('comment_user_id',$this->comment_user_id);
		$criteria->compare('comment_like',$this->comment_like);
		$criteria->compare('comment_dislike',$this->comment_dislike);
		$criteria->compare('comment_created',$this->comment_created,true);
		$criteria->compare('comment_updated',$this->comment_updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}