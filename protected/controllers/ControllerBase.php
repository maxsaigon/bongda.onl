<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nmthanh
 * Date: 7/31/13
 * Time: 11:15 AM
 * To change this template use File | Settings | File Templates.
 */
class ControllerBase extends Controller
{
    public $cs;

    /*
     * Before action
     */
    public function beforeAction($action)
    {
        if( !Yii::app()->request->isAjaxRequest )
        {
            // Register css
            Yii::app()->clientScript->registerCssFile(
                Yii::app()->clientScript->getCoreScriptUrl().
                    '/jui/css/base/jquery-ui.css'
            );
            Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js/libs/notify/css/jNotify.jquery.css');
            Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js/libs/loading/css/showLoading.css');
            // Register Js
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/libs/jquery/js/jquery.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/libs/jquery/js/jquery-ui.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/libs/underscore/js/underscore-min.js',CClientScript::POS_HEAD);
            // Register tag input
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/libs/validate/js/jquery.validate.min.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/libs/validate/js/jquery.validate.custom.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/libs/loading/js/jquery.showLoading.min.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/libs/notify/js/jNotify.jquery.min.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/libs/tipsy/js/bootstrap.tipsy.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/global.js',CClientScript::POS_HEAD);

            Yii::app()->clientScript->registerScript('global-jquery','
                // Global
                var global = new global();
                global.constructor();
            ',CClientScript::POS_END);
        }
        return parent::beforeAction($action);
    }

    /**
     * Return data to browser as JSON
     * @param array $data
     */
    protected function renderJSON($data)
    {
        header('Content-type: application/json');
        echo CJSON::encode($data);

        foreach (Yii::app()->log->routes as $route) {
            if($route instanceof CWebLogRoute) {
                $route->enabled = false; // disable any weblogroutes
            }
        }
        Yii::app()->end();
    }

    /*
     * Convert object to json data
     */
    protected function convertObjectToJson($object)
    {
        return CJSON::encode($object->getAttributes());
    }
}
