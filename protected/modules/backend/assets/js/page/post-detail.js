/**
 * Created with JetBrains PhpStorm.
 * User: WIN7
 * Date: 10/20/13
 * Time: 1:50 PM
 * To change this template use File | Settings | File Templates.
 */
var postDetail = function(){};
postDetail.prototype.constructor = function()
{
    var that = this;
    $(document).ready(function(){
        that.init();
        that.uploadImage();
    });
}

postDetail.prototype.init = function()
{
    var that = this;
    $('#tags').tagsInput({
        autocomplete_url: QA_TAG_URL,
        autocomplete:{selectFirst:true,width:'100px',autoFill:true}
    });

    $('#summernote').summernote({
        //focus: true,    //set focus editable area after Initialize summernote
        height: 350,
        onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0],editor,welEditable);
        }
    });
}

postDetail.prototype.uploadImage = function()
{
    $('.fileUploadImage').liteUploader(
        {
            script: IMG_UPLOAD_URL,
            allowedFileTypes: 'image/jpeg,image/png,image/gif',
            maxSizeInBytes: 3048576,// 3MB
            typeMessage: 'You can put your custom type validation message here',
            each: function (file, errors)
            {
                if (errors.length > 0)
                {
                    $('#response').html('One or more files did not pass validation');
                }
            },
            before: function ()
            {
                $('body').showLoading();
            },
            success: function (response)
            {
                if (response['status'])
                {
                    global.notify(true,'Upload success');
                    $('.fileupload-small img').attr('src', global.addExtent(baseUrl+'/'+response['data'],600,400));
                    $('#feature_image').val(response['data']);
//                    $.colorbox.resize();
                }else{
                    global.notify(false, 'Error');
                }
                $('body').hideLoading();
            },error: function(){
            global.notify(false, 'Error');
        }
        });
}

function sendFile(file,editor,welEditable) {
    data = new FormData();
    data.append("file", file);
    $.ajax({
        data: data,
        type: "POST",
        url: POST_AJAX_UPLOAD_FILE,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(res) {
            editor.insertImage(welEditable, res.msg);
        }
    });
}
