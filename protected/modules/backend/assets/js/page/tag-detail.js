/**
 * Created with JetBrains PhpStorm.
 * User: WIN7
 * Date: 10/20/13
 * Time: 1:50 PM
 * To change this template use File | Settings | File Templates.
 */
var tagDetail = function(){};
tagDetail.prototype.constructor = function()
{
    var that = this;
    $(document).ready(function(){
        that.uploadImage();
        that.uploadImage1();
    });
}

tagDetail.prototype.uploadImage = function()
{
    $('.fileUploadImage').liteUploader(
        {
            script: TAG_IMG_UPLOAD_URL,
            allowedFileTypes: 'image/jpeg,image/png,image/gif',
            maxSizeInBytes: 3048576,// 3MB
            typeMessage: 'You can put your custom type validation message here',
            each: function (file, errors)
            {
                if (errors.length > 0)
                {
                    $('#response').html('One or more files did not pass validation');
                }
            },
            before: function ()
            {
                $('body').showLoading();
            },
            success: function (response)
            {
                if (response['status'])
                {
                    global.notify(true,'Upload success');
                    $('.fileupload-small img').attr('src', global.addExtent(baseUrl+'/'+response['data'],200,200));
                    $('#feature_image').val(response['data']);
//                    $.colorbox.resize();
                }else{
                    global.notify(false, 'Error');
                }
                $('body').hideLoading();
            },error: function(){
            global.notify(false, 'Error');
        }
        });
}

tagDetail.prototype.uploadImage1 = function()
{
    var that = this;
    $('.uploadImage').click(function(){
        var id = $(this).attr('data-id');
        $('.fileUploadImage-'+id).click();
        
        $('.fileUploadImage-'+id).liteUploader(
        {
            script: TAG_IMG_UPLOAD_URL,
            allowedFileTypes: 'image/jpeg,image/png,image/gif',
            maxSizeInBytes: 3048576,// 3MB
            typeMessage: 'You can put your custom type validation message here',
            each: function (file, errors)
            {
                if (errors.length > 0)
                {
                    $('#response').html('One or more files did not pass validation');
                }
            },
            before: function ()
            {
                $('body').showLoading();
            },
            success: function (response)
            {
                if (response['status'])
                {
                    global.notify(true,'Upload success');
                    $('.fileupload-small-'+id+' img').attr('src', global.addExtent(baseUrl+'/'+response['data'],200,200));
                    $('#tag_image-'+id).val(response['data']);
    //                    $.colorbox.resize();
                }else{
                    global.notify(false, 'Error');
                }
                $('body').hideLoading();
            },error: function(){
                global.notify(false, 'Error');
            }
    });
    });
    
}

