<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/15/14
 * Time: 12:34 AM
 */


class BackendPostService {
    CONST ITEM_PER_PAGE = 10;

    protected $model;

    /**
     * construct class.
     */
    public function __construct() {
        $this->model = new BackendPost();
    }

    /*
     * Get post by page
     */
    public function getPosts($page, $term,$deleted)
    {
        $start = ($page-1) * self::ITEM_PER_PAGE;
        $term = Utilities::clean($term);
        // Get posts
        $posts = $this->model->getAllPosts($term, $start, self::ITEM_PER_PAGE,$deleted);
        // Count posts
        $count = $this->model->countAllPosts($term,$deleted);
        $pager = array(
            'totalRecord' => $count,
            'itemPerPage' => self::ITEM_PER_PAGE,
            'itemInPage' => count($posts),
            'currentPage' => $page - 1
        );

        return array('posts' => $posts, 'pager' => $pager);
    }


    /*
     * Insert tag
     */
    public function addTags($postId, $tags)
    {
        $tags = explode(',',$tags);
        foreach ($tags as $tag){
            $tagModel = new BackendTag();
            $thisTag = $tagModel->findByAttributes(array('Name' => trim($tag)));
            if (!$thisTag){
                $tagModel->Name = trim($tag);
                $tagModel->Tag = trim($tag);
                $tagModel->tag_order = 1;
                $tagModel->tag_image = 'images/tags/01.jpg';
                if ($tagModel->save()){
                    $postTagModel = new BackendPostTag();
                    $postTagModel->tag_id = $tagModel->primaryKey;
                    $postTagModel->post_id = $postId;
                    if ($postTagModel->save()){

                    }
                }
            }else{
                $postTagModel = new BackendPostTag();
                $postTagModel->tag_id = $thisTag->id;
                $postTagModel->post_id = $postId;
                if ($postTagModel->save(false)){

                }
            }
        }
        return true;
    }
} 