<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 5/3/14
 * Time: 12:27 AM
 */

class BackendTagService {
    CONST ITEM_PER_PAGE_NOLIMIT = 1000;
    CONST ITEM_PER_PAGE = 50;

    protected $tagConvertmodel;
    protected $tagModel;

    /**
     * construct class.
     */
    public function __construct() {
        $this->tagConvertmodel = new BackendConvertTag();
        $this->tagModel = new BackendTag();
    }

    /*
     * Get tag convert by page
     */
    public function getTagsConvert($page, $term)
    {
        $start = ($page-1) * self::ITEM_PER_PAGE_NOLIMIT;
        $term = Utilities::clean($term);
        // Get tags
        $tags = $this->tagConvertmodel->getAllTagsConvert($term, $start, self::ITEM_PER_PAGE_NOLIMIT);
        // Count posts
        $count = $this->tagConvertmodel->countAllTags($term);
        $pager = array(
            'totalRecord' => $count,
            'itemPerPage' => self::ITEM_PER_PAGE_NOLIMIT,
            'itemInPage' => count($tags),
            'currentPage' => $page - 1
        );

        return array('tags' => $tags, 'pager' => $pager);
    }

    /*
     * Get tag by page
     */
    public function getTags($page, $term)
    {
        $start = ($page-1) * self::ITEM_PER_PAGE;
        $term = Utilities::clean($term);
        // Get tags
        $tags = $this->tagModel->getAllTags($term, $start, self::ITEM_PER_PAGE);
        // Count posts
        $count = $this->tagModel->countAllTags($term);
        $pager = array(
            'totalRecord' => $count,
            'itemPerPage' => self::ITEM_PER_PAGE,
            'itemInPage' => count($tags),
            'currentPage' => $page - 1
        );

        return array('tags' => $tags, 'pager' => $pager);
    }
} 