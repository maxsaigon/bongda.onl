<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 3/31/14
 * Time: 12:50 AM
 */

class HomeController extends BackendController{
    /*
     * Home page
     */
    public function actionIndex()
    {
        $this->layout = '../layouts/backend_style';
        $this->breadcrumbs = array(
            'Post Management' => array(),
        );
        return $this->render('index', array());
    }
} 