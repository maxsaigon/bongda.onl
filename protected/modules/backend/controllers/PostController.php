<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/15/14
 * Time: 12:27 AM
 */

class PostController extends BackendController{
    
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * @return array action filters
     */
    public function accessRules()
    {
        return array(
            array('deny',
                'users' => array('?'),
                'deniedCallback' => function() { Yii::app()->controller->redirect(array ('/')); }
            ),
            array('deny',
                'expression'=>'Yii::app()->user->isUser()',
                'deniedCallback' => function() { Yii::app()->controller->redirect(array ('/')); }
            ),
            array('allow',
                'expression'=>'Yii::app()->user->isSAdmin()'
            ),
            array('allow',
                'expression'=>'Yii::app()->user->isMod()',
            ),
        );
    }
    
    /*
     * Home page
     */
    public function actionIndex($deleted=0)
    {
        $this->layout = '../layouts/backend_style';
        $this->breadcrumbs = array(
            'Post Management' => array(),
        );

        // Get value
        $page = isset($_GET['page'])?$_GET['page']:1;
        $term = isset($_GET['term'])?Utilities::clean($_GET['term']):'';
        // service
        $service = new BackendPostService();
        $posts = $service->getPosts($page, $term ,$deleted);

        return $this->render('index', array(
            'posts' => $posts,
        ));
    }
    
    public function actionTrash($deleted=1)
    {
        $this->layout = '../layouts/backend_style';
        $this->breadcrumbs = array(
            'Post Management' => array(),
        );

        // Get value
        $page = isset($_GET['page'])?$_GET['page']:1;
        $term = isset($_GET['term'])?Utilities::clean($_GET['term']):'';
        // service
        $service = new BackendPostService();
        $posts = $service->getPosts($page, $term , $deleted);

        return $this->render('trash', array(
            'posts' => $posts,
        ));
    }

    /*
     * Detail page
     */
    public function actionDetail()
    {
        $this->layout = '../layouts/backend_style';
        // Register css for tag
        Yii::app()->clientScript->registerCssFile($this->baseScriptUrl .'/css/plugins/jquery.tagsinput.css');
        // Register tag input
        Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl.'/js/plugins/tagsInput/jquery.tagsinput.min.js',CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/libs/upload/js/jquery.liteuploader.min.js',CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl.'/js/page/post-detail.js',CClientScript::POS_HEAD);

        Yii::app()->clientScript->registerScript('tinymce-jquery','
            var baseUrl = "'.Yii::app()->request->baseUrl.'";
            var postDetail = new postDetail();
            postDetail.constructor();'
            ,CClientScript::POS_END);
        // Model
        $model = new BackendPost();
        // List all category
        $termModel = new BackendCategory();
        $listCategories = $termModel->getAllCategories('', 0, 1000);
        if (isset($_GET['postId']) && is_numeric($_GET['postId']))
        {
            $post = $model->getPostByPostId($_GET['postId']);
            if ($post){
                // Breadcrumbs
                $this->breadcrumbs = array(
                    'Post Management' => array('/backend/post/index/'),
                    $post['tile'] => array(),
                );
                // Tags
                $result = array();
                $tagModel = new BackendTag();
                $tagss = $tagModel->getTagByPostId($_GET['postId']); 
                foreach($tagss as $tag)
                {
                    $result[] = $tag['Name'];
                }
                $tags = implode(",",$result);

                // Render
                return $this->render('detail', array(
                    'model'     => $model,
                    'post'      => $post,
                    'type'      => 'edit',
                    'tags'      => $tags,
                    'tagss'      => $tagss,
                    'listCategories' => $listCategories
                ));
            }
        }else{
            return $this->redirect($this->createUrl('/backend/post/index'));
        }
    }

    /*
     * Suggest tag
     */
    public function actionTag()
    {
        if( Yii::app()->request->isAjaxRequest && isset($_GET['term']))
        {
            // Clean
            $term = Utilities::clean($_GET['term']);
            // Get all same name
            $model = new BackendConvertTag();
            $tags = $model->getAllTags($term, 0, 20);
            // Result
            $result = array();
            foreach ($tags as $tag)
            {
                $result[] = $tag['tag_des'];
            }
            return $this->renderJSON($result);
        }
    }

    /*
     * Update
     */
    public function actionUpdate()
    {
        // Current time
        $date = new DateTime(); 
        // Validate POST
        if (isset($_POST['Posts']['id']) && is_numeric($_POST['Posts']['id'])) // Update
        {
            // Model
            $model = new BackendPost();
            $post = $model->findByPk($_POST['Posts']['id']);
            // Check exist
            if ($post)
            {
                if(isset($_POST['is_hot_post'])){
                    $post->is_hot = '1';
                    $model = new BackendPost();
                    $model->updateIsHot($_POST['Posts']['id']);
                }
          
                

                $post->attributes = $_POST['Posts'];
                $post->content = $_POST['post_content'];
                if ($post->save(false))
                {
                    // Remove tag
                    $postTagModel = new BackendPostTag();
                    $postTagModel->deleteAll('post_id =:objectId', array(':objectId' => $post->primaryKey));
                    // Add new tag
                    $service = new BackendPostService();
                    $check = $service->addTags($post->primaryKey, $_POST['tags']);
                    $postTagModel->updatePrimary($post->primaryKey,$_POST['is_primary']);
                    if ($check)
                        return $this->redirect($this->createUrl('/backend/post/detail/').'?postId='.$post->primaryKey);
                }
            }
        }else{
            return $this->redirect(array('/backend/post/index/'));
        }
    }
    
    /*
     * Delete post
     */
    public function actionDelete()
    {
        if (isset($_GET['postId']) && is_numeric($_GET['postId']))
        {
            // Model
            $model = new BackendPost();
            $model->deleteByPk($_GET['postId']);
        }
        $this->redirect(Yii::app()->request->urlReferrer);
    }
    
    /*
     * restore post
     */
    public function actionRestore()
    {
        if (isset($_GET['postId']) && is_numeric($_GET['postId']))
        {
            // Model
            $model = new BackendPost();
            $post = $model->findByPk($_GET['postId']);
            $post->is_deleted = 0;
            if ($post->save(false))
                {
                    return $this->redirect(Yii::app()->request->urlReferrer);
                }
        }
        
    }
    
    /*
     * move trash post
     */
    public function actionMove()
    {
        if (isset($_GET['postId']) && is_numeric($_GET['postId']))
        {
             // Model
            $model = new BackendPost();
            $post = $model->findByPk($_GET['postId']);
            $post->is_deleted = 1;
            if ($post->save(false))
                {
                    return $this->redirect(Yii::app()->request->urlReferrer);;
                }
        }
        
    }
} 