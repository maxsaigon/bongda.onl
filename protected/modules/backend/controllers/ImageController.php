<?php
class ImageController extends BackendController
{
    /*
    * Upload image
    */
    public function actionPostSendFile()
    {
        if ($_FILES["file"]["error"] > 0) {
            return $this->renderJSON(array('status' => false, 'msg' => "Error: " . $_FILES["file"]["error"]));
        }
        else {
            $date = new DateTime();
            $year = $date->format('Y');
            $month = $date->format('m');
            // Save file
            move_uploaded_file($_FILES["file"]["tmp_name"], "./download/posts/".$year.'/'.$month.'/xef_vn_' . $_FILES["file"]["name"]);
            return $this->renderJSON(array('status' => true, 'msg' => Yii::app()->params['site_url']."download/posts/".$year.'/'.$month.'/xef_vn_' . $_FILES["file"]["name"]));
        }
    }

    /**
     * Upload image
     * return json
     */
    public function actionUpload()
    {
        $url = '';
        // Check post value
        if (isset($_POST['liteUploader_id']) && $_POST['liteUploader_id'] == 'fileUploadImage')
        {
            // Check image size
            if ($_FILES['fileUploadImage']['size'][0] < SiteConstant::IMAGE_SIZE){
                foreach ($_FILES['fileUploadImage']['error'] as $key => $error)
                {
                    if ($error == UPLOAD_ERR_OK)
                    {
                        $date = new DateTime();
                        $path = Yii::app()->params['download_source'].'/'.$date->format("Y").'/'.$date->format("m");
                        // Create folder & set permit
                        if(!is_dir($path)){
                            mkdir($path, 0777);
                        }
                        $url = $path.'/'. $_FILES['fileUploadImage']['name'][$key];
                        // Upload image
                        move_uploaded_file( $_FILES['fileUploadImage']['tmp_name'][$key], $url);
                        // Generate 50 x50
                        $imageService = new ImageService();
                        $returnUrl = 'download/posts/'.$date->format("Y").'/'.$date->format("m").'/'. $_FILES['fileUploadImage']['name'][$key].'?'.time();
                        $imageService->cropImage($url, Yii::app()->params['dimention']);
                        return $this->renderJSON(array('status' => true, 'data' => $returnUrl));
                    }
                }
            }
            return $this->renderJSON(array('status' => false, 'data' => ''));
        }
    }

    /**
     * Upload image
     * return json
     */
    public function actionTagUpload()
    {
        $url = '';
        // Check post value
        if (isset($_POST['liteUploader_id']) && $_POST['liteUploader_id'] == 'fileUploadImage')
        {
            // Check image size
            if ($_FILES['fileUploadImage']['size'][0] < SiteConstant::IMAGE_SIZE){
                foreach ($_FILES['fileUploadImage']['error'] as $key => $error)
                {
                    if ($error == UPLOAD_ERR_OK)
                    {
                        $path = Yii::app()->params['tag_source'];
                        // Create folder & set permit
                        if(!is_dir($path)){
                            mkdir($path, 0777);
                        }
                        $url = $path. $_FILES['fileUploadImage']['name'][$key];
                        // Upload image
                        move_uploaded_file( $_FILES['fileUploadImage']['tmp_name'][$key], $url);
                        // Generate 50 x50
                        $imageService = new ImageService();
                        $returnUrl = 'download/tags/'.$_FILES['fileUploadImage']['name'][$key];
                        $imageService->cropImage($url, Yii::app()->params['tag_dimention']);
                        return $this->renderJSON(array('status' => true, 'data' => $returnUrl));
                    }
                }
            }
            return $this->renderJSON(array('status' => false, 'data' => ''));
        }
    }
}
?>