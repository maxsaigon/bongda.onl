<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 3/22/14
 * Time: 11:03 AM
 */

class BackendController extends Controller
{
    public $baseScriptUrl;
    /*
     * Before action
     */
    public function beforeAction($action)
    {
        Yii::app()->clientScript->registerScript('globals-jquery',"
            var IMG_UPLOAD_URL = '".$this->createUrl('/backend/image/upload')."';
            var POST_AJAX_UPLOAD_FILE = '".$this->createUrl('/backend/image/postSendFile')."';
            var QA_TAG_URL = '".$this->createUrl('/backend/post/tag/')."';
            var TAG_IMG_UPLOAD_URL = '".$this->createUrl('/backend/image/tagUpload/')."';
            var VALIDATE_EMAI_URL = '".$this->createUrl('/frontend/site/validateEmail/')."';
        ",CClientScript::POS_BEGIN);

        // Title
        $this->pageTitle = Yii::app()->params['title'];
        // Description
        $this->pageDescription = Yii::app()->params['description'];
        // Register js & css
        if( !Yii::app()->request->isAjaxRequest )
        {
            $this->baseScriptUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.backend.assets'));
            // Register css
            Yii::app()->clientScript->registerCssFile(
                Yii::app()->clientScript->getCoreScriptUrl().
                '/jui/css/base/jquery-ui.css'
            );
            Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js/libs/notify/css/jNotify.jquery.css');
            Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js/libs/loading/css/showLoading.css');
            Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl .'/js/libs/bootstrap/dist/css/bootstrap.css');
            Yii::app()->clientScript->registerCssFile($this->baseScriptUrl .'/fonts/font-awesome-4/css/font-awesome.min.css');
            Yii::app()->clientScript->registerCssFile($this->baseScriptUrl .'/js/jquery.gritter/css/jquery.gritter.css');
            Yii::app()->clientScript->registerCssFile($this->baseScriptUrl .'/js/jquery.nanoscroller/nanoscroller.css');
            Yii::app()->clientScript->registerCssFile($this->baseScriptUrl .'/js/jquery.easypiechart/jquery.easy-pie-chart.css');
            Yii::app()->clientScript->registerCssFile($this->baseScriptUrl .'/js/bootstrap.switch/bootstrap-switch.css');
            Yii::app()->clientScript->registerCssFile($this->baseScriptUrl .'/js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css');
            Yii::app()->clientScript->registerCssFile($this->baseScriptUrl .'/js/jquery.select2/select2.css');
            Yii::app()->clientScript->registerCssFile($this->baseScriptUrl .'/js/bootstrap.slider/css/slider.css');
            Yii::app()->clientScript->registerCssFile($this->baseScriptUrl .'/js/jquery.easypiechart/jquery.easy-pie-chart.css');
            Yii::app()->clientScript->registerCssFile($this->baseScriptUrl .'/js/intro.js/introjs.css');
            Yii::app()->clientScript->registerCssFile($this->baseScriptUrl .'/js/bootstrap.summernote/dist/summernote.css');
            Yii::app()->clientScript->registerCssFile($this->baseScriptUrl .'/css/style.css');
            
            Yii::app()->clientScript->registerCssFile($this->baseScriptUrl.'/js/plugins/jquery.tablesorter/themes/blue/style.css');
            // Register Js
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/jquery.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/jquery.nestable/jquery.nestable.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/libs/jquery/js/jquery-ui.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/libs/underscore/js/underscore-min.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/jquery.gritter/js/jquery.gritter.min.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/jquery.nanoscroller/jquery.nanoscroller.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/jquery.sparkline/jquery.sparkline.min.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/behaviour/voice-commands.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/jquery.flot/jquery.flot.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/jquery.flot/jquery.flot.pie.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/jquery.flot/jquery.flot.resize.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/jquery.flot/jquery.flot.labels.js',CClientScript::POS_HEAD);
            // Register tag input
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/libs/validate/js/jquery.validate.min.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/libs/validate/js/jquery.validate.custom.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/libs/loading/js/jquery.showLoading.min.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/libs/notify/js/jNotify.jquery.min.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/bootstrap/dist/js/bootstrap.min.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/libs/tipsy/js/bootstrap.tipsy.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/bootstrap.switch/bootstrap-switch.min.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl .'/js/jquery.select2/select2.min.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl .'/js/skycons/skycons.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl .'/js/bootstrap.summernote/dist/summernote.min.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl .'/js/jquery.easypiechart/jquery.easy-pie-chart.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl .'/js/intro.js/intro.js',CClientScript::POS_HEAD);

            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/behaviour/general.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/global.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js',CClientScript::POS_HEAD);
            
            //table sorter
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/plugins/jquery.tablesorter/jquery.tablesorter.min.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScript('global-jquery','
                // Global
                var global = new global();
                global.constructor();
            ',CClientScript::POS_END);
        }
        return parent::beforeAction($action);
    }
    /**
     * Return data to browser as JSON
     * @param array $data
     */
    protected function renderJSON($data)
    {
        header('Content-type: application/json');
        echo CJSON::encode($data);
        foreach (Yii::app()->log->routes as $route) {
            if($route instanceof CWebLogRoute) {
                $route->enabled = false; // disable any weblogroutes
            }
        }
        Yii::app()->end();
    }
} 