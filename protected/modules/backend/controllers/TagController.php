<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 5/3/14
 * Time: 12:20 AM
 */

class TagController extends BackendController {
    
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * @return array action filters
     */
    public function accessRules()
    {
        return array(
            array('deny',
                'users' => array('?'),
                'deniedCallback' => function() { Yii::app()->controller->redirect(array ('/')); }
            ),
            array('deny',
                'expression'=>'Yii::app()->user->isUser()',
                'deniedCallback' => function() { Yii::app()->controller->redirect(array ('/')); }
            ),
            array('allow',
                'expression'=>'Yii::app()->user->isSAdmin()'
            ),
            array('allow',
                'expression'=>'Yii::app()->user->isMod()',
            ),
        );
    }
    /*
     * Home page
     */
    public function actionTagConvert()
    {
        $this->layout = '../layouts/backend_style';
        $this->breadcrumbs = array(
            'Tag Management' => array(),
        );

        // Get value
        $page = isset($_GET['page'])?$_GET['page']:1;
        $term = isset($_GET['term'])?Utilities::clean($_GET['term']):'';
        // service
        $service = new BackendTagService();
        $tags = $service->getTagsConvert($page, $term);

        return $this->render('tag_convert_manage', array(
            'tags' => $tags,
        ));
    }

    /*
     * Update tag convert
     */
    public function actionUpdateTagConvert()
    {
        if (isset($_POST['tag']['id']) && is_numeric($_POST['tag']['id'])) // Update
        {
            // Model
            $model = new BackendConvertTag();
            $post = $model->findByPk($_POST['tag']['id']);
            // Check exist
            if ($post)
            {
                $post->attributes = $_POST['tag'];
                if ($post->save(false))
                {
                    return $this->redirect($this->createUrl('/backend/tag/tagConvert'));
                }
            }
        }
        return $this->redirect($this->createUrl('/backend/tag/tagConvert'));
    }

    /*
     * Create tag convert
     */
    public function actionCreateTagConvert()
    {
        // Model
        $model = new BackendConvertTag();
        $model->attributes = $_POST['tag'];
        if ($model->save(false)){}
        return $this->redirect($this->createUrl('/backend/tag/tagConvert'));
    }

    /*
     * Delete tag convert
     */
    public function actionDeleteTagConvert()
    {
        if (isset($_GET['id']) && is_numeric($_GET['id'])){
            // Model
            $model = new BackendConvertTag();
            $model->deleteByPk($_GET['id']);
        }
        return $this->redirect($this->createUrl('/backend/tag/tagConvert'));
    }

    /*
     * Get all tag
     */
    public function actionTag()
    {
        $this->layout = '../layouts/backend_style';
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/libs/upload/js/jquery.liteuploader.min.js',CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl.'/js/page/tag-detail.js',CClientScript::POS_HEAD);

        Yii::app()->clientScript->registerScript('tinymce-jquery','
            var baseUrl = "'.Yii::app()->request->baseUrl.'";
            var tagDetail = new tagDetail();
            tagDetail.constructor();'
            ,CClientScript::POS_END);
        $this->breadcrumbs = array(
            'Tag Management' => array(),
        );

        // Get value
        $page = isset($_GET['page'])?$_GET['page']:1;
        $term = isset($_GET['term'])?Utilities::clean($_GET['term']):'';
        // service
        $service = new BackendTagService();
        $tags = $service->getTags($page, $term);

        return $this->render('tag_manage', array(
            'tags' => $tags,
        ));
    }

    /*
     * Detail page
     */
    public function actionDetail()
    {
        $this->layout = '../layouts/backend_style';
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/libs/upload/js/jquery.liteuploader.min.js',CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl.'/js/page/tag-detail.js',CClientScript::POS_HEAD);

        Yii::app()->clientScript->registerScript('tinymce-jquery','
            var baseUrl = "'.Yii::app()->request->baseUrl.'";
            var tagDetail = new tagDetail();
            tagDetail.constructor();'
            ,CClientScript::POS_END);
        if (isset($_GET['tagId']) && is_numeric($_GET['tagId']))
        {
            $model = new BackendTag();
            $tag = $model->findByPk($_GET['tagId']);
            if ($tag){
                // Breadcrumbs
                $this->breadcrumbs = array(
                    'Tag Management' => array('/backend/tag/tag/'),
                    $tag->Name => array(),
                );

                // Render
                return $this->render('tag_detail', array(
                    'model'     => $model,
                    'tag'      => $tag,
                    'type'      => 'edit'
                ));
            }
        }else{
            return $this->redirect($this->createUrl('/backend/tag/tag'));
        }
    }

    /*
     * Update
     */
    public function actionUpdate()
    {
        // Validate POST
        if (isset($_POST['tag']['id']) && is_numeric($_POST['tag']['id'])) // Update
        {
            // Model
            $model = new BackendTag();
            $post = $model->findByPk($_POST['tag']['id']);
            // Check exist
            if ($post)
            {
                $post->attributes = $_POST['tag'];
                $post->Tag = $_POST['tag']['Tag'];
                $post->Name = $_POST['tag']['Tag'];
                if ($post->save(false))
                {
                    return $this->redirect($this->createUrl('/backend/tag/detail/').'?tagId='.$post->primaryKey);
                }
            }
        }else{
            return $this->redirect(array('/backend/tag/tag/'));
        }
    }
    
    
    /*
     * Update Image
     */
    public function actionUpdateImage()
    {
        // Validate POST
        if (isset($_POST['id']) && is_numeric($_POST['id'])) // Update
        {
            // Model
            $model = new BackendTag();
            $post = $model->findByPk($_POST['id']);
            // Check exist
            if ($post)
            {
                $post->tag_image = $_POST['tag_image'];
                if ($post->save(false))
                {
                    return $this->redirect($this->createUrl('/backend/tag/tag/'));
                }
            }
        }else{
            return $this->redirect(array('/backend/tag/tag/'));
        }
    }

    /*
     * Delete
     */
    public function actionDelete()
    {
        if (isset($_GET['tagId']) && is_numeric($_GET['tagId'])){
            // Model
            $model = new BackendTag();
            $tag = $model->findByPk($_GET['tagId']);
            // Check exist
            if ($tag)
            {
                // Delete post tag
                $postTagModel = new BackendPostTag();
                $postTagModel->deleteAll('tag_id =:tagId', array(':tagId' => $tag->id));
                // Delete tag
                $model->deleteByPk($tag->id);
            }
        }
        return $this->redirect(array('/backend/tag/tag/'));
    }
} 