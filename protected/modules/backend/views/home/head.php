<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/15/14
 * Time: 12:30 AM
 */
?>
<div id="head-nav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="fa fa-gear"></span>
            </button>
            <a class="navbar-brand" href="#"><span>BongDa App</span></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="http://bongda.onl" rel="_bank">Font-End</a></li>
<!--
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Live Content <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Create new post</a></li>
                        <li><a href="#">Post manager</a></li>
                        <li><a href="#">Tag manager</a></li>
                        <li class="dropdown-submenu"><a href="#">Category</a>
                            <ul class="dropdown-menu">
                                <li><a href="#">England</a></li>
                                <li><a href="#">Spain</a></li>
                                <li><a href="#">Italia</a></li>
                                <li><a href="#">Germany</a></li>
                                <li><a href="#">C1</a></li>
                                <li class="dropdown-submenu"><a href="#">Other</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">France</a></li>
                                        <li><a href="#">Vietnam</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Raw Content <b class="caret"></b></a>
                    <ul class="dropdown-menu col-menu-2">
                        <li class="col-sm-6 no-padding">
                            <ul>
                                <li class="dropdown-header"><i class="fa fa-gear"></i>Leech Engine</li>
                                <li><a href="#">Config</a></li>
                                <li><a href="#">Source Manager</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="dropdown-header"><i class="fa fa-rocket"></i>Source</li>
                                <li><a href="#">Bongda.com.vn</a></li>
                                <li><a href="#">Bongdaplus.vn</a></li>
                                <li><a href="#">Thethaovanhoa.vn</a></li>
                            </ul>
                        </li>
                        <li  class="col-sm-6 no-padding">
                            <ul>
                                <li class="dropdown-header"><i class="fa fa-legal"></i>Raw Content</li>
                                <li><a href="#">New post</a></li>
                                <li><a href="#">Filter by tags</a></li>
                                <li><a href="#">Filter by category</a></li>
                                <li><a href="#">Compare </a></li>
                                <li><a href="#">Raw post manager</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
-->
            </ul>
<!--
            <ul class="nav navbar-nav navbar-right user-nav">
                <li class="dropdown profile_menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img alt="Avatar" src="images/avatar2.jpg" /><span>Thanh Nguyen</span> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">My Account</a></li>
                        <li><a href="#">Profile</a></li>
                        <li><a href="#">Messages</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Sign Out</a></li>
                    </ul>
                </li>
            </ul>
-->
        </div><!--/.nav-collapse animate-collapse -->
    </div>
</div>