<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/15/14
 * Time: 12:28 AM
 */
?>
<div class="cl-sidebar">
    <div class="cl-toggle"><i class="fa fa-bars"></i></div>
    <div class="cl-navblock">
        <div class="menu-space">
            <div class="content">
                <div class="side-user">
                    <div class="avatar"><img src="images/avatar1_50.jpg" alt="Avatar" /></div>
                    <div class="info">
                        <a href="#">Thanh Nguyen</a>
                        <img src="images/state_online.png" alt="Status" /> <span>Online</span>
                    </div>
                </div>
                <ul class="cl-vnavigation">
                    <li class="active"><a href="index.html"><i class="fa fa-home"></i><span>Trang chủ</span></a></li>
                    <li>
                        <a href="<?php echo $this->createUrl('/backend/post/index'); ?>">
                            <i class="fa fa-smile-o"></i><span>Bài Viết</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $this->createUrl('/backend/tag/tagConvert'); ?>">
                            <i class="fa fa-smile-o"></i><span>Chuyển đổi tag</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $this->createUrl('/backend/tag/tag'); ?>">
                            <i class="fa fa-smile-o"></i><span>Quản lý tag</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $this->createUrl('/backend/post/trash'); ?>">
                            <i class="fa fa-smile-o"></i><span>Thùng rác</span>
                        </a>
                    </li>
<!--                    <li><a href="#"><i class="fa fa-map-marker nav-icon"></i><span>Source Manager</span></a>-->
<!--                        <ul class="sub-menu">-->
<!--                            <li><a href="source.html">Source</a></li>-->
<!--                            <li><a href="source-manager.html">Create Source</a></li>-->
<!--                        </ul>-->
<!--                    </li>-->
<!--                    <li><a href="#"><i class="fa fa-user nav-icon"></i><span>User Manager</span></a>-->
<!--                        <ul class="sub-menu">-->
<!--                            <li><a href="user.html">User Manager</a></li>-->
<!--                            <li><a href="user-edit.html">User Edit</a></li>-->
<!--                        </ul>-->
<!--                    </li>-->
<!--                    <li><a href="charts.html"><i class="fa fa-trash-o"></i><span>Trash</span></a></li>-->
<!--                    <li><a href="#"><i class="fa fa-file"></i><span>Pages</span></a>-->
<!--                        <ul class="sub-menu">-->
<!--                            <li><a href="pages-blank.html">Blank Page</a></li>-->
<!--                            <li><a href="pages-blank-header.html">Blank Page Header</a></li>-->
<!--                            <li><a href="pages-blank-aside.html"><span class="label label-primary pull-right">New</span>Blank Page Aside</a></li>-->
<!--                            <li><a href="pages-login.html">Login</a></li>-->
<!--                            <li><a href="pages-404.html">404 Page</a></li>-->
<!--                            <li><a href="pages-500.html">500 Page</a></li>-->
<!--                            <li><a href="pages-gallery.html"><span class="label label-primary pull-right">New</span>Gallery</a></li>-->
<!--                            <li><a href="pages-timeline.html"><span class="label label-primary pull-right">New</span>Timeline</a></li>-->
<!--                        </ul>-->
<!--                    </li>-->
                </ul>
            </div>
        </div>
        <div class="text-right collapse-button" style="padding:7px 9px;">
            <input type="text" class="form-control search" placeholder="Search..." />
            <button id="sidebar-collapse" class="btn btn-default" style=""><i style="color:#fff;" class="fa fa-angle-left"></i></button>
        </div>
    </div>
</div>