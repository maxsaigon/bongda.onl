<?php $this->renderPartial('../home/head', array()); ?>
<div id="cl-wrapper" class="fixed-menu">
    <?php $this->renderPartial('../home/left', array()); ?>
<div class="container-fluid" id="pcont">
<div class="page-head">
    <h2><?php echo $type == 'create'?'Viết bài mới':'Sửa bài viết'; ?></h2>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->createUrl('/backend/');?>">Trang chủ</a></li>
        <li><a href="<?php echo $this->createUrl('/backend/');?>">Quản lý bài viết</a></li>
        <li class="active"><?php echo $type == 'create'?'Viết bài mới':'Sửa bài viết'; ?></li>
    </ol>
</div>
<div class="cl-mcont">
<div class="row">
    <div class="block-flat">
        <div class="header">
            <h3><?php echo $type == 'create'?'Viết bài mới':'Sửa bài viết'; ?></h3>
        </div>
        <div class="content">
            <form class="form-horizontal span12" action="<?php echo $this->createUrl('/backend/post/update/'); ?>" method="POST">
                <?php if ($type != 'create'): ?>
                    <input type="hidden" name="Posts[id]" value="<?php echo $post['id']; ?>">
                <?php endif ?>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tiêu Đề</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="input" name="Posts[tile]" value="<?php echo isset($post['tile'])?$post['tile']:''; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Mô Tả</label>
                    <div class="col-sm-6">
                        <textarea rows="3" class="form-control" name="Posts[description]"><?php echo isset($post['description'])?$post['description']:''; ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Từ Khoá</label> 
                    <div class="col-sm-6">
                        <input name="tags" id="tags" class="tagsinputs" value="<?php echo isset($tags)?$tags:'' ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Trạng thái</label>
                    <div class="col-sm-6">
                        <select id="select-status" name="Posts[is_reviewed]" class="form-control">
                            <option value="0" <?php echo isset($post['is_reviewed'])&&$post['is_reviewed']=='0'?'selected':''; ?> >
                                Chưa duyệt
                            </option>
                            <option value="1" <?php echo isset($post['is_reviewed'])&&$post['is_reviewed']=='1'?'selected':''; ?> >
                                Đã duyệt
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tag Hiển thị</label>
                    <div class="col-sm-6">
                        <select id="select-status" name="is_primary" class="form-control">
                            <?php foreach($tagss as $tag): ?>
                                
                                <option value="<?php echo $tag['id'] ?>">
                                    <?php echo $tag['Name'] ?>
                                </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Is Hot Post</label>
                    <div class="radio"> 
                     <input class="icheck"  type="checkbox" name="is_hot_post" value="1" id="optionsCheckbox" <?php echo isset($post['is_hot'])&&$post['is_hot']==1?'checked=checked':''; ?> >
                    
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="selectMultiple">Ảnh đại diện</label>
                    <div class="col-sm-6">
                        <div data-provides="fileupload" class="fileupload fileupload-new">
                            <input type="hidden">
                            <div class="fileupload-new fileupload-small thumbnail">
                                <img alt="Upload preview" src="<?php echo Yii::app()->params['site_url'].Utilities::addImageEx($post['feature_image'],600,400); ?>" width="120">
                            </div>
                            <input type="hidden" name="Posts[feature_image]" id="feature_image" value="<?php echo isset($post['feature_image'])?$post['feature_image']: ''; ?>">
                            <input id="fileUploadImage" type="file" name="fileUploadImage" class="fileUploadImage" VALUE="upload" />
                            <p class="help-block">Chọn hình ảnh đại diện của bài viết</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="selectMultiple">Thời Điểm Khởi Tạo</label>
                    <div class="col-sm-6">
                        <div class="input-group date datetime" data-show-meridian="true" data-start-view="1" data-date="" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input">
                            <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
                            <input type="text" value="<?php echo isset($post['created'])?substr($post['created'],0,16):''; ?>" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="content">
                            <textarea class="input-block-level" id="summernote" name="post_content" rows="18">
                                <?php if (isset($post['content'])){
                                    $content = str_replace('<br>','', $post['content']);
                                    echo Utilities::wpautop($content);
                                }
                                ?>
                            </textarea>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <?php if ($type == 'create'): ?>
                    <button type="submit" class="btn btn-primary">Tạo tin mới</button>
                <?php else: ?>
                    <button type="submit" class="btn btn-primary">Chỉnh sửa tin hiện tại</button>
                    <button type="cancel" onclick="javascript:document.location.reload();" class="btn btn-primary">Cancel</button>
                <?php endif ?>
            </form>
        </div>
    </div>
</div>
</div>
</div>
