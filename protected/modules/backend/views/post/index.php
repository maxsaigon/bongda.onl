<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 3/31/14
 * Time: 12:58 AM
 */
?>
<!-- Fixed navbar -->

<?php $this->renderPartial('../home/head', array()); ?>
<div id="cl-wrapper" class="fixed-menu">
    <?php $this->renderPartial('../home/left', array()); ?>

    <div class="container-fluid" id="pcont">
        
        <div class="cl-mcont">
            <form class="form-horizontal" action="">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="pull-left">
                            <div>
                                <label>
                                
                                    <input type="text" aria-controls="datatable" class="form-control" id="input" name="term" placeholder="Search">
                                </label>
                            </div>
                        </div>
        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class=" blue  tablesorter" id="myTable" >
                    <thead>
                    <tr>
                        <th class="text-center">Ảnh</th>
                        <th class="text-center">Title</th>
                        <th class="text-center">URL</th>
                        <th class="text-center">Trạng thái</th>
                        <th class="text-center">Bài Hot</th>
                        <th class="text-center">Time</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($posts['posts'] as $post): ?>
                    <tr>
                        <td class="text-left">
                            <img src="<?php echo Yii::app()->params['site_url'].Utilities::addImageEx($post['feature_image'],600,400); ?>" width="120">
                        </td>
                        <td class="text-left">
                            <a href="<?php echo $this->createUrl('/backend/post/detail?postId='.$post['id']) ?>"><?php echo $post['tile']; ?></a>
                        </td>
                        <td class="text-left"><?php echo $post['url']; ?></td>
                        <td class="text-left"><?php echo $post['is_reviewed']==0?'Chưa duyệt':'Đã duyệt'; ?></td>
                        <td class="text-left"><?php echo $post['is_hot']==0?'Không':'Có'; ?></td>
                        <td class="text-left"><?php echo $post['created']; ?></td>
                        <td class="text-left">
                            <a href="<?php echo $this->createUrl('/backend/post/detail?postId='.$post['id']) ?>">
                                <i class="fa fa-pencil-square-o"></i>
                            </a>
                            <a href="<?php echo $this->createUrl('/backend/post/move?postId='.$post['id']) ?>"><i class="fa fa-trash-o"></i></a></td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <?php echo $this->renderPartial('//base/pagination', array('data' => $posts)); ?>
        </div>
    </div>

</div>
<script>
    $(document).ready(function() 
    { 
        $("#myTable").tablesorter({
            // pass the headers argument and assing a object 
        headers: { 
            // assign the secound column (we start counting zero) 
            0: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            }, 
            // assign the third column (we start counting zero) 
            2: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            },
            // assign the third column (we start counting zero) 
            6: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            }, 
        } 
            
             
        }); 
    } 
); 
    </script>