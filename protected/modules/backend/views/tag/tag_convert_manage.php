<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 3/31/14
 * Time: 12:58 AM
 */
?>
<!-- Fixed navbar -->

<?php $this->renderPartial('../home/head', array()); ?>
<div id="cl-wrapper" class="fixed-menu">
    <?php $this->renderPartial('../home/left', array()); ?>

    <div class="container-fluid" id="pcont">

        <div class="cl-mcont">
            <div class="table-responsive">
                <table class="blue">
                    <thead>
                    <tr>
                        <th class="text-center">Chuyển đổi từ</th>
                        <th class="text-center">Sang</th>
                        <th class="text-center">Công cụ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($tags['tags'] as $tag): ?>
                        <form action="<?php echo $this->createUrl('/backend/tag/updateTagConvert') ?>" method="post">
                        <tr><input type="hidden" name="tag[id]" value="<?php echo $tag['id']; ?>">
                            <td class="text-left">
                                <input class="input-medium" type="text" name="tag[tag_source]" value="<?php echo $tag['tag_source']; ?>">
                            </td>
                            <td class="text-left">
                                <input class="input-medium" type="text" name="tag[tag_des]" value="<?php echo $tag['tag_des']; ?>">
                            </td>
                            <td class="text-left">
                                <button type="submit" >Sửa</button> |
                                <a href="<?php echo $this->createUrl('/backend/tag/deleteTagConvert').'?id='.$tag['id'] ?>">Xóa</a>
                            </td>
                        </tr>
                        </form>
                    <?php endforeach; ?>
                        <form action="<?php echo $this->createUrl('/backend/tag/createTagConvert') ?>" method="post">
                            <tr>
                                <td class="text-left">
                                    <input class="input-medium" type="text" name="tag[tag_source]" value="" placeholder="Nhập tên">
                                </td>
                                <td class="text-left">
                                    <input class="input-medium" type="text" name="tag[tag_des]" value="" placeholder="Nhập tên">
                                </td>
                                <td class="text-left">
                                    <button type="submit" >Tạo mới</button>
                                </td>
                            </tr>
                        </form>
                    </tbody>
                </table>
            </div>
            <?php echo $this->renderPartial('//base/pagination', array('data' => $tags)); ?>
        </div>
    </div>

</div>