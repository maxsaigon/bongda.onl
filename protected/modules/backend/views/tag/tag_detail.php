<?php $this->renderPartial('../home/head', array()); ?>
<div id="cl-wrapper" class="fixed-menu">
    <?php $this->renderPartial('../home/left', array()); ?>
    <div class="container-fluid" id="pcont">
        <div class="page-head">
            <h2><?php echo $type == 'create'?'Viết bài mới':'Sửa bài viết'; ?></h2>
            <ol class="breadcrumb">
                <li><a href="<?php echo $this->createUrl('/backend/');?>">Trang chủ</a></li>
                <li><a href="<?php echo $this->createUrl('/backend/tag/tag/');?>">Quản lý tag</a></li>
                <li class="active"><?php echo $type == 'create'?'Viết bài mới':'Sửa bài viết'; ?></li>
            </ol>
        </div>
        <div class="cl-mcont">
            <div class="row">
                <div class="block-flat">
                    <div class="header">
                        <h3><?php echo $type == 'create'?'Viết bài mới':'Sửa bài viết'; ?></h3>
                    </div>
                    <div class="content">
                        <form class="form-horizontal span12" action="<?php echo $this->createUrl('/backend/tag/update/'); ?>" method="POST">
                            <?php if ($type != 'create'): ?>
                                <input type="hidden" name="tag[id]" value="<?php echo $tag['id']; ?>">
                            <?php endif ?>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Tên tag</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="input" name="tag[Tag]" value="<?php echo isset($tag['Tag'])?$tag['Tag']:''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Trạng thái</label>
                                <div class="col-sm-6">
                                    <select id="select-status" name="tag[tag_order]" class="form-control">
                                        <option value="0" <?php echo isset($tag['tag_order'])&&$tag['tag_order']=='0'?'selected':''; ?> >
                                            Không ưu tiên
                                        </option>
                                        <option value="1" <?php echo isset($tag['tag_order'])&&$tag['tag_order']=='1'?'selected':''; ?> >
                                            Ưu tiên
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Level</label>
                                <div class="col-sm-6">
                                    <select id="select-status" name="tag[tag_level]" class="form-control">
                                        <option value="1" <?php echo isset($tag['tag_level'])&&$tag['tag_level']=='1'?'selected':''; ?> >
                                            Level 1 
                                        </option>
                                        <option value="2" <?php echo isset($tag['tag_level'])&&$tag['tag_level']=='2'?'selected':''; ?> >
                                            Level 2
                                        </option>
                                        <option value="3" <?php echo isset($tag['tag_level'])&&$tag['tag_level']=='3'?'selected':''; ?> >
                                            Level 3
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="selectMultiple">Ảnh đại diện</label>
                                <div class="col-sm-6">
                                    <div data-provides="fileupload" class="fileupload fileupload-new">
                                        <input type="hidden">
                                        <div class="fileupload-new fileupload-small thumbnail">
                                            <img alt="Upload preview" src="<?php echo Yii::app()->params['site_url'].Utilities::addImageEx($tag['tag_image'],200,200); ?>" width="120">
                                        </div>
                                        <input type="hidden" name="tag[tag_image]" id="feature_image" value="<?php echo isset($tag['tag_image'])?$tag['tag_image']: ''; ?>">
                                        <input id="fileUploadImage" type="file" name="fileUploadImage" class="fileUploadImage" VALUE="upload" />
                                        <p class="help-block">Chọn hình ảnh đại diện cho tag</p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <?php if ($type == 'create'): ?>
                                <button type="submit" class="btn btn-primary">Tạo tin mới</button>
                            <?php else: ?>
                                <button type="submit" class="btn btn-primary">Chỉnh sửa tag hiện tại</button>
                                <button type="cancel" onclick="javascript:document.location.reload();" class="btn btn-primary">Cancel</button>
                            <?php endif ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
