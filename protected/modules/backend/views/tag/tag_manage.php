<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 3/31/14
 * Time: 12:58 AM
 */
?>
<!-- Fixed navbar -->

<?php $this->renderPartial('../home/head', array()); ?>
<div id="cl-wrapper" class="fixed-menu">
    <?php $this->renderPartial('../home/left', array()); ?>

    <div class="container-fluid" id="pcont">

        <div class="cl-mcont">
            <form class="form-horizontal" action="">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="pull-left">
                            <div>
                                <label>
                                
                                    <input type="text" aria-controls="datatable" class="form-control" id="input" name="term" placeholder="Search">
                                </label>
                            </div>
                        </div>
        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="blue tablesorter" id="myTable">
                    <thead>
                    <tr>
                        <th class="text-center">Hình ảnh</th>
                        <th class="text-center">Tên</th>
                        <th class="text-center">Ưu tiên</th>
                        <th class="text-center">Level</th>
                        <th class="text-center">Công cụ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($tags['tags'] as $tag): ?>
                    <form method="post" action='<?php echo $this->createUrl('/backend/tag/updateImage/') ?>'>
                        <tr>
                            <input name="id" type="hidden" value="<?php echo $tag['id']; ?>" />
                            <td>
                                <div class=" text-left fileupload-new fileupload-small-<?php echo $tag['id']; ?>">
                                    <img src="<?php echo isset($tag['tag_image'])?Yii::app()->params['site_url'].Utilities::addImageEx($tag['tag_image'],200,200):'/images/sample_content/upload-50x50.png'; ?>" alt="" width="50px" height="50px"
                                         onerror="this.src='<?php echo Yii::app()->params['default_image']; ?>'" class="uploadImage"
                                        data-id="<?php echo $tag['id']; ?>" >
                                </div>
                                 <input type="hidden" name="tag_image" id="tag_image-<?php echo $tag['id']; ?>" value="<?php echo $tag['tag_image'] ?>">   
                                 <input id="fileUploadImage" type="file" name="fileUploadImage" class="fileUploadImage-<?php echo $tag['id']; ?> hidden" VALUE="upload"/>
                            </td>
                            <td class="text-left">
                                <a href="<?php echo $this->createUrl('/backend/tag/detail?tagId='.$tag['id']) ?>">
                                    <?php echo $tag['Tag']; ?>
                                </a>
                            </td>
                            <td class="text-left">
                                <?php echo $tag['tag_order']==1?'Có':'Không'; ?>
                            </td>
                            <td class="text-left">
                                <?php echo $tag['tag_level']; ?>
                            </td>
                            <td class="text-left">
                                <a href="<?php echo $this->createUrl('/backend/tag/detail?tagId='.$tag['id']) ?>">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>
                                <a href="<?php echo $this->createUrl('/backend/tag/delete?tagId='.$tag['id']) ?>">
                                    <i class="fa fa-trash-o"></i></a>
                                <button type="submit">Sửa Hình Ảnh</button>   
                            </td>

                        </tr>
                    </form>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <?php echo $this->renderPartial('//base/pagination', array('data' => $tags)); ?>
        </div>
    </div>
<script>
    $(document).ready(function() 
    { 
        $("#myTable").tablesorter({
            headers: { 
            // assign the secound column (we start counting zero) 
            0: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            }, 
            // assign the third column (we start counting zero) 
            4: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            } 
        }
        }); 
    } 
); 
    </script>
</div>