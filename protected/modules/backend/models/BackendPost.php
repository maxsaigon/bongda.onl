<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/15/14
 * Time: 12:35 AM
 */

class BackendPost extends BaseUrl {
    /*
     * Get all Posts
     */
    public function getAllPosts($term = '', $start, $limit,$deleted)
    {
        $posts = Yii::app()->db->createCommand()
        ->select('u.*')
        ->from('url u')
        ->andWhere('u.is_deleted ='.$deleted);  
        if ($term != '' )
            $posts->andWhere('u.tile LIKE "%'.$term.'%"');
        return $posts->offset($start)
        ->limit($limit)
        ->order('u.created DESC')
        ->group('u.id')
        ->queryAll();
    }

    /*
     * Count all posts
     */
    public function countAllPosts($term = '',$deleted)
    {
        $posts = Yii::app()->db->createCommand()
            ->select('COUNT(DISTINCT(u.id))')
            ->from('url u')
            ->andWhere('u.is_deleted ='.$deleted);  
        if ($term != '' )
            $posts->andWhere('u.tile LIKE "%'.$term.'%"');
        return $posts->queryScalar();
    }

    /*
     * Get post
     */
    public function getPostByPostId($id)
    {
        return Yii::app()->db->createCommand()
            ->select('u.*,c.*,s.*')
            ->from('url u')
            ->join('category c','c.Id = u.category_id')
            ->join('site s','s.Id = c.SiteId')
            ->where('u.id = '.$id)
            ->queryRow();
    }
    
    public function updateIsHot($id)
    {
        $sql = 'UPDATE url SET is_hot = CASE WHEN id=('.$id.') THEN 1 ElSE 0 END;';
        return Yii::app()->db->createCommand($sql)->execute();
    }
} 