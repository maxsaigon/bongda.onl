<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 5/2/14
 * Time: 5:41 PM
 */

class BackendTag extends BaseTag {
    /*
     * Get tag by post id
     */
    public function getTagByPostId($postId)
    {
        return Yii::app()->db->createCommand()
            ->select('t.*')
            ->from('post_tag p_t')
            ->join('tag t', 't.id = p_t.tag_id')
            ->where('p_t.post_id = '.$postId)
            ->order('p_t.is_primay DESC')
            ->queryAll();
    }

    /*
     * Get all Posts
     */
    public function getAllTags($term = '', $start, $limit)
    {
        $tags = Yii::app()->db->createCommand()
            ->select('t.*')
            ->from('tag t');
        if ($term != '' )
            $tags->where('t.TAG LIKE "%'.$term.'%"');
        return $tags->order('t.tag_order DESC')
            ->offset($start)
            ->limit($limit)
            ->queryAll();
    }

    /*
     * Count all posts
     */
    public function countAllTags($term = '')
    {
        $tags = Yii::app()->db->createCommand()
            ->select('COUNT(DISTINCT(t.id))')
            ->from('tag t');
        if ($term != '' )
            $tags->where('t.TAG LIKE "%'.$term.'%"');
        return $tags->queryScalar();
    }
} 