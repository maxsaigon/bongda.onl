<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 5/2/14
 * Time: 11:05 PM
 */

class BackendConvertTag extends BaseTagConvert {
    /*
        * Get all tag
        */
    public function getAllTags($term = '', $start, $limit)
    {
        $tags = Yii::app()->db->createCommand()
            ->select('t.*')
            ->from('tag_convert t');
        if ($term != '' )
            $tags->where('t.tag_des LIKE "%'.$term.'%"');
        return $tags
            ->group('t.tag_des')
            ->offset($start)
            ->limit($limit)
            ->queryAll();
    }

    /*
     * Get all Posts
     */
    public function getAllTagsConvert($term = '', $start, $limit)
    {
        $tags = Yii::app()->db->createCommand()
            ->select('t.*')
            ->from('tag_convert t');
        if ($term != '' )
            $tags->where('t.tag_des LIKE "%'.$term.'%"');
        return $tags->order('t.tag_des')->offset($start)
            ->limit($limit)
            ->queryAll();
    }

    /*
     * Count all posts
     */
    public function countAllTags($term = '')
    {
        $tags = Yii::app()->db->createCommand()
            ->select('COUNT(DISTINCT(t.id))')
            ->from('tag_convert t');
        if ($term != '' )
            $tags->where('t.tag_des LIKE "%'.$term.'%"');
        return $tags->queryScalar();
    }
} 