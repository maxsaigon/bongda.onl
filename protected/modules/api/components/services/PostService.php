<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/4/14
 * Time: 10:18 AM
 */

class PostService {
    CONST ITEM_PER_PAGE = 10;

    protected $model;

    /**
     * construct class.
     */
    public function __construct() {
        $this->model = new UrlApi();
    }

    /*
     * Get all posts
     */
    public function getAllPosts($id = '', $tags = '', $page = 1, $limit = self::ITEM_PER_PAGE)
    {
        $start = ($page - 1) * $limit;
        // Search by tags        
        if ($tags != ''){
            $newTag = '';
            $arr = explode(',',$tags);
            foreach ($arr as $ar){
                if ($newTag == ''){
                    $newTag .= '"'.$ar.'"';
                }else{
                    $newTag .= ',"'.$ar.'"';
                }

            }

            // Get post
            $posts = $this->model->getAllPostsByTags($newTag, $start, $limit);
            // Count
            $count   = $this->model->countAllPostsByTags($newTag);
            $pager = array(
                'totalRecord' => $count,
                'itemPerPage' => $limit,
                'itemInPage' => count($posts),
                'currentPage' => $page
            );
            return array ('pager' => $pager , 'posts'=> $posts);
        }else{
            if ($id != ''){
                // Get post
                $posts = $this->model->getAllPosts($id, $start, $limit);
                return array('posts'=> $posts);
            }else{
                // Get post
                $posts = $this->model->getAllPosts('', $start, $limit);
                // Count
                $count   = $this->model->countAllPosts();
                $pager = array(
                    'totalRecord' => $count,
                    'itemPerPage' => $limit,
                    'itemInPage' => count($posts),
                    'currentPage' => $page
                );
                return array ('pager' => $pager , 'posts'=> $posts);
            }

        }
    }

    /*
     * Get hot posts
     */
    public function getHotPosts($page, $limit = self::ITEM_PER_PAGE)
    {
        $start = ($page - 1) * $limit;

        // Get post
        $posts = $this->model->getHotPosts($start, $limit);
        // Count
        $count   = $this->model->countHotPosts();
        $pager = array(
            'totalRecord' => $count,
            'itemPerPage' => $limit,
            'itemInPage' => count($posts),
            'currentPage' => $page
        );
        return array ('pager' => $pager , 'posts'=> $posts);

    }
    
} 