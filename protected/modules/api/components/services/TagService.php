<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/16/14
 * Time: 12:51 AM
 */

class TagService {
    CONST ITEM_PER_PAGE = 10;

    protected $model;

    /**
     * construct class.
     */
    public function __construct() {
        $this->model = new TagConvert();
    }

    /*
     * Get all Tag
     */
    public function getAllTags()
    {
        // Get all tags
        $tags = $this->model->getAllTag();

        return array('status'=> true, 'data' => $tags);
    }
} 