<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 5/31/14
 * Time: 11:04 AM
 */

class UserService {
    /*
     * Check login
     */
    public function checkLogin($token, $email)
    {
        // Call graph API
        $graph_url= "https://graph.facebook.com/me?fields=email&method=GET&access_token=" .$token;
        $data = file_get_contents($graph_url);
        $json_a=json_decode($data, true);
        if ($email == $json_a['email'] ){
            // Check email
            $model = new ApiUser();
            $user = $model->findByAttributes(array('email' => $email));
            if ($user){
                return true;
            }
        }
        return false;
    }
} 