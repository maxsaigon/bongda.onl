<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/16/14
 * Time: 12:51 AM
 */

class TagController extends BaseAPIController {
    /*
         * Get all tags
         */
    public function actionAll()
    {
        // Service
        $service = new TagService();
        $result = $service->getAllTags();
        return $this->renderJSON($result);
    }
} 