<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/4/14
 * Time: 9:52 AM
 */

class PostController extends BaseAPIController {
    /*
     * Get all posts
     */
    public function actionAll()
    {
        // Service
        $service = new PostService();
        if(Yii::app()->request->isPostRequest)
        {
            $data = file_get_contents("php://input");
            //decode json post input as php array:
            $data = CJSON::decode($data, true);
            $page = isset($data['page'])?(int)$data['page']:1;
            $pageSize = isset($data['pageSize'])?(int)$data['pageSize']:10;
            $tags = isset($data['tags'])?Utilities::clean($data['tags']):'';
            $posts = $service->getAllPosts('', $tags, $page, $pageSize);
        }else{
            // GET
            $id = isset($_GET['id'])?(int)$_GET['id']:'';
            $posts = $service->getAllPosts($id, '', 0, 10);
        }
        return $this->renderJSON(array('status' =>true, 'data' => $posts));
    }

    /*
     * Get all posts
     */
    public function actionHot()
    {
        // Service
        $service = new PostService();

        $page = isset($_GET['page'])?(int)$_GET['page']:1;
        $pageSize = isset($_GET['pageSize'])?(int)$_GET['pageSize']:10;
        $posts = $service->getHotPosts($page, $pageSize);

        return $this->renderJSON(array('status' =>true, 'data' => $posts));
    }
    
    public function actionHotPost()
    {
        $model = new UrlApi();
        $posts = $model->getOnlyHotPost();
        return $this->renderJSON(array('status' =>true, 'data' => $posts));
        
    }
} 