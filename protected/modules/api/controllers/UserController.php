<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 5/31/14
 * Time: 11:04 AM
 */

class UserController extends BaseAPIController {
    /*
     * Check login
     */
    public function actionLogin()
    {
        if(Yii::app()->request->isPostRequest)
        {
            $data = file_get_contents("php://input");
            //decode json post input as php array:
            $data = CJSON::decode($data, true);
            // Service
            $service = new UserService();
            $bool = $service->checkLogin($data['token'], $data['email']);
            if ($bool){
                return $this->renderJSON(array('status' =>true));
            }
            return $this->renderJSON(array('status' =>false));
        }
    }
} 