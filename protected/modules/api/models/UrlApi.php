<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/4/14
 * Time: 10:10 AM
 */

class UrlApi extends BaseUrl {
    /*
     * Get all posts
     */
    public function getAllPosts($id = '', $start, $limit)
    {
        if ($id != ''){
        return Yii::app()->db->createCommand()
            ->select('u.*')
            ->from('url u')
            ->andWhere('u.id =:id', array(':id' => $id))
                ->queryRow();
        }
        return Yii::app()->db->createCommand()
            ->select('u.id, u.tile as title, u.feature_image, u.created as post_date, u.description')
            ->from('url u')
            ->offset($start)
            ->limit($limit)
            ->order('u.id DESC')
            ->queryAll();
    }

    /*
     * Get all posts by tags
     */
    public function getAllPostsByTags($tags = '', $start, $limit)
    {
        if ($tags != ''){
            return  Yii::app()->db->createCommand()
                ->select('u.id,u.tile as title, u.feature_image, u.created as post_date, u.description')
                ->from('url u')
                ->join('post_tag p_t', 'p_t.post_id = u.id')
                ->join('tag t', 't.id = p_t.tag_id')
                ->where('t.Tag IN ('.$tags.')')
                ->offset($start)
                ->limit($limit)
                ->order('u.id DESC')
                ->group('u.id')
                ->queryAll();
        }
        return array();
    }

    /*
     * Get all hot posts
     */
    public function getHotPosts($start, $limit)
    {
        return  Yii::app()->db->createCommand()
            ->select('u.id,u.tile as title, u.feature_image, u.created as post_date, u.description')
            ->from('url u')
            ->where('u.is_hot = 1')
            ->offset($start)
            ->limit($limit)
            ->order('u.id DESC')
            ->queryAll();
    }
    
    
    public function getOnlyHotPost()
    {
        return  Yii::app()->db->createCommand()
            ->select('u.id,u.tile as title, u.feature_image, u.created as post_date, u.description')
            ->from('url u')
            ->where('u.is_hot = 1')
            ->queryRow();
    }

    /*
     * Count all posts
     */
    public function countAllPosts()
    {
        $posts =  Yii::app()->db->createCommand()
            ->select('COUNT(u.id) as count')
            ->from('url u');
        $count = $posts->queryRow();
        return $count['count'];
    }

    /*
     * Count all hot posts
     */
    public function countHotPosts()
    {
        return $this->count('is_hot = 1');
    }

    /*
     * Count all posts by tags
     */
    public function countAllPostsByTags($tags = '')
    {
        if ($tags != ''){
            $count = Yii::app()->db->createCommand()
                ->select('DISTINCT(u.id) as count')
                ->from('url u')
                ->join('post_tag p_t', 'p_t.post_id = u.id')
                ->join('tag t', 't.id = p_t.tag_id')
                ->where('t.Tag IN ('.$tags.')')
                ->order('u.id DESC')
                ->queryRow();
            return $count['count'];
        }
        return 0;
    }
} 