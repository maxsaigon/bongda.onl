<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/25/14
 * Time: 12:39 AM
 */
class FrontendPostServices {
    CONST ITEM_PER_PAGE = 10;

    protected $model;

    /**
     * construct class.
     */
    public function __construct() {
        $this->model = new FrontendUrl();
    }

    /*
     * Get post by page
     */
    public function getPosts($page = 1, $term = '', $tag = '')
    {
        $start = ($page-1) * self::ITEM_PER_PAGE;
        $term = Utilities::clean($term);
        // Get posts
        $posts = $this->model->getAllPosts($term, $tag, $start, self::ITEM_PER_PAGE);
        $count = count($posts);
        $tagModel = new FrontendTag();
        for ($i = 0; $i < $count; $i++){
            $posts[$i]['tagFc'] = $tagModel->getTagByPostId($posts[$i]['postId']);
        }
        // Count posts
        $count = $this->model->countAllPosts($term, $tag);
        $pager = array(
            'totalRecord' => $count,
            'itemPerPage' => self::ITEM_PER_PAGE,
            'itemInPage' => count($posts),
            'currentPage' => $page - 1
        );

        return array('posts' => $posts, 'pager' => $pager);
    }

    /*
     * Count
     */
    public function countPostByTag()
    {
        $tagConvertModel = new FrontendTagConvert();
        $tags = $tagConvertModel->getAllTag();
        $count = count($tags);
        for ($i = 0; $i< $count; $i++){
            $tags[$i]['count'] = $this->model->countAllPosts('', $tags[$i]['name']);
        }
        return $tags;
    }

    /*
     * Get related post
     */
    public function getRelatedPostByTag($tile,$tagId)
    {
        $posts = $this->model->getPostByTagId($tile,$tagId, 0, 4);
        return $posts;
    }
} 