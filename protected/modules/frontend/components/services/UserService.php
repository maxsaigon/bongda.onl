<?php
/**
 * Created by JetBrains PhpStorm.
 * User: WIN7
 * Date: 10/10/13
 * Time: 8:01 PM
 * To change this template use File | Settings | File Templates.
 */
class UserService
{
    protected $model;

    /**
     * construct class.
     */
    public function __construct() {
        $this->model = new FrontendUsers();
    }

    /*
     * Login by Facebook
     */
     public function loginByFacebook($url = '/')
    {
        $model = Yii::app()->getModule('hybridauth');
        $ha = $model->getHybridAuth();
        $ha->authenticate('Facebook','');
        $facebook = $ha->getAdapter('facebook');
        $profile = $facebook->getUserProfile();
        // User Model
        $userModel = new FrontendUsers();
        $user = $userModel->findByAttributes(array('email' => $profile->email));
        if(!$user)
        {
            $userProfile = $userModel;
            $userProfile->password = Yii::app()->params['default_pass'];
            //$userProfile->repeat_password = Yii::app()->params['default_pass'];
            $userProfile->password_salt = Yii::app()->params['default_pass'];
            $userProfile->rememberMe = 1;
            $userProfile->provider = 'Facebook';
            $userProfile->email = $profile->email;
            $userProfile->emailverified = $profile->emailVerified;
            $userProfile->firstname = $profile->firstName;
            $userProfile->language = $profile->language;
            $userProfile->lastname = $profile->lastName;
            $userProfile->displayname = $profile->displayName;
            $userProfile->gender = $profile->gender;
            $userProfile->description = $profile->description;
            $userProfile->identifier = $profile->identifier;
            $userProfile->profileurl = $profile->profileURL;
            $userProfile->photourl = $profile->photoURL;
            $userProfile->websiteurl = $profile->webSiteURL;
            $userProfile->country = $profile->country;
            $userProfile->phone = $profile->phone;
            $userProfile->zip = $profile->zip;
            $userProfile->city = $profile->city;
            $userProfile->birthday = $profile->birthDay;
            $userProfile->birthmonth = $profile->birthMonth;
            $userProfile->birthyear = $profile->birthYear;
            $userProfile->age = $profile->age;
            $userProfile->address = $profile->address;
            $userProfile->user_role = 0;
            
            if ($userProfile->save(false)){
                $auth = $userProfile->authenticate('social');
                if ($auth['status']){
                    $ha->redirect($url);
                    return true;
                }
            }else{
                // Fail to save
                return false;
            }
        }else{
            $auth = $user->authenticate('social');
            if ($auth['status']){
                $ha->redirect($url);
                return true;
            }
        }
        return false;
    }
    /*
    * Login by Google
    */
    public function loginByGoogle($url = '/')
    {
        $model = Yii::app()->getModule('hybridauth');
        $ha = $model->getHybridAuth();
        $ha->authenticate('google','');
        $google = $ha->getAdapter('google');
        $profile = $google->getUserProfile();
        // User Model
        $userModel = new FrontendUsers();
        $user = $userModel->findByAttributes(array('email' => $profile->email));
        if(!$user)
        {
            $userProfile = $userModel;
            $userProfile->password = Yii::app()->params['default_pass'];
            //$userProfile->repeat_password = Yii::app()->params['default_pass'];
            $userProfile->password_salt = Yii::app()->params['default_pass'];
            $userProfile->rememberMe = 1;
            $userProfile->provider = 'Google';
            $userProfile->email = $profile->email;
            $userProfile->emailverified = $profile->emailVerified;
            $userProfile->firstname = $profile->firstName;
            $userProfile->language = $profile->language;
            $userProfile->lastname = $profile->lastName;
            $userProfile->displayname = $profile->displayName;
            $userProfile->gender = $profile->gender;
            $userProfile->description = $profile->description;
            $userProfile->identifier = $profile->identifier;
            $userProfile->profileurl = $profile->profileURL;
            $userProfile->photourl = $profile->photoURL;
            $userProfile->websiteurl = $profile->webSiteURL;
            $userProfile->country = $profile->country;
            $userProfile->phone = $profile->phone;
            $userProfile->zip = $profile->zip;
            $userProfile->city = $profile->city;
            $userProfile->birthday = $profile->birthDay;
            $userProfile->birthmonth = $profile->birthMonth;
            $userProfile->birthyear = $profile->birthYear;
            $userProfile->age = $profile->age;
            $userProfile->address = $profile->address;
            $userProfile->user_role = 0;

            if ($userProfile->save(false)){
                $auth = $userProfile->authenticate('social');
                if ($auth['status']){
                    $ha->redirect($url);
                    return true;
                }
            }else{
                // Fail to save
                return false;
            }
        }else{
            $auth = $user->authenticate('social');
            if ($auth['status']){
                $ha->redirect($url);
                return true;
            }
        }
        return false;
    }
    /*
     * Register User
     */
    public function register($email, $password, $name)
    {
        $name = Utilities::clean($name);
        // 1. validate data - email
        $check = Utilities::checkEmail($email);
        if (!$check){
            return array('status' => false, 'msg' => 'Email của bạn không hợp lệ');
        }
        // 2. Check email exist
        $user = $this->model->findByAttributes(array('user_email' => $email));
        if ($user){
            return array('status' => false, 'msg' => 'Địa chỉ email này đã tồn tại trong hệ thống');
        }
        // 4. hash password
        $hash = new PasswordHash(8, true);
        $passHash = $hash->HashPassword(trim($password));
        // 5. Insert to database
        $key = md5($email.time()) ; // Generate key
        $id = $this->model->register($name, $email, $passHash, $key);
        // Check insert
        if ($id == 0){
            return array('status' => false, 'msg' => 'Chức năng đăng ký bị lỗi');
        }
        // Return
        return array('status' => true, 'msg' => 'Vui lòng kiểm tra email của bạn, chúng tôi đã gửi một email thông báo đến hộp thư của bạn');
    }

    /*
     * Forgot password
     */
    public function forgot($email)
    {
        // Step 1. Check email exist
        $user = $this->model->findByAttributes(array('user_email' => $email));
        // Don't exist
        if (!$user){
            return array('status' => false, 'msg' => SiteConstant::EMAIL_ERROR);
        }
        // Step 2. Insert key and change status to 2
        $key = md5($email.time()) ; // Generate key
        $id = $this->model->setForgotPass($email, SiteConstant::FORGET_STATUS, $key);
        // Check insert
        if ($id == 0){
            return array('status' => false, 'msg' => SiteConstant::ERROR);
        }
        $user->user_activation_key = $key;
        // Return
        return array('status' => true, 'msg' => SiteConstant::SUCCESS, 'user' => $user);
    }


    /*
     * Request new password
     */
    public function requestNewPassword($userId, $token)
    {
        // Step 1. Validate input - Check user ID is number type, clean token
        //Check Id
        $check = is_numeric($userId);
        if (!$check){
            return array('status' => false, 'msg' => SiteConstant::ERROR);
        }
        //Check Token
        $clean = Utilities::clean($token);
        if(!$clean){
            return array('status' => false, 'msg' => SiteConstant::ERROR);
        }
            
        // Step 2. Check user ID and token
        $user = $this->model->findByAttributes(array('ID' => $userId, 'user_activation_key' => $token));
        if ($user){
            if ($user->user_status == SiteConstant::FORGET_STATUS){ // Status =  2 is user pending new password
                // Random new pass
                $newPass = Utilities::randomString(10);
                // Hash
                $hash = new PasswordHash(8, true);
                $passHash = $hash->HashPassword($newPass);
                $user->user_pass = $passHash;
                $user->user_activation_key = '';
                $user->user_status = SiteConstant::NORMAL_STATUS;
                if ($user->save()){
                    // Return
                    return array('status' => true, 'msg' => SiteConstant::SUCCESS, 'pass' => $newPass, 'user' => $user);
                }
            }
        }
        // Return
        return array('status' => false, 'msg' => SiteConstant::ERROR);
    }

    /*
     * Active User
     */
    public function activeUser($userId, $token)
    {
        // Step 1. Validate input - Check user ID is number type, clean token
        //Check Id
        $check = Utilities::checkNumber($userId);
        if (!$check){
        return array('status' => false, 'msg' => SiteConstant::ERROR);
        }
        //Check Token
        $clean = Utilities::clean($token);
        if(!$clean){
            return array('status' => false, 'msg' => SiteConstant::ERROR);
        }

        // Step 2. Check user ID and token
        $user = $this->model->findByAttributes(array('ID' => $userId, 'user_activation_key' => $token));
        if ($user){
            if ($user->user_status == SiteConstant::ACTIVE_STATUS){ // Status =  1 is user pending active
                $user->user_status = SiteConstant::NORMAL_STATUS;
                $user->user_activation_key = '';
                if ($user->save()){
                    // Return
                    return array('status' => true, 'msg' => SiteConstant::SUCCESS);
                }
            }
        }
        // Return
        return array('status' => false, 'msg' => SiteConstant::ERROR);
    }

    /*
     * Edit profile
     */
    public function editProfile($post)
    {
        $userId = Yii::app()->user->getId();
        $user = BaseUsers::model()->findByPk($userId);
        // Validate email
        $check = Utilities::checkEmail($post['email']);
        if (!$check){
            return array('status' => false, 'msg' => 'Vui lòng nhập lại email');
        }
        // Validate date input
        $checkDate = Utilities::checkDate($post['birthday']);
        if (!$check){
            return array('status' => false, 'msg' => 'Vui lòng kiểm tra ngày tháng năm sinh');
        }
        // If exist
        if ($user)
        {
            // Validate password
            // Hash
            $hash = new PasswordHash(8, true);
            $passHash = $hash->HashPassword(trim($post['new_pass1']));
            $checkPass = $hash->CheckPassword($user->user_pass, trim($post['old_pass']));
            if ($checkPass && $post['old_pass'] != '')
            {
                return array('status' => false, 'msg' => 'Mật khẩu bạn nhập không đúng');
            }
            if (Utilities::clean($post['new_pass1']) != Utilities::clean($post['new_pass2']) && $post['new_pass2'] != '')
            {
                return array('status' => false, 'msg' => 'Mật khẩu mới không hợp lệ');
            }
            $user->display_name = Utilities::clean($post['display_name']);
            $user->user_des = Utilities::clean($post['user_des']);
            $user->user_email = Utilities::clean($post['email']);
            $user->user_job = Utilities::clean($post['user_job']);
            // Change password
            if ($post['new_pass1'] != '' && $post['new_pass2'] != '' && $post['old_pass'] != '')
            {
                $user->user_pass = $passHash;
                $user->user_url = Utilities::clean($post['profileurl']);
            }
            // Save
            if ($user->save()){
                $userProfile = BaseUserProfile::model()->findByAttributes(array('user_id' => $userId));
                if (!$userProfile){
                    $userProfile = new BaseUserProfile();
                    $userProfile->provider = 'site';
                    $userProfile->object_sha = 'xxx';
                    $userProfile->identifier = 'xxx';
                    $userProfile->photourl = Yii::app()->params['default_avatar'];
                    $userProfile->description = '';
                    $userProfile->firstname = '';
                    $userProfile->lastname = '';
                    $userProfile->language = '';
                    $userProfile->age = 0;
                    $userProfile->phone = '';
                    $userProfile->address = '';
                    $userProfile->country = '';
                    $userProfile->city = '';
                    $userProfile->zip = '';
                    $userProfile->description = '';
                }
                $userProfile->profileurl = Utilities::clean($post['profileurl']);
                $userProfile->email = $post['email'];
                $userProfile->emailverified = $post['email'];
                $userProfile->displayname = Utilities::clean($_POST['display_name']);
                $userProfile->gender = Utilities::clean($post['gender']);
                $userProfile->region = Utilities::clean($post['region']);
                // Check date
                $birth = Utilities::clean($post['birthday']);
                $date = explode('-', $birth);
                $userProfile->birthyear = $date[0];
                $userProfile->birthmonth = $date[1];
                $userProfile->birthday = $date[2];
                // Save profile
                if ($userProfile->save())
                {
                    // Model
                    $userModel = new Users();
                    $userInfo = $userModel->getUserByUserLogin($user->user_login);
                    Yii::app()->user->setUser($userInfo);
                    return array('status' => true, 'msg' => 'Lưu thông tin thành công!');
                }
            }
        }
        return array('status' => false, 'msg' => 'Lưu thất bại');
    }

}