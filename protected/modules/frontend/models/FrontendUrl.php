<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/25/14
 * Time: 12:39 AM
 */

class FrontendUrl extends BaseUrl {
    /*
         * Get all Posts
         */
    public function getAllPosts($term = '', $tag = '', $start, $limit)
    {
        $posts = Yii::app()->db->createCommand()
            ->select('u.*,c.*,s.*, t.*, u.id as postId')
            ->from('url u')
            ->join('category c','c.Id = u.category_id')
            ->join('site s','s.Id = c.SiteId')
            ->leftJoin('post_tag p_t', 'p_t.post_id = u.id')
            ->join('tag t', 't.id = p_t.tag_id')
            ->where('u.is_reviewed = 1')
            ->andWhere('u.is_deleted = 0');
        if ($term != '' )
            $posts->andWhere('u.title LIKE "%'.$term.'%"');
        if ($tag != '')
            $posts->andWhere('t.Tag = "'.$tag.'"');
        return $posts->offset($start)
            ->limit($limit)
            ->order('u.created DESC, t.tag_order DESC')
            ->group('u.tile')
            ->queryAll();
    }

    /*
     * Count all posts
     */
    public function countAllPosts($term = '', $tag = '')
    {
        $posts = Yii::app()->db->createCommand()
            ->select('COUNT(DISTINCT(u.id))')
            ->from('url u')
            ->join('post_tag p_t', 'p_t.post_id = u.id')
            ->join('tag t', 't.id = p_t.tag_id')
            ->where('u.is_reviewed = 1')
            ->andWhere('u.is_deleted = 0');
        if ($term != '' )
            $posts->andWhere('u.title LIKE "%'.$term.'%"');
        if ($tag != '')
            $posts->andWhere('t.Tag = "'.$tag.'"');
        return $posts->queryScalar();
    }

    /*
         * Get all Posts
         */
    public function getPostByPostId($postId)
    {
        return Yii::app()->db->createCommand()
            ->select('u.*,c.*,s.*, t.*, u.id as postId')
            ->from('url u')
            ->join('category c','c.Id = u.category_id')
            ->join('site s','s.Id = c.SiteId')
            ->leftJoin('post_tag p_t', 'p_t.post_id = u.id')
            ->join('tag t', 't.id = p_t.tag_id')
            ->where('u.is_reviewed = 1')
            ->andWhere('u.is_deleted = 0')
            ->andWhere('u.id =:id', array(':id' => $postId))
            ->queryRow();
    }

    /*
         * Get all Posts
         */
    public function getPostByTagId($tile,$tagId, $start, $limit)
    {
        return Yii::app()->db->createCommand()
            ->select('u.*,c.*,s.*, t.*, u.id as postId')
            ->from('url u')
            ->join('category c','c.Id = u.category_id')
            ->join('site s','s.Id = c.SiteId')
            ->leftJoin('post_tag p_t', 'p_t.post_id = u.id')
            ->join('tag t', 't.id = p_t.tag_id')
            ->where('u.is_reviewed = 1')
            ->andWhere('u.is_deleted = 0')  
            ->andWhere('p_t.tag_id=:tagId', array(':tagId' => $tagId))
            ->andWhere('u.tile !=:title',array(':title' => $tile))
            ->offset($start)
            ->limit($limit)
            ->order('u.id DESC')
            ->queryAll();
    }
} 