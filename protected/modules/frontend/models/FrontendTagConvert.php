<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 5/3/14
 * Time: 2:57 AM
 */

class FrontendTagConvert extends BaseTagConvert {
    /*
         * Get all tag
         */
    public function getAllTag()
    {
        return Yii::app()->db->createCommand()
            ->select('t_c.tag_des as name, t.tag_image as image')
            ->from('tag_convert t_c')
            ->join('tag t', 't.Name = t_c.tag_des')
            ->group('t_c.tag_des')
            ->queryAll();
    }
} 