<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 5/3/14
 * Time: 2:48 AM
 */

class FrontendTag extends BaseTag {
    /*
         * Get tag by post id
         */
    public function getTagByPostId($postId)
    {
        return Yii::app()->db->createCommand()
            ->select('t.*')
            ->from('post_tag p_t')
            ->join('tag t', 't.id = p_t.tag_id')
            ->where('p_t.post_id = '.$postId)
            ->order('t.tag_level ASC ,p_t.is_primay DESC')
            ->queryRow();
    }
} 