<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 5/18/14
 * Time: 11:39 PM
 */

class FrontendUsers extends BaseUsers {
    /**
     * Validate email
     * @return boolean
     */
    public function validateEmail($email)
    {
        $user = Yii::app()->db->createCommand()
            ->select('u.email')
            ->from('users u')
            ->where('u.email=:email', array(':email' => Utilities::clean($email)))
            ->queryRow();
        if ($user)
            return false;
        return true;
    }

    /**
     * Authentication user login
     * @return
     */
    public function authenticate($type = 'none')
    {
        if(!$this->hasErrors()) // we only want to authenticate when no input errors
        {
            $identity = new UserIdentity($this->email,$this->password);
            if ($type == 'none')
                $result = $identity->authenticate();
            if ($type == 'social')
                $result = $identity->authenBySocial();
            switch($result)
            {
                case UserIdentity::ERROR_NONE:
                    $duration = $this->rememberMe == 1 ? 3600*24*10 : 0; // 10 days
                    Yii::app()->user->login($identity,$duration);
                    return array('status' => true);
                case UserIdentity::ERROR_USERNAME_INVALID:
                    return array('status' => false, 'msg' => 'Bạn nhập sai email');
                case UserIdentity::USER_IS_DISABLED:
                    return array('status' => false, 'msg' => 'Tài khoản của bạn đã bị khóa');
                case UserIdentity::SOMETHING_WRONG:
                    return array('status' => false, 'msg' => 'Có lỗi xảy ra');
                case UserIdentity::NEED_ACTIVE:
                    return array('status' => false, 'msg' => 'Bạn cần active tài khoản bằng email');
                default: // UserIdentity::ERROR_PASSWORD_INVALID
                    return array('status' => false, 'msg' => 'Bạn nhập sai mật khẩu');
            }
        }
        return array('status' => false, 'msg' => 'error');
    }
    
    /*
     * Get user
     */
    public function getUserByUserLogin($name)
    {
        return Yii::app()->db->createCommand()
            ->select('u.*')
            ->from('users u')
            ->andWhere('u.email=:name', array(':name' => Utilities::clean($name)))
            ->queryRow();
    }
    
} 