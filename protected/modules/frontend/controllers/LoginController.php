<?php
/**
 * Created by JetBrains PhpStorm.
 * User: WIN7
 * Date: 10/9/13
 * Time: 7:46 PM
 * To change this template use File | Settings | File Templates.
 */
class LoginController extends FrontendController
{
    /*
     * Check login
     */
    public function actionCheck()
    {
        // Check post
        if (isset($_POST['user']) && isset($_POST['pass']))
        {
            $userModel = new Users();
            // Normal login
            $userModel->user_login = $_POST['user'];
            $userModel->user_pass = $_POST['pass'];
            // Check remember
            if (isset($_POST['rememberme']) && $_POST['rememberme'] == 1)
            {
                $userModel->rememberMe = 1;
            }
            // validate user input and redirect to previous page if valid
            $auth = $userModel->authenticate();
            if ($auth['status']){
                return $this->renderJSON(array('status' => true, 'msg' => Yii::app()->request->urlReferrer));
            }
            return $this->renderJSON($auth);
        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        Yii::app()->getModule('hybridauth')->getHybridAuth()->getAdapter('facebook')->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    /*
     * Authen with provider
     */
    public function actionOauth() {
        if(isset($_GET['provider']))
        {
            $callBack = isset($_GET['redirect'])?$_GET['redirect']:'/';
            $service = new UserService();
            if ($_GET['provider'] == 'facebook')
            {
                $check = $service->loginByFacebook($callBack);
                return $this->redirect('/');
            }
            
            elseif ($_GET['provider'] == 'google'){
                $check = $service->loginByGoogle($callBack);
                return $this->redirect('/');
            }
        }else{
            return $this->redirect('/');
        }
    }

    /*
     * Register user
     */
    public function actionRegister()
    {
        // Check POST user_login, user_email, user_pass
        if (isset($_POST['pass'])&& isset($_POST['user_email']) && isset($_POST['name']))
        {
            // Service
            $service = new UserService;
            $result = $service->register( $_POST['user_email'], $_POST['pass'], $_POST['name']);
            // Send mail
            if ($result['status'])
            {
                // Model
                $model = new NewsUsers();
                $user = $model->findByAttributes(array('user_email' => $_POST['user_email']));
                // Setup params for send email
                $params = array('params'=> array(
                    'name' => $user['display_name'],
                    'url' => Yii::app()->params['site_url'].'login/request?userId='.$user['ID'].'&token='.$user['user_activation_key'],
                ));
                // Send email
                $message = new YiiMailMessage;
                //this points to the file test.php inside the view path
                $message->view = "active_user";
                $message->subject = 'Kích hoạt tài khoản';
                $message->setBody($params, 'text/html');
                $message->addTo($_POST['user_email']);
                $message->from = Yii::app()->params['mail'];
                Yii::app()->mail->send($message);
                return $this->redirect('/login/success/');
            }
            return $this->renderJSON($result);
        }
        return $this->renderJSON(array('status' => false, 'msg' => 'Wrong request'));
    }

    /*
     * Forgot password
     */
    public function actionForgot()
    {
        // Check GET value
        if (isset($_POST['user_email'])&& Yii::app()->request->isAjaxRequest )
        {
            // Service
            $service = new UserService();
            $result = $service->forgot($_POST['user_email']);
            // Send mail
            if ($result['status'])
            {
                // Setup params for send email
                $params = array('params'=> array(
                    'name' => $result['user']['display_name'],
                    'url' => Yii::app()->params['site_url'].'login/active?userId='.$result['user']['ID'].'&token='.$result['user']['user_activation_key'].'/',
                ));
                // Send email
                $message = new YiiMailMessage;
                //this points to the file test.php inside the view path
                $message->view = "for_got_password";
                $message->subject = 'Phục hồi mật khẩu';
                $message->setBody($params, 'text/html');
                $message->addTo($_POST['user_email']);
                $message->from = Yii::app()->params['mail'];
                Yii::app()->mail->send($message);
            }
            return $this->renderJSON($result);
        }
        // Layout
        $this->layout = '../layouts/news_style';
        // Title
        $this->pageTitle = Yii::app()->params['news_title'];
        // Description
        $this->pageDescription = Yii::app()->params['news_description'];
        // Service
        $service = new PostService();
        // Get post of category
        $postDoXe = $service->getPostByCategory(4,0,1);
        // Get new post
        $newPosts = $service->getNewPost(0,3);
        // Return array
        return $this->render('forgot', array('postDoXe' => $postDoXe, 'newPosts' => $newPosts,));
    }

    /*
     * Request password
     */
    public function actionRequest()
    {
        // Check GET value
        if (isset($_GET['userId']) && isset($_GET['token']))
        {
            $service = new UserService();
            $result = $service->requestNewPassword($_GET['userId'], $_GET['token']);
            // Send mail
            if ($result['status'])
            {
                // Setup params for send email
                $params = array('params'=> array(
                    'name' => $result['user']['display_name'],
                    'pass' => $result['pass']
                ));
                // Send email
                $message = new YiiMailMessage;
                //this points to the file test.php inside the view path
                $message->view = "for_got_password";
                $message->subject = 'Khởi tạo mật khẩu mới';
                $message->setBody($params, 'text/html');
                $message->addTo($result['user']['user_email']);
                $message->from = Yii::app()->params['mail'];
                Yii::app()->mail->send($message);
            }
            // render
            return ;
        }
        return $this->redirect('/');
    }

    /*
     * Active user
     */
    public function actionActive()
    {
        // Check GET value
        if (isset($_GET['userId']) && isset($_GET['token']))
        {
            $service = new UserService();
            $result = $service->activeUser($_GET['userId'], $_GET['token']);
            // render
            return ;
        }
        return $this->redirect('/');
    }

    /*
     * Test send mail
     */
    public function actionTest()
    {
        $mail = new YiiMailer();
        $mail->setFrom(Yii::app()->params['mail'], Yii::app()->params['news_title'].' - Register account');
        $mail->setTo('wind.0401@gmail.com');
        $mail->setSubject('Register');
        $mail->setBody('Simple message');
        $mail->send();
    }
    
    /*
     * reigister success
     */
    public function actionSuccess()
    {
        $email = Yii::app()->request->getQuery('email');
        // Layout
        $this->layout = '../layouts/news_style';
        // Title
        $this->pageTitle = Yii::app()->params['news_title'];
        // Description
        $this->pageDescription = Yii::app()->params['news_description'];

        $this->render( 'success', array('email' => $email));
    }    
}
