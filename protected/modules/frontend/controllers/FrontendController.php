<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/23/14
 * Time: 12:43 AM
 */

class FrontendController extends ControllerBase
{
    public $baseScriptUrl;
    /*
     * Before action
     */
    public function beforeAction($action)
    {
        Yii::app()->clientScript->registerScript('globals-jquery',"
            var IMG_UPLOAD_URL = '".$this->createUrl('/backend/image/')."upload';
            var VALIDATE_EMAI_URL = '".$this->createUrl('/frontend/site/validateEmail/')."';
        ",CClientScript::POS_BEGIN);
        // Title
        $this->pageTitle = Yii::app()->params['title'];
        // Description
        $this->pageDescription = Yii::app()->params['description'];
        // Register js & css
        if( !Yii::app()->request->isAjaxRequest )
        {
            $this->baseScriptUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.frontend.assets'));
            // Register css for tag
            Yii::app()->clientScript->registerCssFile($this->baseScriptUrl .'/js/bootstrap/css/bootstrap.min.css');
            Yii::app()->clientScript->registerCssFile($this->baseScriptUrl .'/js/font-awesome-4.0.3/css/font-awesome.min.css');
            Yii::app()->clientScript->registerCssFile($this->baseScriptUrl.'/css/style.css');
            // Register Js
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/jquery-1.10.2.min.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/holder.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/bootstrap/js/bootstrap.min.js',CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScript('global-jquery','
                $(document).ready(function(){

                });
            ',CClientScript::POS_END);
        }
        return parent::beforeAction($action);
    }
    /**
     * Return data to browser as JSON
     * @param array $data
     */
    protected function renderJSON($data)
    {
        header('Content-type: application/json');
        echo CJSON::encode($data);
        foreach (Yii::app()->log->routes as $route) {
            if($route instanceof CWebLogRoute) {
                $route->enabled = false; // disable any weblogroutes
            }
        }
        Yii::app()->end();
    }
} 