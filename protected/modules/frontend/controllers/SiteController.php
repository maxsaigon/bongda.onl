<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/23/14
 * Time: 12:44 AM
 */

class SiteController extends FrontendController {
    /*
     * Index page
     */
    public function actionIndex()
    {
        $this->layout = '../layouts/frontend_style';
        // Title
        $this->pageTitle = Yii::app()->params['news_title'];
        // Description
        $this->pageDescription = Yii::app()->params['news_description'];
        // Register js
        Yii::app()->clientScript->registerScriptFile($this->baseScriptUrl . '/js/page/index.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScript('indexjs', '
            var baseUrl = "' . Yii::app()->request->baseUrl . '";
            var index = new index();
            index.constructor();'
            , CClientScript::POS_END
        );
        // Count
        $service = new FrontendPostServices();
        $count = $service->countPostByTag();
        return $this->render('index', array(
            'count' => $count
        ));
    }

    /*
     * Detail page
     */
    public function actionDetail($id = '')
    {
        $this->layout = '../layouts/frontend_style';
        if ($id != '' && is_numeric($id)) {
            $model = new FrontendUrl();
            $post = $model->getPostByPostId($id);
            if (!empty($post)) {
                // Title
                $this->pageTitle = $post['tile'];
                // Description
                $this->pageDescription = $post['description'];
                // tag
                $tagModel = new FrontendTag();
                $tags = $tagModel->getTagByPostId($id);
                // Count
                $service = new FrontendPostServices();
                $count = $service->countPostByTag();
                // Get related post
                $relatedPosts = array();
                if (!empty($tags))
                    $relatedPosts = $service->getRelatedPostByTag($post['tile'],$tags['id']);
                // Render
                return $this->render('detail', array(
                    'post' => $post,
                    'tags' => $tags,
                    'count' => $count,
                    'relatedPosts' => $relatedPosts
                ));
            }
        }
        $this->redirect('/');

    }

    /*
     * Ajax load
     */
    public function actionAjax()
    {
        if (isset($_POST)){
            $page = isset($_POST['page'])?(int)$_POST['page']:1;
            $term = isset($_POST['term'])?Utilities::clean($_POST['term']):'';
            $tag = isset($_POST['tag'])?Utilities::clean($_POST['tag']):'';
            // Services
            
            $service = new FrontendPostServices();
            $posts = $service->getPosts($page, $term, $tag);
            return $this->renderPartial('post_list', array(
                'posts' => $posts
            ));
        }
    }

    /*
     * Get content of post
     */
    public function actionAjaxContent()
    {
        if (isset($_POST['postId']) && is_numeric($_POST['postId'])){
            // Model
            $model = new FrontendUrl();
            $post = $model->findByPk($_POST['postId']);
            if ($post){
                echo $post->content;die;
            }
        }
        echo '';die;
    }

    /**
     * Validate email
     */
    public function actionValidateEmail()
    {
        if (isset($_POST['email']))
        {
            // Model
            $model = new FrontendUsers();
            $result = $model->validateEmail($_POST['email']);
            return $this->renderJSON($result);
        }
        return $this->renderJSON(false);
    }
} 