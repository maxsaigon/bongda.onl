<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/23/14
 * Time: 12:50 AM
 */
?>
<?php $this->renderPartial('header', array()); ?>  
<div class="clearfix"></div>
<div class="bc">
    <div class="container">
        <div class="home row">
            <div class="col-sm-10">
                <article class="row">
                    <div class="col-md-8">
                        <div class="content">
                            <div class="media">
                                <div class="media-body">
                                    <h4 class="heading"><a href="#"><?php echo $post['tile'] ?></a></h4>
                                    <ul class="list-inline text-muted media-info">
                                        <li><?php echo Utilities::timeSince($post['created']); ?></li>
                                        <li><a href=""><?php echo $post['name'] ?></a></li>
                                        <li><?php echo $post['SiteName'] ?></li>
                                        <?php if (!Yii::app()->user->isGuest && !Yii::app()->user->isUser()): ?>
                                        <li>
                                            <a class="btn purple btn-xs"
                                               href="<?php echo $this->createUrl('/backend/post/detail') . '?postId=' . $post['postId']; ?>"
                                               target="_blank">
                                                Sửa Bài Viết
                                            </a>
                                        </li>
                                    <?php endif ?>
                                    </ul>
                                    <?php if($post['type'] == 'video'): ?>
                                        <div class="text">
                                        <p><?php echo $post['content']; ?></p>
                                        </div>                     
                                    <?php elseif($post['type'] == 'photo'): ?>
                                        <div class="media-ofject center-block">
                                        <img class="img-responsive" src="<?php echo Yii::app()->params['site_url'].$post['feature_image'] ?>">
                                        </div>
                                    <?php else : ?>
<!--
                                        <div class="media-ofject center-block">
                                            <img class="img-responsive" src="<?php echo Yii::app()->params['site_url'].Utilities::addImageEx($post['feature_image'],500,280) ?>">
                                        </div>
-->
                                        <div class="text">
                                            <p><?php echo $post['content']; ?></p>
                                        </div>
                                    <?php endif ?>
                                    <ul class="action list-inline">
                                        <li><div class="fb-like" data-href="<?php echo Yii::app()->params['site_url'].$post['postId'] ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></li>
                                        <li class="pull-right"><fb:comments-count href=<?php echo Yii::app()->params['site_url'].$post['postId'];  ?>></fb:comments-count> <i class="fa fa-comment"></i></li>
                                    </ul>
                                    <div class="action-data">
                                        <div id="comment" class="collapse in comment">
                                            <div class="fb-comments" data-href="<?php echo Yii::app()->params['site_url'].$post['postId'] ?>" data-numposts="5" data-colorscheme="light"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 hidden-xs">
                        <div class="club">
                            <?php if (!empty($tags)): ?>
                                <div class="club-head text-center">
                                    <h5><?php echo $tags['Name']; ?></h5>
                                </div>
                                <div class="center-block text-center club-img">
                                    <img class="img-responsive" src="<?php echo Yii::app()->params['site_url'].Utilities::addImageEx($tags['tag_image'],200,200); ?>" width="100">
                                </div>
                                <ul class="list-inline club-info text-center">
                                    <li><button class="btn btn-xs btn-info"><i class="fa fa-heart"></i> Fan</button></li>
                                    <li><button class="btn btn-xs btn-success"><i class="fa fa-rss"></i> Read</button></li>
                                    <li><button class="btn btn-xs btn-warning"><i class="fa fa-thumbs-o-down"></i> Anti</button></li>
                                </ul>
                            <?php endif; ?>
                        </div>
                        
                        <div class="related">
                        <?php
                            $i = 0;
                            foreach ($relatedPosts as $post):
                                if ($i == 0):
                        ?>
                            <article class="top">
                                <a href="<?php echo Yii::app()->params['site_url'].$post['postId'];  ?>">
                                    <img class="img-responsive" src="<?php echo Yii::app()->params['site_url'].Utilities::addImageEx($post['feature_image'],285,159) ?>" width="295px">
                                </a>
                                <div class="detail">
                                    <h5><a href="<?php echo Yii::app()->params['site_url'].$post['postId'];  ?>"><?php echo $post['tile'] ?></a></h5>
                                </div>
                            </article>   
                            <ul class="list-unstyled">
                            <?php else: ?>
                                <li>
                                    <article>
                                        <img class="thumbnail pull-left" src="<?php echo Yii::app()->params['site_url'].Utilities::addImageEx($post['feature_image'],200,200) ?>" width="50px" height="50px">
                                        <h6>
                                            <a href="<?php echo Yii::app()->params['site_url'].$post['postId'];  ?>">
                                                <?php echo $post['tile'] ?>
                                            </a>
                                        </h6>
                                    </article>
                                </li>
                                <?php
                                    endif;
                                    $i++;
                                    endforeach
                                ?>
                            </ul>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-sm-2 hidden-xs">
                <?php $this->renderPartial('right', array('count' => $count)); ?>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="foot hidden-xs">
    <?php $this->renderPartial('footer', array()); ?>
</div>