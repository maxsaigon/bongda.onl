<?php foreach($posts['posts'] as $post): ?>
    <article class="row row-list">
        <div class="col-sm-8 col-md-8 col-xs-12">
            <div class="content">
                <div class="media">
                    <div class="media-body">
                        <h4 class="heading"><a href="<?php echo Yii::app()->params['site_url'].$post['postId'];  ?>"><?php echo $post['tile'] ?></a></h4>
                        <ul class="list-inline text-muted media-info">
                            <li><?php echo Utilities::timeSince($post['created']); ?></li>
                            <li><a href=""><?php echo $post['name'] ?></a></li>
                            <li><?php echo $post['SiteName'] ?></li>
                        </ul>
                        <?php if($post['type'] == 'video'): ?>
                            <div class="media-ofject center-block">
                                <div class="embed-responsive embed-responsive-16by9">
                                <?php echo $post['content']; ?>
                                </div>
                            </div>                      
                        <?php elseif($post['type'] == 'photo'): ?>
                            <div class="media-ofject center-block">
                            <a href="<?php echo Yii::app()->params['site_url'].$post['postId'];  ?>">
                                <img class="img-responsive thumbnail" src="<?php echo Yii::app()->params['site_url'].$post['feature_image'] ?>">
                            </a>
                        </div>
                        <?php else : ?>
                        <div class="media-ofject center-block">
                            <a href="<?php echo Yii::app()->params['site_url'].$post['postId'];  ?>">
                                <img class="img-responsive thumbnail" src="<?php echo Yii::app()->params['site_url'].Utilities::addImageEx($post['feature_image'],500,280) ?>">
                            </a>
                        </div>
                        <div class="sdes">
                            <?php echo Utilities::splitContent($post['description'], 400); ?>
                            <a href="<?php echo Yii::app()->params['site_url'].$post['postId'];  ?>" class="load_more">đọc thêm</a>
                        </div>
                        <?php endif ?>                     
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-md-4 hidden-xs">
            <div class="club">
                <?php if (!empty($post['tagFc'])): ?>
                    <div class="club-head text-center">
                        <h5><?php echo $post['tagFc']['Name']; ?></h5>
                    </div>
                    <div class="center-block text-center club-img">
                        <img class="img-responsive" src="<?php echo Yii::app()->params['site_url'].Utilities::addImageEx($post['tagFc']['tag_image'],200,200); ?>" width="100">
                    </div>
                    <ul class="list-inline club-info text-center">
                        <li><button class="btn btn-xs btn-info"><i class="fa fa-heart"></i> Fan</button></li>
                        <li><button class="btn btn-xs btn-success readmore" data-name="<?php echo $post['tagFc']['Tag']; ?>"><i class="fa fa-rss"></i> Read</button></li>
                        <li><button class="btn btn-xs btn-warning"><i class="fa fa-thumbs-o-down"></i> Anti</button></li>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </article>
<?php endforeach; ?>