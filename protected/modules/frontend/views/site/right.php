<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/27/14
 * Time: 11:43 AM
 */
?>
<div class="flow">
    <div class="flow-head">
        <h5>Dòng Tin</h5>
    </div>
    <ul class="list-unstyled text-right">
        <?php foreach ($count as $c): ?>
            <li><a href=""><?php echo $c['name'] ?></a> <span class="badge"><?php echo $c['count'] ?></span></li>
        <?php endforeach; ?>
    </ul>
</div>
