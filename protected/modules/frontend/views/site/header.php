<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/27/14
 * Time: 12:00 PM
 */
?>
<header class="head">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="container-fluid container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#appnav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo Yii::app()->params['site_url']; ?>">BongDa.Onl</a>
            </div>
            <div class="collapse navbar-collapse" id="appnav">
                <ul class="nav navbar-nav">
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php if (Yii::app()->user->isGuest): ?>
                    <li><a href="#" data-toggle="modal" data-target="#signin">Đăng Nhập</a></li>
                    <li>
                        <a href="#" data-toggle="modal" data-target="#signin">Đăng Ký</a>
                    </li>
                    <?php else: ?>
                        <ul class="list-inline pull-right">
                            <li class=" dropdown user-profile is_logged_in">
                                <a href="JavaScript:void(0)" data-toggle="dropdown" class="dropdown-toggle"><img class="img-circle img-thumbnail" src="<?php echo Yii::app()->user->getColumnName('photourl') ;  ?>" onerror="this.src='<?php //echo Yii::app()->user->getColumnName('photourl')!=''?Yii::app()->user->getColumnName('photourl'):Yii::app()->params['default_avatar']; ?>'" width="30px" height="30px"> <?php echo Yii::app()->user->getColumnName('displayname'); ?><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <?php if (!Yii::app()->user->isUser()): ?>
                                    <li><a href="<?php echo $this->createUrl('/backend/post/index/'); ?>">Trang Quản Trị</a></li>
                                    <?php endif ?>
                                    <li><a href="<?php //echo $this->createUrl('/profile/activity/'); ?>">Trang Cá Nhân</a></li>
                                    <li class="end"><a href="<?php //echo $this->createUrl('/profile/'); ?>">Chỉnh Sửa Thông Tin</a></li>
                                    <li class="divider"></li>
                                    <li class="end all"><a href="<?php echo $this->createUrl('/frontend/login/logout/'); ?>">thoát</a></li>
                                </ul>
                            </li>
                            <li class="newfeed userlogin" id="notification">
        
                            </li>
                        </ul>
                    <?php endif ?>
                </ul>
            </div>
            <!--    BEGIN LIGHTBOX SIGNIN & SIGNUP - HIDDEN DEFAULT -->
            <!--BEGIN SIGNIN-->
            <?php $this->renderPartial('signin', array()); ?>
            <!--END SIGNIN-->
            <!--BEGIN SIGNUP-->
            <?php //$this->renderPartial('signup', array()); ?>
            <!--END SIGNUP-->
            <!--    END LIGHTBOX SIGNIN & SIGNUP - HIDDEN DEFAULT -->
        </div>
    </nav>
</header>