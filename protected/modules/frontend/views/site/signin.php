<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 5/18/14
 * Time: 11:15 PM
 */
?>
<!--BEGIN SIGNIN-->
                <div class="modal fade" id="signin" tabindex="-1" role="dialog" aria-labelledby="singupLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Đăng Nhập</h4>
                            </div>
                            <div class="modal-body">
                                <div class="sociallogin">
                                    Click vào nút dưới đây để đăng nhập với tài khoản Facebook hoặc Google của bạn. Tài khoản của bạn trên bongda.onl sẽ tự động được tạo sau lần đăng nhập đầu tiên mà không cần đăng ký.
                                    <div class="text-center socialbtn">
                                        <a href="<?php echo $this->createUrl('/frontend/login/').'/oauth?provider=facebook'; ?>">
                                            <div class="btn-group">
        									<button type="button" class="btn btn-sm btn-default btn-facebook bg"><i class="fa fa-facebook"></i></button>
        									<button type="button" class="btn btn-sm btn-default">Facebook</button>
        								</div></a>
                                        <a href="<?php echo $this->createUrl('/frontend/login/').'/oauth?provider=google'; ?>">
                                                <div class="btn-group">
        									<button type="button" class="btn btn-sm btn-default btn-google-plus bg"><i class="fa fa-google-plus"></i></button>
        									<button type="button" class="btn btn-sm btn-default">Google</button>
        								</div></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END SIGNIN--> 