<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/23/14
 * Time: 12:50 AM
 */
?>
<?php $this->renderPartial('header', array()); ?>
<div class="clearfix"></div>
<div class="bc">
    <div class="container">
        <div class="home row">
            <div class="col-sm-10 col-xs-12" id="main-content">

            </div>
            <input type="hidden" id="page-num" value="1">
            <input type="hidden" id="tag" value="">
            <div class="col-sm-2 hidden-xs">
                <?php $this->renderPartial('right', array('count' => $count)); ?>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="foot hidden-xs">
    <?php $this->renderPartial('footer', array()); ?>
</div>
