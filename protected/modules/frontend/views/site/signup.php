<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 5/18/14
 * Time: 11:16 PM
 */
?>
<div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="singupLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Đăng Kí</h4>
            </div>
            <div class="modal-body">
                <p>Đăng kí thông qua:</p>
                <a href="#" class="btn btn-primary bnt-sm"><i class="fa fa-facebook"></i> Facebook</a>
                <a href="#" class="btn btn-danger bnt-sm"><i class="fa fa-google-plus"></i> Google</a>
                <p>hoặc :</p>
                <form role="form">
                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-male fa-fw"></i>
                                        </span>
                        <input class="form-control" type="text" placeholder="Tên hiển thị">
                    </div>
                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope-o fa-fw"></i>
                                        </span>
                        <input class="form-control" type="text" placeholder="Email">
                    </div>
                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-key fa-fw"></i>
                                        </span>
                        <input class="form-control" type="pass" placeholder="Mật khẩu">
                    </div>
                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-key fa-fw"></i>
                                        </span>
                        <input class="form-control" type="pass" placeholder="Mật khẩu">
                    </div>
                    <div class="margin-top-20">
                        <button type="submit" class="btn btn-primary">Đăng Kí</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>