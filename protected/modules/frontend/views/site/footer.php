<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/27/14
 * Time: 11:58 AM
 */
?>
<nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
    <div class="container-fluid container">
        <ul class="nav navbar-nav navbar-right">
            <li>Website đang trong quá trình xây dựng!</li>
        </ul>
    </div>
</nav>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-15971058-5', 'auto');
  ga('send', 'pageview');

</script>