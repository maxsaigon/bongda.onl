/**
 * Created by Thanhf.NguyeenX on 4/27/14.
 */
var index = function(){};
index.prototype.constructor = function()
{
    var that = this;
    $(document).ready(function(){
        $('#page-num').val(1);
        that.loadData();
        that.loadContent();
        that.loadByClub();
    });
}

index.prototype.scrollDown = function()
{
    var that = this;
    $(window).scroll(function() {
        if ($(document).height() <= $(window).scrollTop() + $(window).height()) {
            that.loadData();
        }
    });
}

index.prototype.loadData = function()
{
    var that = this;
    var page = $('#page-num').val();
    var tag = $('#tag').val();
    var term = '';
    $.ajax({
        url: 'index.php/frontend/site/ajax',
        type:"POST",
        dataType: "html",
        data: {term:term, page:page, tag:tag},
        success: function(data){
            if (page ==1)
                that.scrollDown();
            $('#main-content').append(data);
            $('#page-num').val(page*1+1);
            $('#tag').val(tag);
        },
        error: function(e){
            alert('error');
        }
    });
}

index.prototype.loadContent = function()
{
    $('body').on('click','.load_more', function(){
        var postId = $(this).attr('data-postid');
        if ($('#content_'+postId).is(':empty')){
            if ($('#content_'+postId).hasClass('hidden')){
                $.ajax({
                    url: 'index.php/frontend/site/ajaxContent',
                    type:"POST",
                    dataType: "html",
                    data: {postId:postId},
                    success: function(data){
                        $('#content_'+postId).html(data);
                        $('#content_'+postId).fadeOut( "slow", function() {
                            $(this).removeClass('hidden')
                        });
                    },
                    error: function(e){
                        alert('error');
                    }
                });
            }else{
                $('#content_'+postId).addClass('hidden').toggle();
            }
        }else{
            if ($('#content_'+postId).hasClass('hidden')){
                $('#content_'+postId).fadeOut( "slow", function() {
                    $(this).removeClass('hidden')
                });
            }else{
                $('#content_'+postId).fadeIn( "slow", function() {
                    $(this).addClass('hidden')
                });
            }

        }

    });
}

index.prototype.loadByClub = function()
{
    $('body').on('click','.readmore', function(){
        var tag = $(this).attr('data-name');
        var page = $('#page-num').val();
        var term = '';
            $.ajax({
            url: 'index.php/frontend/site/ajax',
            type:"POST",
            dataType: "html",
            data: {term:term, page:page, tag:tag},
            success: function(data){
                if (page ==1)
                    that.scrollDown();
                $('#main-content').html();   
                $('#main-content').html(data);
                $('#page-num').val(page*1+1);
                $('#tag').val(tag);
                //Put code here like so
                $('html, body').animate({ scrollTop: 0 }, 0);
                },
            error: function(e){
                alert('error');
            }
        });

    });
}


