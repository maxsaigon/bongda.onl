<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 3/22/14
 * Time: 11:53 AM
 */

Class UrlService {
    /*
     * Get content
     */
    public function getBDVNContent()
    {
        $date = new DateTime();
        // Step 1: get site info
        $siteModel = new SiteConsole();
        $site = $siteModel->findByPk(1);
        // Step 2: get all category page
        $categoryModel = new CategoryConsole();
        $categories = $categoryModel->getCategoryFromSiteId(1);// Example with siteId = 1
        // Step 3: Get content of category
        $urlModel = new UrlConsole();
        foreach ($categories as $cate){
            $data = $this->_getContentByUrl($cate['Title']);
            $urls = Utilities::getUrlInText($data, SiteConstant::URL_CLASS);
            foreach ($urls as $url){
                // Step 4: check url exist
                $urlExist = $urlModel->findByAttributes(array('url' => $site->Url.$url));
                if (!$urlExist){// Does not exist
                    $siteUrl = $site->Url.$url;echo $siteUrl;
                    $except = explode(',',$site->url_except);
                    $stop = false;
                    foreach ($except as $ee){echo $ee;
                        $pos = strpos($siteUrl, $ee);
                        if ($pos !== false) {
                            $stop = true;
                            break;
                        }
                    }
                    if ($stop)
                        continue;
                
                    try{
                        $html = $this->_getContentByUrl($siteUrl);
                        $html = Utilities::addImgResponsive($html);
                        $html = Utilities::removeImgStyle($html);
                        $title = Utilities::getContentByXPath($html, $site->title_xpath);
                        $title = str_replace('(BongDa.com.vn)','',$title);
                        $content = Utilities::getContentInClass($html, SiteConstant::CONTENT_CLASS, 'div');
                        $content = Utilities::replaceFontTag($content);
                        //$content = strip_tags($content, '<p><br><div><img>');
                        $content = strip_tags($content, '<p><br><img>');
                        $content =  html_entity_decode($content,null,'UTF-8');
                        $content = Utilities::replaceFontTag($content);
                        $content = str_replace('(BongDa.com.vn)','',$content);
                        $tags = Utilities::getContentByXPath($html, $site->tag_xpath);
                        $array = Utilities::getContentByXPath1($html,'//div[@class="read_news"]/div/strong//text()[normalize-space()]');
                        $description = implode("",$array );
                        $description = str_replace('(BongDa.com.vn)','',$description);             
                        // Store content
                        $model = new UrlConsole();
                        // Feature image
                        $src = Utilities::clean(Utilities::getSrcFromImg($content));
                        if ($src != '')
                        {
                            $date = new DateTime();
                            $year = $date->format('Y');
                            $month = $date->format('m');
                            // Save image
                            $imagecontent = Utilities::downloadImageFromUrl($src);
                            if (isset($imagecontent)){
                                $fileName = 'bongda_'.time().rand ( 1, 10000).'.jpg';
                                $source = Yii::app()->params['download_source'] . $year . '/' . $month . '/'. $fileName;
                                $savefile = fopen($source, 'w+');
                                fwrite($savefile, $imagecontent);
                                fclose($savefile);
                                chmod($source,0777);
//                                 Generate
                                $imageService = new ImageService();
                                $dimention = Yii::app()->params['dimention'];
                                $imageService->cropImage($source, $dimention);
                                $model->feature_image = 'download/posts/' . $year . '/' . $month . '/' . $fileName;
                            }else{
                                $model->feature_image = '';
                            }

                        }else{
                            $stop = true;
                            break;
                        }
                        $model->category_id = $cate['Id'];
                        $model->tile = $title;
                        $model->slug = Utilities::friendlyUrl($title);
                        $model->description = $description;
                        $model->is_reviewed= 1;
                        $model->content = $content;
                        $model->url = $site->Url.$url;
                        $model->created = $date->format('m/d/Y H:i:s');
                        $model->status = 1;
                        if ($model->save(false))
                        {
                            $id = $model->primaryKey;
                            // Process tag
                            $tags = str_replace('Xem thêm:','',$tags);
                            $tags = explode(',',$tags);
                            foreach ($tags as $tag){
                                $flag = 0;
                                $tagConvertModel = new TagConvertConsole();
                                $tagConvert = $tagConvertModel->findByAttributes(array('tag_source' => trim($tag)));
                                if ($tagConvert){
                                    $tagDes =  $tagConvert->tag_des;
                                    $flag = 1;
                                }else{
                                    $tagDes = trim($tag);
                                }

                                $tagModel = new TagConsole();
                                $thisTag = $tagModel->findByAttributes(array('Name' => $tagDes));
                                if (!$thisTag){
                                    $tagModel->Name = $tagDes;
                                    $tagModel->Tag = $tagDes;
                                    $tagModel->tag_order = $tagDes;
                                    $tagModel->tag_image = 'images/tags/01.jpg';
                                    if ($tagModel->save(false)){
                                        $postTagModel = new PostTagConsole();
                                        $postTagModel->tag_id = $tagModel->primaryKey;
                                        $postTagModel->post_id = $id;
                                        if ($postTagModel->save()){

                                        }
                                    }
                                }else{
                                    $postTagModel = new PostTagConsole();
                                    $postTagModel->tag_id = $thisTag->id;
                                    $postTagModel->post_id = $id;
                                    if ($postTagModel->save(false)){

                                    }
                                }
                            }
                        }
                    }catch (Exception $e){
                        echo "<pre>";print_r($siteUrl);die;
                    }

                }
            }
        }
    }

    /*
     * Get content by url
     */
    protected function _getContentByUrl($url, $post='', $refer='', $cookie='', $headers='', $header=1, $nobody=0)
    {
        $browse = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.13) Gecko/2009073022 Firefox/3.0.13";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_REFERER, $refer);
        curl_setopt($ch, CURLOPT_COOKIE, $cookie);
        if($post){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);}
        if($headers){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);}
        curl_setopt($ch, CURLOPT_USERAGENT, 'Googlebot/2.1 (http://www.googlebot.com/bot.html)');
        curl_setopt($ch, CURLOPT_HEADER, $header);
        curl_setopt($ch, CURLOPT_NOBODY, $nobody);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    /*
     * Get content
     */
    public function getBDPContent()
    {
        $date = new DateTime();
        // Step 1: get site info
        $siteModel = new SiteConsole();
        $site = $siteModel->findByPk(2);
        // Step 2: get all category page
        $categoryModel = new CategoryConsole();
        $categories = $categoryModel->getCategoryFromSiteId(2);// Example with siteId = 1
        // Step 3: Get content of category
        $urlModel = new UrlConsole();
        foreach ($categories as $cate){
            $data = $this->_getContentByUrl($cate['Title']);
            $urls = Utilities::getUrlByCondition($data, $cate['id_on_site']);
            foreach ($urls as $url){
                // Step 4: check url exist
                $urlExist = $urlModel->findByAttributes(array('url' => $site->Url."/tin-bai/".$cate['id_on_site']."/".$url."bdplus"));
                if (!$urlExist){// Does not exist
                    $siteUrl = $site->Url."/tin-bai/".$cate['id_on_site']."/".$url."bdplus";echo $siteUrl;
                    $except = explode(',',$site->url_except);
                    $stop = false;
                    foreach ($except as $ee){
                        $pos = strpos($siteUrl, $ee);
                        if ($pos !== false) {
                            $stop = true;
                            break;
                        }
                    }
                    if ($stop)
                        continue;

                    try{
                        $html = $this->_getContentByUrl($siteUrl);
                        $html = Utilities::addImgResponsive($html);                       
                        $title = Utilities::getContentByXPath($html, $site->title_xpath);
                        $content = Utilities::getContentBetween($html, "contentbox");
                        $content = Utilities::replaceSpanTag($content);
                        $content = strip_tags($content, '<p><br><img>');
                        $content =  html_entity_decode($content,null,'UTF-8');
                        $tagHtml = Utilities::getContentBetween($html, "news_tags");
                        $tags = array();
                        if(preg_match_all('/<a\s+.*?>(.*?)<\/a>/i', $tagHtml, $links, PREG_PATTERN_ORDER))
                            $tags = array_unique($links[1]);
                        $description = Utilities::getContentByXPath($html, $site->description_xpath);
                        //$description = strip_tags($description, '<strong>');
                        // Store content
                        $model = new UrlConsole();
                        // Feature image
                        $src = Utilities::clean(Utilities::getSrcFromImg($content));
                        if ($src != '')
                        {
                            $date = new DateTime();
                            $year = $date->format('Y');
                            $month = $date->format('m');
                            // Save image
                            $imagecontent = Utilities::downloadImageFromUrl($src);
                            if (isset($imagecontent)){
                                $fileName = 'bongda_'.time().rand ( 1, 10000).'.jpg';
                                $source = Yii::app()->params['download_source'] . $year . '/' . $month . '/'. $fileName;
                                $savefile = fopen($source, 'w+');
                                fwrite($savefile, $imagecontent);
                                fclose($savefile);
                                chmod($source,0777);
//                                 Generate
                                $imageService = new ImageService();
                                $dimention = Yii::app()->params['dimention'];
                                $imageService->cropImage($source, $dimention);
                                $model->feature_image = 'download/posts/' . $year . '/' . $month . '/' . $fileName;
                            }else{
                                $model->feature_image = '';
                                $model->is_reviewed= 1;
                            }

                        }
                        $content = Utilities::removeImg($content);
                        //$content = Utilities::replaceImgTag($content);
                        //$content = Utilities::replaceImgTag1($content);                       
                        $model->category_id = $cate['Id'];
                        $model->tile = $title;
                        $model->slug = Utilities::friendlyUrl($title);
                        $model->description = $description;
                        $model->content = $content;
                        $model->is_reviewed= 1;
                        $model->url = $siteUrl;
                        $model->created = $date->format('m/d/Y H:i:s');
                        $model->status = 1;
                        if ($model->save(false))
                        {
                            $id = $model->primaryKey;
                            foreach ($tags as $tag){
                                $flag = 0;
                                $tagConvertModel = new TagConvertConsole();
                                $tagConvert = $tagConvertModel->findByAttributes(array('tag_source' => trim($tag)));
                                if ($tagConvert){
                                    $tagDes =  $tagConvert->tag_des;
                                    $flag = 1;
                                }else{
                                    $tagDes = trim($tag);
                                }

                                $tagModel = new TagConsole();
                                $thisTag = $tagModel->findByAttributes(array('Name' => $tagDes));
                                if (!$thisTag){
                                    $tagModel->Name = $tagDes;
                                    $tagModel->Tag = $tagDes;
                                    $tagModel->tag_order = $tagDes;
                                    $tagModel->tag_image = 'images/tags/01.jpg';
                                    if ($tagModel->save()){
                                        $postTagModel = new PostTagConsole();
                                        $postTagModel->tag_id = $tagModel->primaryKey;
                                        $postTagModel->post_id = $id;
                                        if ($postTagModel->save()){

                                        }
                                    }
                                }else{
                                    $postTagModel = new PostTagConsole();
                                    $postTagModel->tag_id = $thisTag->id;
                                    $postTagModel->post_id = $id;
                                    if ($postTagModel->save()){

                                    }
                                }
                            }
                        }
                    }catch (Exception $e){
                        echo "<pre>";print_r($siteUrl);die;
                    }

                }
            }
        }
    }
    
    public function getTBDContent()
    {
        $date = new DateTime();
        // Step 1: get site info
        $siteModel = new SiteConsole();
        $site = $siteModel->findByPk(3);
  
        // Step 3: Get content of category
        $urlModel = new UrlConsole();
        $data = $this->_getContentByUrl($site->Url);
        $urls = Utilities::getUrlByConditionTBD($data);
        foreach ($urls as $url){
            // Step 4: check url exist
                $urlExist = $urlModel->findByAttributes(array('url' => $url));
                if (!$urlExist){// Does not exist
                    $siteUrl = $url ;echo $siteUrl; 
                    $except = explode(',',$site->url_except);
                    $stop = false;
                    foreach ($except as $ee){
                        $pos = strpos($siteUrl, $ee);
                        if ($pos !== false) {
                            $stop = true;
                            break;
                        }
                    }
                    if ($stop)
                        continue;
                    try{
                        
                        $html = $this->_getContentByUrl($siteUrl);                  
                        $title = Utilities::getContentByXPath($html,$site->title_xpath);
                        $content = Utilities::getContentBetween($html, "itemBody");
                        $content = strip_tags($content, '<img><iframe>');
                        $content =  html_entity_decode($content,null,'UTF-8');
                        
                        $abc = strpos($content,'img');
                        
                        if($abc === false){
                            $type = "video";
                        }else{
                            $type = "photo";
                        }
                        $tags = array("0"=>"Troll Bóng Đá");
                       
                        $description = Utilities::getContentByXPath($html, '//*[@class="itemTitle"]');
                        // Store content
                        $model = new UrlConsole();
                        if($type == 'photo'){
                            // Feature image
                            $src = 'http://trollbongda.com'.Utilities::clean(Utilities::getSrcFromImg($content));
                            if ($src != '')
                            {
                                $date = new DateTime();
                                $year = $date->format('Y');
                                $month = $date->format('m');
                                // Save image
                                $imagecontent = Utilities::downloadImageFromUrl($src);
                                if (isset($imagecontent)){
                                    $fileName = 'bongda_'.time().rand ( 1, 10000).'.jpg';
                                    $source = Yii::app()->params['download_source'] . $year . '/' . $month . '/'. $fileName;
                                    $savefile = fopen($source, 'w+');
                                    fwrite($savefile, $imagecontent);
                                    fclose($savefile);
                                    chmod($source,0777);
    //                                 Generate
                                    $imageService = new ImageService();
                                    $dimention = Yii::app()->params['dimention'];
                                    $imageService->cropImage($source, $dimention);
                                    $model->feature_image = 'download/posts/' . $year . '/' . $month . '/' . $fileName;
                                }else{
                                    $model->feature_image = '';
                                    $model->is_reviewed= 1;
                                }
    
                            }
                        }else{
                            $srcs = Utilities::clean(Utilities::getSrcFromIframe($content));
                            if ($srcs != '') {
                                $year = $date->format('Y');
                                $month = $date->format('m');
                                $src = Utilities::getYoutubeImage($srcs);
                                $code = Utilities::getYoutubeCode($srcs);
                                    // New image feature
                                    // Save image
                                    $imagecontent = Utilities::downloadImageFromUrl($src);
                                    if (isset($imagecontent)){
                                        $fileName = 'bongda_'.time().rand ( 1, 10000).'.jpg';
                                        $source = Yii::app()->params['download_source'] . $year . '/' . $month . '/'. $fileName;
                                        $savefile = fopen($source, 'w+');
                                        fwrite($savefile, $imagecontent);
                                        fclose($savefile);
                                        chmod($source,0777);
        //                                 Generate
                                        $imageService = new ImageService();
                                        $dimention = Yii::app()->params['dimention'];
                                        $imageService->cropImage($source, $dimention);
                                        $model->feature_image = 'download/posts/' . $year . '/' . $month . '/' . $fileName;
                                }else{
                                    $model->feature_image = '';
                                    $model->is_reviewed= 1;
                                }
                                
                            }
                        }                      
                        $model->category_id = 5;
                        $model->tile = $title;
                        $model->type = $type;
                        $model->slug = Utilities::friendlyUrl($title);
                        $model->description = $description;
                        $model->content = $content;
                        $model->is_reviewed= 1;
                        $model->url = $siteUrl;
                        $model->created = $date->format('m/d/Y H:i:s');
                        $model->status = 1;
                        if ($model->save(false))
                        {
                            $id = $model->primaryKey;
                            foreach ($tags as $tag){
                                $flag = 0;
                                $tagConvertModel = new TagConvertConsole();
                                $tagConvert = $tagConvertModel->findByAttributes(array('tag_source' => trim($tag)));
                                if ($tagConvert){
                                    $tagDes =  $tagConvert->tag_des;
                                    $flag = 1;
                                }else{
                                    $tagDes = trim($tag);
                                }

                                $tagModel = new TagConsole();
                                $thisTag = $tagModel->findByAttributes(array('Name' => $tagDes));
                                if (!$thisTag){
                                    $tagModel->Name = $tagDes;
                                    $tagModel->Tag = $tagDes;
                                    $tagModel->tag_order = $tagDes;
                                    $tagModel->tag_image = 'images/tags/01.jpg';
                                    if ($tagModel->save(false)){
                                        $postTagModel = new PostTagConsole();
                                        $postTagModel->tag_id = $tagModel->primaryKey;
                                        $postTagModel->post_id = $id;
                                        if ($postTagModel->save(false)){

                                        }
                                    }
                                }else{
                                    $postTagModel = new PostTagConsole();
                                    $postTagModel->tag_id = $thisTag->id;
                                    $postTagModel->post_id = $id;
                                    if ($postTagModel->save(false)){

                                    }
                                }
                            }
                        }                        
                    }catch (Exception $e){
                        echo "<pre>";print_r($siteUrl);die;
                    }
        
        }   
        
    }
    }
}