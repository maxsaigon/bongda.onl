<?php
class ImageService
{	
	public function cropImage($url, $dimention = '')
	{
		try{
			$image = Yii::app()->image->load($url);
			$info = $image->getImageInfo();
            if ($dimention == '')
			    $dimention = Yii::app()->params['dimention'];
			foreach( $dimention as $d)
			{
                $ratio1 = $info[0]/$d['width'];
                $ratio2 = $info[1]/$d['height'];
				if ($ratio1 > $ratio2) // Width > height
					$image->resize($d['width'], $d['height'], Image::HEIGHT);
				else
					$image->resize($d['width'], $d['height'], Image::WIDTH);
				$image->crop($d['width'], $d['height'], 'center','center');
				$image->save(Utilities::addImageEx($url, $d['width'], $d['height']),0777);
			}
			return true;
		}catch(Exception $e){
			var_dump($e->getMessage());die;
		}
	}

    public function cropImageBySize($url, $size)
    {
        try{
            $image = Yii::app()->image->load($url);
            $info = $image->getImageInfo();
            // Check
            $ratio1 = $info[0]/$size['width'];
            $ratio2 = $info[1]/$size['height'];
            if ($ratio1 > $ratio2) // Width > height
                $image->resize($size['width'], $size['height'], Image::HEIGHT);
            else
                $image->resize($size['width'], $size['height'], Image::WIDTH);
            $image->crop($size['width'], $size['height'], 'center','center');
            $image->save(Utilities::addImageEx($url, $size['width'], $size['height']));
            return true;
        }catch(Exception $e){
            return false;
        }
    }

    public function cropOriginImage($url, $size)
    {
        try{
            $image = Yii::app()->image->load($url);
            $info = $image->getImageInfo();
            // Check
            $ratio1 = $info[0]/$size['width'];
            $ratio2 = $info[1]/$size['height'];
            if ($ratio1 > $ratio2) // Width > height
                $image->resize($size['width'], $size['height'], Image::HEIGHT);
            else
                $image->resize($size['width'], $size['height'], Image::WIDTH);
            $image->crop($size['width'], $size['height'], 'center','center');
            $image->save($url);
            return true;
        }catch(Exception $e){
            return false;
        }
    }
}
?>