<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 3/22/14
 * Time: 11:50 AM
 */

class UrlCommand extends BaseCommands
{
    /*
     * Get Content
     */
    public function actionBDVN()
    {
        // Service
        $service = new UrlService();
        $service->getBDVNContent();
    }

    /*
     * Get Content
     */
    public function actionBDP()
    {
        // Service
        $service = new UrlService();
        $service->getBDPContent();
    }
    
    /*
     * Get Content
     */
    public function actionTBD()
    {
        // Service
        $service = new UrlService();
        $service->getTBDContent();
    }

    public function actionDelete()
    {
        echo "Begin";

        $dir = Yii::app()->params['home_source'] . '/assets/';
        $it = new RecursiveDirectoryIterator($dir);
        $files = new RecursiveIteratorIterator($it,
            RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($files as $file) {
            if ($file->getFilename() === '.' || $file->getFilename() === '..') {
                continue;
            }
            if ($file->isDir()) {
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }

        $dir1 = Yii::app()->params['home_source'] . '/protected/runtime/';
        $it1 = new RecursiveDirectoryIterator($dir1);
        $files1 = new RecursiveIteratorIterator($it1,
            RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($files1 as $file1) {
            if ($file1->getFilename() === '.' || $file1->getFilename() === '..') {
                continue;
            }
            if ($file1->isDir()) {
                rmdir($file1->getRealPath());
            } else {
                unlink($file1->getRealPath());
            }
        }

        echo "End";
   }   
} 