<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 3/22/14
 * Time: 11:49 AM
 */

class CategoryConsole extends BaseCategory {
    /*
     * Get category
     */
    public function getCategoryFromSiteId($siteId)
    {
        return Yii::app()->db->createCommand()
            ->select('c.*')
            ->from('category c')
            ->where('c.SiteId = :siteId', array(':siteId' => $siteId))
            ->queryAll();
    }
} 