<?php

require_once(dirname(__FILE__).'/config/environment.php');
//For production
$environment = new Environment(Environment::PRODUCTION, 'console');

// change the following paths if necessary
$yiic = dirname(__FILE__).'/../library/yii/framework/yiic.php';
//$config = dirname(__FILE__).'/config/console.php';
$config = $environment->getConsoleConfig();
require_once($yiic);


