<?php
/**
 * User: nqdtrang
 * Date: 18/12/13
 * Time: 14:37 PM
 */
?>
<html>
<head>
</head>
<body>
    Dear Winebuys Team,
    <br />
    <br />A partner has submitted a change to their location information. Please review the details below and click <a href="<?php echo $link.'/id/'.$myMail->id.'/locationname/'.$myMail->name; ?>">here</a> to update the location information.
    <br />
    <br />Request From: <?php echo $username; ?>
    <br />
    <br />&nbsp; Visible to users: <?php echo $myMail->hidden == 1 ? 'No' : 'Yes'; ?>
    <br />&nbsp; Business Name: <?php echo $myMail->name; ?>
    <br />&nbsp; Address Line 1: <?php echo $myMail->address1; ?>
    <br />&nbsp; Address Line 2: <?php echo $myMail->address2; ?>
    <br />&nbsp; Address Line 3: <?php echo $myMail->address3; ?>
    <br />&nbsp; City: <?php echo $myMail->municipality; ?>
    <br />&nbsp; State: <?php echo $myMail->district; ?>
    <br />&nbsp; Zip: <?php echo $myMail->postcode; ?>
    <br />&nbsp; Country: <?php echo $myMail->country; ?>
    <br />&nbsp; Phone Number: <?php echo $myMail->telephone; ?>
    <br />&nbsp; Website: <?php echo $myMail->website; ?>
    <br />&nbsp; Business Type: <?php echo $myMail->type; ?>
    <br />&nbsp; Contact Email Address: <?php echo $myMail->contact_email; ?>
    <br />&nbsp; Contact Name: <?php echo $myMail->contact_name; ?>
    <br />
    <br />Regards.
</body>
</html>