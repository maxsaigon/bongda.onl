<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nmthanh
 * Date: 12/12/13
 * Time: 1:32 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<html>
<head>
</head>
<body>
    Dear <?php echo $myMail['name']; ?>,
    <br />
    <br /><?php echo 'Your account has been changed by admin with status: '.$myMail['status']; ?>.
	<br />If you have questions, please <a href="<?php echo $myMail['contactLink']; ?>">contact us</a>.
    <br />
    <br />
    Regards,
    <br />Winebuys Team
</body>
</html>