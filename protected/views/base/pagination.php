<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nmthanh
 * Date: 7/31/13
 * Time: 11:22 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<div class="paging">
    <div class="row-fluid">
    <?php if($data['pager']['totalRecord'] > 0):?>
        <?php if (isset($csv) && $csv != ''): ?>
        <div class="span2 csv-download">
            <span>Download <a href="JavaScript:void(0)" id="download" data-type="<?php echo isset($name)?$name:''; ?>">CSV</a></span>
        </div>
        <div class="span3 limit-status">
            <span>
                <?php $start = $data['pager']['currentPage'] * $data['pager']['itemPerPage'];?>
                <?php echo $start + 1; ?> to <?php echo $start + $data['pager']['itemInPage']; ?> of
                <?php echo $data['pager']['totalRecord']; ?> <?php echo isset($name)? Utilities::formatNamePager($name) : ''; ?></span>
        </div>
        <?php else: ?>
        <div class="span5">
            <span>
                <?php $start = $data['pager']['currentPage'] * $data['pager']['itemPerPage'];?>
                <?php echo $start + 1; ?> to <?php echo $start + $data['pager']['itemInPage']; ?> of
                <?php echo $data['pager']['totalRecord']; ?> <?php echo isset($name)? Utilities::formatNamePager($name) : ''; ?></span>
        </div>
        <?php endif ?>
     <?php endif;?>
        <div class="span7">
            <?php
            $this->widget('MyLinkPager', array(
                'currentPage' => $data['pager']['currentPage'],
                'itemCount' => $data['pager']['totalRecord'],
                'pageSize' => $data['pager']['itemPerPage'],
                'maxButtonCount' => 5,
                'header' => '',
                'htmlOptions' => array('class'=>'pages pagination'),
                'firstPageLabel'=>'First',  //fill in the following as you want
                'prevPageLabel'=>'Previous',
                'nextPageLabel'=>'Next',
                'lastPageLabel'=>'Last'
            ));
            ?>
        </div>
    </div>
</div>