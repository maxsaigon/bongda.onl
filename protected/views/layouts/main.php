<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
    <meta property="fb:app_id" content="<?php echo Yii::app()->params['facebookId'] ?>"/>
    <title><?php echo $this->pageTitle; ?></title>
    <meta name="description" content="<?php echo $this->pageDescription; ?>">
    <!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <script type="text/javascript" src="https://connect.facebook.net/en_US/all.js"></script>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=<?php echo Yii::app()->params['facebookId'] ?>&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
    <?php echo $content; ?>
</body>

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl. '/js/plugin/avim.js' ?>"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/libs/summer/js/summernote.min.js' ?>"></script>
</html>