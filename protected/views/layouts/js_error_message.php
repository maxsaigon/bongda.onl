<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nmthanh
 * Date: 12/9/13
 * Time: 11:45 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<script type="text/javascript">
    var REQUIRE_FIELD = 'This field is required.';
    var UNABLE_FORGOT = 'Unable for submit this form. The highlighted fields contain an error.'
    var UNABLE_CREATE_ACC = 'Unable to create account. The highlighted fields contain an error.'

    //    Register page
    var INVALID_PHONE = '';
    var INVALID_PASS_LEN = 'Please enter at least 6 characters.';
    var EMAIL_INVALID = 'Please enter a valid email address';
    var EMAIL_EXISTING = 'The email you entered already exists. Please try again.';
    var FULL_NAME_REQUIRED =  'Full Name is required.';
    var BUSINESS_NAME_REQUIRED = 'Business Name is required.';
    var EMAIL_REQUIRED = 'Email Address is required';
    var ADDRESS_REQUIRED = 'Address is required.';
    var CITY_REQUIRED = 'City is required.';
    var ZIP_REQUIRED = 'A valid Zip Code is required.';
    var PHONE_REQUIRED = 'Please enter a valid phone number.';
    var PASSWORD_REQUIRED = 'Please enter a password for your account';
    var REPEAT_PASSWORD_REQUIRED = 'Passwords do not match. Please reenter your password.';

    // Suggest form
    var WINE_BRAND_REQUIRED = 'Brand name is required';

    // Contact form
    var UNABLE_SEND_MESSAGE = 'Unable to send your message. The highlighted fields contain an error.';
    var NAME_REQUIRE = 'Your Name is required.';
    var EMAIL_REQUIRED = 'Your email address is required';
    var EMAIL_CONTACT = 'Please enter a valid email address of the form johnsmith@email.com';
    var SUBJECT_REQUIRE = 'Please enter a subject for your message.';
    var MESSAGE_REQUIRE = 'Please enter some text for your message describing your inquiry.';
    var CAPTCHA = 'Please enter a valid CAPTCHA.';
    var MESSAGE_SENT = 'Your message has been sent';

    // Edit Location page
    var COUNTRY_REQUIRED = 'Country is required.';
    var WEBSITE_REQUIRED = 'Website is required.';
    var UNABLE_SAVE_LOCATION = 'Unable to update location. The highlighted fields contain error.';

    // Location landing url
    var LOCATION_LANDING_URL = '<?php echo Yii::app()->createUrl('backend/location/page'); ?>';

    // renew password
    var RENEW_SUCCESS = 'Your password was reset successfully. Please try logging in again.';

    // setting user action url	
    var SETTINGS_USER_URL = '<?php echo Yii::app()->createUrl('backend/user/settings'); ?>';
    
    // edit user action url	
    var EDIT_USER_URL = '<?php echo Yii::app()->createUrl('backend/user/edit'); ?>';
    var UNABLE_SAVE_EDIT = 'Unable to update user. The highlighted fields contain an error.';

    var LNGLAT_FORMAT = 'Please enter a valid format';

    // Mobile detect
    var MOBILE_DETECT = 'For an optimal user experience, please use a desktop browser.';

    // Assign location
    var NO_USER_ASSIGN = 'Please select a user to manage this location.';

    // Website
    var INVALID_WEBSITE = 'Please enter a valid website format. Example: <br>http://www.website.com <br>http://www.website.com/test <br>http://test.website.com';

    // Zipcode 
    var INVALID_ZIPCODE = 'A valid Zip Code is required.<br>Example: xxxxx';

    //Prize
    var PRIZE_REQUIRED = 'This field is required';
    var PERCENT_REQUIRED = 'A percentage is required for each prize';
    var NO_PRIZE = 'You must configure at least 1 prize';
    var RANGE_VALUE = 'Please enter a value between 1 and 99 for the percentage';
    var SUM_MUST_BE_100 = 'The percentage for all prizes configured must total 100%';

    var USER_TYPE_PARTNER = 4;
</script>
