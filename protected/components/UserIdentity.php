<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;
    public $email;
    public $password;
    public $errorCode;

    const ERROR_NONE = 0;
    const ERROR_EMAIL_INVALID = 1;
    const ERROR_PASSWORD_INVALID = 2;
    const USER_IS_DISABLED = 3;
    const SOMETHING_WRONG = 4;

    const ERROR_NONE_BUT_PENDING = 5;
    const ERROR_NONE_BUT_SUSPENDED = 6;

    /**
     * construct class.
     */
    public function __construct($email, $password) {
        $this->email = $email;
        $this->password = $password;
    }
    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        // Model
        $user = BackendUsers::model()->findByAttributes(array('user_email' => $this->email));
        if ($user === null) { // No user found!
            $this->errorCode = self::ERROR_EMAIL_INVALID;
        } else if ($user->user_password !== Utilities::hashPassword($user->password_salt, $this->password)) { // Invalid password!
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else { // Okay!
            // Check user enable or disable
            switch ($user->user_status){
                case SiteConstant::USER_ACTIVE:
                    $this->_id = $user->user_id;
                    $this->setState('roles', $user->user_role);
                    // Store user info
                    $this->setState('user', $user);
                    $this->errorCode = self::ERROR_NONE;
                    break;
                case SiteConstant::USER_PENDING: // For now, same with active status
                    $this->_id = $user->user_id;
                    $this->setState('roles', $user->user_role);
                    // Store user info
                    $this->setState('user', $user);
                    $this->errorCode = self::ERROR_NONE;
                    break;
                case SiteConstant::USER_SUSPENDED: // For now, same with active status
                    $this->_id = $user->user_id;
                    $this->setState('roles', $user->user_role);
                    // Store user info
                    $this->setState('user', $user);
                    $this->errorCode = self::ERROR_NONE;
                    break;
                case SiteConstant::USER_INACTIVE: // make user possible to login, but redirect to settings page
                	$this->_id = $user->user_id;
                	$this->setState('roles', $user->user_role);
                	// Store user info
                	$this->setState('user', $user);
                	$this->errorCode = self::ERROR_NONE;
                    break;
            }
        }

        return $this->errorCode;
    }

    /*
     * Authenticates
     */
    public function authenBySocial1()
    {
        // Model
        $userModel = new FrontendUsers();
        $user = $userModel->findByAttributes(array(),'email =:email', array(':email' => $this->email));
        if ($user === null) { // No user found!
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else{
            $this->errorCode = self::ERROR_NONE;
        }
        return $this->errorCode;
    }
    
    /*
     * Authenticates
     */
    public function authenBySocial()
    {
        // Model
        $userModel = new FrontendUsers();
        $user = $userModel->getUserByUserLogin($this->email);
        if ($user === null) { // No user found!
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else{
            // Check user status
                $this->_id = $user['user_id'];
                $this->setState('user', $user);
                $this->errorCode = self::ERROR_NONE;
           
        }
        return $this->errorCode;
    }

    /**
     * Get user Id
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Load user
     */
    protected function loadUser($id=null)
    {
        if($this->_model===null)
        {
            if($id!==null)
                $this->_model= BackendUsers::model()->findByPk($id);
        }
        return $this->_model;
    }
}