<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nmthanh
 * Date: 10/9/13
 * Time: 11:24 AM
 * To change this template use File | Settings | File Templates.
 */
class SiteConstant
{
    /*
     * For Bongda.com.vn
     */
    CONST MAIN_TITLE = 'id="topNew"';
    CONST URL_CLASS = 'class="read_more"';
    CONST CONTENT_CLASS = 'class="read_news"';
    CONST TITLE_CLASS = '//*[@class="title_read_news"]';


    Const IMAGE_SIZE = 3048576;
    
    Const USER_TYPE = 0;
    Const ADMIN_TYPE = 1;
    Const MOD_TYPE = 2;
    Const EDITOR_TYPE = 3;
    
    Const NORMAL_STATUS = 0;
    Const ACTIVE_STATUS = 1;
    Const FORGET_STATUS = 2;
    Const INACTIVE_STATUS = 3;
    
    Const NORMAL_ROLE = 0;
}