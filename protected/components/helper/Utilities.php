<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nmthanh
 * Date: 5/21/13
 * Time: 11:11 AM
 * To change this template use File | Settings | File Templates.
 */
class Utilities
{
    /*
     * Clean SQL Injection and XSS
     * @param String $str
     */
    static function clean($str)
    {
        // Fix fox IE 9
        if ($str == 'Search+by+Keyword' || $str == 'Search by Keyword')
            $str = '';
        $value = addslashes(stripslashes(htmlspecialchars(strip_tags($str))));
        $value = str_replace('&amp;', '&', $value);
        return $value;
    }

    /*
     * Check exist and is number value
     * @param INT $val
     */
    static function checkNumber($val)
    {
        if(isset($val) && is_numeric($val))
            return true;
        return false;
    }

    /*
     * Get Post and Put parameter
     * @param INT userId
     */
    static function getValue()
    {
        // Get POST or PUT parameters
        $input = file_get_contents('php://input');
        // Decode POST value json
        return json_decode($input,true);
    }

    /*
     * Convert day type to date time
     * @return array date time
     */
    static function convertDayType($dayType, $startTime, $dayRegimen)
    {
        // Create data result
        $data = array();
        // Get end day
        $endDay = date('Y-m-d', strtotime($startTime. ' + '. $dayRegimen .' days'));
        // Get dayType
        $dayTypeArr = explode(',', $dayType);
        foreach ($dayTypeArr as $day) {
            if ($day == SiteConstant::EVERYDAY)
                return Utilities::dateRange($startTime, $endDay);
            elseif ($day > 8 || $day < 1){
                return $data;
            }else{
                $dayArr = Utilities::dateRange($startTime, $endDay);
                foreach ($dayArr as $days){
                    if (strtolower(date('l', strtotime($days))) == strtolower(Utilities::getDayByNum($day)))
                        $data[] = $days;
                }
            }
        }
        return $data;
    }

    static function timeDiffString($from, $to = '', $full = true, $type = 1)
    {
        $current = new DateTime();
        $to = $to == ''?$current->format('Y-m-d H:i:s'):$to;
        $from = new DateTime($from);
        $to = new DateTime($to);
        $diff = $to->diff($from);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        if ($type == 1)
            $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day'
            );
        elseif ($type ==2){
            $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day',
                'h' => 'hour',
                'i' => 'minute'
            );
        }else{
            $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day',
                'h' => 'hour',
                'i' => 'minute',
                's' => 'second'
            );
        }
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        // Get only 2 params
        $i = 0;
        $temp = array();
        foreach ($string as $tr){
            if ($i <2){
                $temp[] = $tr;
            }
            $i++;
        }
        return $string ? implode(' ', $temp) : 'just now';
    }


    /**
     * creating between two date
     * @param string since
     * @param string until
     * @param string step
     * @param string date format
     * @return array
     */
    static function dateRange($first, $last, $step = '+1 day', $format = 'Y-m-d' ) {

        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);

        while( $current <= $last ) {

            $dates[] = date($format, $current);
            $current = strtotime($step, $current);
        }

        return $dates;
    }

    /**
     * get day of week by number
     * @param integer num
     * @return string day of week
     */
    static function getDayByNum($num)
    {
        switch ($num){
            case SiteConstant::MONDAY:
                return 'monday';
            case SiteConstant::TUESDAY:
                return 'tuesday';
            case SiteConstant::WEDNESDAY:
                return 'wednesday';
            case SiteConstant::THURSDAY:
                return 'thursday';
            case SiteConstant::FRIDAY:
                return 'friday';
            case SiteConstant::SATURDAY:
                return 'saturday';
            default;
                return 'sunday';
        }
    }

    /**
     * get number of day
     * @param string day
     * @return string day of week
     */
    static function getNumByDay($day)
    {
        switch (strtolower($day)){
            case 'monday':
                return SiteConstant::MONDAY;
            case 'tuesday':
                return SiteConstant::TUESDAY;
            case 'wednesday':
                return SiteConstant::WEDNESDAY;
            case 'thursday':
                return SiteConstant::THURSDAY;
            case 'friday':
                return SiteConstant::FRIDAY;
            case 'saturday':
                return SiteConstant::SATURDAY;
            default;
                return SiteConstant::SUNDAY;
        }
    }

    /**
     * Hashing a password with password salt.
     * @return string.
     */
    static function hashPassword($salt, $password)
    {
        return MD5($salt.$password);
    }

    /**
     * method to encrypt or decrypt a plain text string
     * initialization vector has to be the same when encrypting and decrypting
     * you can also choose to append the IV to the encrypted text and get it when decrypting
     *
     * @param string $action: can be 'encrypt' or 'decrypt'
     * @param string $string: string to encrypt or decrypt
     *
     * @return bool|string
     */
    static function encrypt_decrypt($action, $string, $key) {
        $output = false;

        // initialization vector
        $iv = md5(md5($key));

        if( $action == 'encrypt' ) {
            $output = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $string, MCRYPT_MODE_ECB, $iv);
            $output = base64_encode($output);
        }
        else if( $action == 'decrypt' ){
            $output = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($string), MCRYPT_MODE_ECB, $iv);
            $output = rtrim($output, "");
        }
        return $output;
    }

    /**
     * Check email
     * @return bool
     */
    static function checkEmail($email)
    {
        $regexp = "/^[^0-9][A-z0-9._-]+([.][A-z0-9._-]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/";

        if (preg_match($regexp, $email))
            return true;
        return false;
    }

    /**
     * Get all file name in folder
     */
    static function directoryToArray($directory, $recursive)
    {
        $array_items = array();
        if ($handle = opendir($directory)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    if (is_dir($directory. "/" . $file)) {
                        if($recursive) {
                            $array_items = array_merge($array_items, directoryToArray($directory. "/" . $file, $recursive));
                        }
                        $file = $directory . "/" . $file;
                        $array_items[] = preg_replace("/\/\//si", "/", $file);
                    } else {
                        $file = $directory . "/" . $file;
                        $array_items[] = preg_replace("/\/\//si", "/", $file);
                    }
                }
            }
            closedir($handle);
        }
        return $array_items;
    }

    /**
     * Add string extension
     */
    static function addSubExt($string)
    {
        $string = str_replace('.jpg','_thumb.jpg',$string);
        $string = str_replace('.png','_thumb.png',$string);
        $string = str_replace('.gif','_thumb.gif',$string);
        $string = str_replace('.jpeg','_thumb.jpeg',$string);

        return $string;
    }

    /**
     * Count date
     */
    static function howDays($from, $to) {
        $first_date = strtotime($from);
        $second_date = strtotime($to);
        $offset = $second_date-$first_date;
        return floor($offset/60/60/24);
    }

    /**
     * Get Point
     */
    static function getPoint($attemptAnswer)
    {
        switch ($attemptAnswer){
            case 0:
                return 4;
            case 1:
                return 3;
            case 2:
                return 2;
            case 3:
                return 1;
            default :
                return 0;
        }
    }

    /**
     * Check value
     */
    static function checkDuplicateArrayValue($arr)
    {
        $result = array_unique($arr);
        if (count($result) != count($arr))
            return false;
        return true;
    }

    /**
     * Get check box
     */
    static function getCheckBox($arr, $number)
    {
        $temp = array();
        for($i=0; $i <$number; $i++)
        {
            $temp[] = isset($arr[$i])?1:0;
        }
        return $temp;
    }

    /*
     *   Convert dob to age
     */
    static function birthday ($birthday)
    {
        if ($birthday == '')
            return '';
        $arr = explode('-', $birthday);

        $years  = date('Y') - $arr[0];
        $months = date('m') - $arr[1];
        $days   = date('d') - $arr[2];

        if ($months < 0 || ($months == 0 && $days < 0))
            $years--;

        return $years;
    }

    /*
     *   Check date
     */
    static function checkDate($date)
    {
        $regex = '/^' .
            '(' .

            // Allows years 0000-9999
            '(?:[0-9]{4})' .
            ')' .
            '\-' .

            // Allows 01-12
            '(?:' .
            '(?:01)|(?:02)|(?:03)|(?:04)|(?:05)|(?:06)|(?:07)|(?:08)|(?:09)|(?:10)|' .
            '(?:11)|(?:12)' .
            ')' .
            '\-' .

            // Allows 01-31
            '(?:' .
            '(?:01)|(?:02)|(?:03)|(?:04)|(?:05)|(?:06)|(?:07)|(?:08)|(?:09)|(?:10)|' .
            '(?:11)|(?:12)|(?:13)|(?:14)|(?:15)|(?:16)|(?:17)|(?:18)|(?:19)|(?:20)|' .
            '(?:21)|(?:22)|(?:23)|(?:24)|(?:25)|(?:26)|(?:27)|(?:28)|(?:29)|(?:30)|' .
            '(?:31)' .
            ')' .

            '$/';

        if ( preg_match($regex, $date) ) {
            return true;
        }

        return false;
    }

    /*
     * Validate date
     */
    static function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /*
     *  Get current date time
     */
    static function getCurrentTime($type = 'datetime')
    {
        $date = new DateTime();
        switch ($type){
            case 'date':
                return $date->format('Y-m-d');
                break;
            case 'time':
                return $date->format('H:i:s');
                break;
            default:
                return $date->format('Y-m-d H:i:s');
        }
    }

    /*
     * Resize image without saving
     */
    static function show_image($image, $w_src, $h_src)
    {
        Header('Content-type:image/jpeg');
        // Check exist
        $img = str_replace(Yii::app()->params['site_url'], "./", $image);
        if (!file_exists($img) || $image =='')
        {
            $img = Yii::app()->params['default_image'];
        }
        /*
        list($width, $height) = getimagesize($images);
        $src=imagecreatefromjpeg($images);
        $images=imagecreatetruecolor($w_src,$h_src);
        imagecopyresized($images,$src,0,0,0,0,$w_src,$h_src,$width,$height);
        */
        list($orig_width, $orig_height) = getimagesize($img);

        $width = $orig_width;
        $height = $orig_height;

        # taller
        if ($height > $h_src) {
            $width = ($h_src / $height) * $width;
            $height = $h_src;
        }

        # wider
        if ($width > $w_src) {
            $height = ($w_src / $width) * $height;
            $width = $w_src;
        }

        $image_p = imagecreatetruecolor($width, $height);

        $img = imagecreatefromjpeg($img);

        imagecopyresampled($image_p, $img, 0, 0, 0, 0, $width, $height, $orig_width, $orig_height);
        imagejpeg($image_p,NULL,100);
        imagedestroy($src);
        imagedestroy($image_p);
    }

    /*
     * add image extension
     */
    static function addImageEx($string, $width, $height)
    {
        $string = str_replace('.jpg','-'.$width.'x'.$height.'.jpg',$string);
        $string = str_replace('.png','-'.$width.'x'.$height.'.png',$string);
        $string = str_replace('.gif','-'.$width.'x'.$height.'.gif',$string);
        $string = str_replace('.jpeg','-'.$width.'x'.$height.'.jpeg',$string);
        return $string;
    }

    /*
     * spilt content
     */
    static function splitContent($string, $width)
    {
        // Split tag
        $string = strip_tags($string);
        // Count
        $length = strlen ($string);
        $string = substr($string, 0, $width);
        if ($length > $width)
        {
            $string = $string.'...';
        }
        return $string;
    }

    /*
     * Friendly url
     */
    static function friendlyUrl ($str = '')
    {
        $str = Utilities::khongdau($str);
        $friendlyURL = htmlentities($str, ENT_COMPAT, "UTF-8", false);
        $friendlyURL = preg_replace('/&([a-z]{1,2})(?:acute|lig|grave|ring|tilde|uml|cedil|caron);/i','\1',$friendlyURL);
        $friendlyURL = html_entity_decode($friendlyURL,ENT_COMPAT, "UTF-8");
        $friendlyURL = preg_replace('/[^a-z0-9-]+/i', '-', $friendlyURL);
        $friendlyURL = preg_replace('/-+/', '-', $friendlyURL);
        $friendlyURL = trim($friendlyURL, '-');
        $friendlyURL = strtolower($friendlyURL);
        return $friendlyURL;
    }

    /*
     * Random string
     */
    static function randomString($length)
    {
        $key = '';
        $keys = array_merge(range(0,9), range('a', 'z'));
        for($i=0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $key;
    }

    /*
     * Format number
     */
    static function formatWithSuffix($input)
    {
        $suffixes = array('', 'k', 'm', 'g', 't');
        $suffixIndex = 0;

        while(abs($input) >= 1000 && $suffixIndex < sizeof($suffixes))
        {
            $suffixIndex++;
            $input /= 1000;
        }

        return (
        $input > 0
            // precision of 3 decimal places
            ? floor($input * 1000) / 1000
            : ceil($input * 1000) / 1000
        )
            . $suffixes[$suffixIndex];
    }

    /*
    * Xóa dấu (only vietnammese)
    */
    static function khongdau($str) {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        //$str = str_replace(" ", "-", str_replace("&*#39;","",$str));
        return $str;
    }

    /**
     *
     * @convert UNIX TIMESTAMP to MySQL TIMESTAMP
     *
     * @param int $timestamp
     *
     * @return string
     *
     */
    static function unixToMySQL($timestamp)
    {
        return date('Y-m-d H:i:s', $timestamp);
    }
    
    /**
     * Validate email format
     * @return boolean
     */
    static function validateEmailFormat($email)
    {
    	$validator = new CEmailValidator;
    	if ($validator->validateValue($email))
    		return true;
    	return false;
    }

    /*
     * Convert user role
     */
    static function convertUserRole($role)
    {
        switch ($role){
            case SiteConstant::USER_TYPE_SADMIN:
                return "S Admin";
            case SiteConstant::USER_TYPE_ADMIN:
                return "Admin";
            case SiteConstant::USER_TYPE_EDITOR:
                return "Editor";
            case SiteConstant::USER_TYPE_PARTNER:
                return "Partner";
            case SiteConstant::USER_TYPE_PARTNER_EDITOR:
                return "Partner Editor";
        }
    }
    
    /**
     * Get all Business Type
     */
    
    
    /**
     * Get business type
     * return selected option
     */
    static function getSelectedOptionByBusinessType($type)
    {
    	$selected = 0;
    	foreach (SiteConstant::$BUSINESS_TYPE as $key=>$value) {
    		if(strtolower($value) == strtolower($type)) {
    			$selected = $key;
    		}
    	}
    	return $selected;
    }
    
    /**
     * Get business type
     * return selected value
     */
    static function getBusinessTypeBySelectedOption($index)
    {
    	$type = '';
    	foreach (SiteConstant::$BUSINESS_TYPE as $key=>$value) {
    		if($key == $index) {
    			$type = $value;
    		}
    	}
    	return $type;
    }
    
    /*
     *  Validate lng lat
     */
    static function validateLngLat($lnglat) 
    {
    	$numericReg = '/^(d*[0-9])?((-)?\d*[0-9]((.)?\d*[0-9])?)?$/';
    	if(preg_match($numericReg, $lnglat)) {
    		return true;
    	}
    	return false;
    }

    /*
     * Mobile detect
     */
    static function isMobile()
    {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
    
    /*
     * Get slected index in array bay value
     */
    static function getIndexArrayByName($array, $name) 
    {
    	foreach ($array as $key => $value) {
    		if (strtolower($name) == strtolower($value)) {
    			return $key;
    		}
    	}
    	return 0;
    }
    
    /*
     * Validate zip code
     */
    static function validateZipCode($code)
    {
   		//$numericReg = '/^([0-9]){5}(-([0-9]){4})?$/';
    	$numericReg = '/^([0-9]){5}$/';
    	if(preg_match($numericReg, $code)) {
    		return true;
    	}
    	return false;
    }
    
    /*
     * Validate zip code
    */
    static function validatePhoneNumber($code)
    {
    	$numericReg = '/^([0-9]){10}$/';
    	if(preg_match($numericReg, $code)) {
    		return true;
    	}
    	return false;
    }
    
    /*
     * Format name, i.e John S. (John Smith)
    */
    static function formatLoyaltyUserName($firstName, $lastName) {
    	return ucfirst($firstName).' '.strtoupper(mb_substr($lastName, 0, 1, 'UTF-8')).'.';
    }
    
    /*
     * Format sex, male: M Femail: F
    */
    static function formatLoyaltyUserGender($gender) {
    	return (strtolower($gender) == 'male')? 'M' : 'F';
    }
    
    /*
     * Format notify user login denied
    */
    static function formatNotifyUserDenied($array1 , $array2, $string = '') {
    	$len = count($array1);
    	for ($i = 0; $i < $len; $i++) {
    		$string = str_replace($array1[$i], $array2[$i], $string);
    	}
    	
    	return $string;
    }
    
    /*
     * Get Date only
     */
    static function getDateOnly($date = '') {
    	$spaceIndex = strpos($date, ' ');
    	return substr($date, 0, $spaceIndex);
    }
    
    /*
     * Replace s to (s)
     */
    static function formatNamePager($string) {
    	return substr($string, 0, -1).'(s)';
    }

    /*
     * Get url in text
     */
    static function getUrlInText($string, $condition)
    {
        $arr = array();
        if(preg_match_all('/<a\s+.*?'.$condition.'\s+href=["\']([^"\']+)["\']/i', $string, $links, PREG_PATTERN_ORDER))
            $arr = array_unique($links[1]);
        return $arr;
    }

    /*
     * Get url in text
     */
    static function getUrlByCondition($string, $id)
    {
        $start = "\/tin-bai\/".$id."\/";
        $end = "bdplus";
        $matches = array();
        $regex = "/$start(.*?)$end/i";
        preg_match_all($regex, $string, $matches, PREG_GREP_INVERT);
        return $matches[1];
    }
    
    /*
     * Get url in text
     */
    static function getUrlByConditionTBD($string)
    {
        $start = "http:\/\/trollbongda.com\/item\/";
        $end = "html";
        $matches = array();
        $regex = "/$start(.*?)$end/i";
        preg_match_all($regex, $string, $matches, PREG_GREP_INVERT);
        return $matches[0];
    }

    /*
     * Get url in text
     */
    static function getUrlByConditionStartEnd($string, $start, $end)
    {
        $matches = array();
        $regex = "/$start(.*?)$end/i";
        preg_match_all($regex, $string, $matches, PREG_GREP_INVERT);
        return $matches[1];
    }


    static function getContentBetween($html, $class)
    {
        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($html);
        $finder = new DomXPath($doc);
        $node = $finder->query("//div[@class='".$class."']");
        return $doc->saveHTML($node->item(0));
    }
    
    function addImgResponsive($content){
        $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
        $document = new DOMDocument();
        libxml_use_internal_errors(true);
        $document->loadHTML($content);

        $imgs = $document->getElementsByTagName('img');
        foreach ($imgs as $img) {           
           $img->setAttribute('class','img-responsive');
        }

        $html = $document->saveHTML();
        return $html;   
}



    

    function removeImgStyle( $html ) {
   return preg_replace( '/(height|width)="\d*"\s/', "", $html );
}

    /*
     * Remove link in content
     */
    static function removeLink($string, $condition, $type = 'a')
    {

        return preg_replace('#<font size="2" face="Verdana">(.*?)</font>#i', '', $string);
        return preg_replace('{<'.$type.'\s+.'.$condition.'\s+.*?>(.*?)</'.$type.'>}si', '', $string);
    }

    /*
    * Remove link in content
    */
    static function removeByCondition($string)
    {
        return preg_replace('\<<\s+.*?<br>/i', '', $string);
    }

    /*
     * Get content
     */
    static function getContentInClass($string, $condition, $type = 'span')
    {
        $arr = array();
        $matchcount = preg_match_all('{<'.$type.'\s+.*?'.$condition.'\s*>(.*?)</'.$type.'>}si',$string, $estimates);
        if ($matchcount > 0){
            $arr = array_unique($estimates[1]);
        }
        if (isset($arr[0])){
            return $arr[0];
        }
        return '';
    }

    /*
     * Get content by xpath
     */
    static function getContentByXPath($string, $xpath)
    {
        try{
            $string = mb_convert_encoding($string, 'HTML-ENTITIES', "UTF-8");
            $dom = new DOMDocument();
            @$dom->loadHTML($string);
            $x = new DOMXPath($dom);
            libxml_use_internal_errors(false);
            libxml_get_errors(false);
            $matches = $x->query($xpath);  //DIV is just an example this could be any xPath exprn
            if (isset($matches->item(0)->nodeValue))
                return $matches->item(0)->nodeValue;
            return '';
        }catch (Exception $e){
            echo "<pre>";print_r($string);
        }

    }
    
    /*
     * Get content by xpath bongda.com.vn description
     */
    static function getContentByXPath1($string,$xpath)
    {
        try{
            $string = mb_convert_encoding($string, 'HTML-ENTITIES', "UTF-8");
            $dom = new DOMDocument();
            @$dom->loadHTML($string);
            $path = new DOMXPath($dom);        
            $textNodes = $path->query($xpath);
            $arr =array();
            foreach ($textNodes as $text)
            {
              $arr[]= "$text->nodeValue";
            }
            
            return $arr;
        }catch (Exception $e){
            echo "<pre>";print_r($string);
        }

    }

    /*
     * get src
     */
    static public function getSrcFromImg($html)
    {
        $array = array();
        preg_match( '/src="([^"]*)"/i', $html, $array ) ;

        if (isset($array[1])){
            $rs = str_replace(Yii::app()->params['site_url'], "", $array[1]);
            return $rs ;
            
        }
        return '';
    }
    
     /*
    * get src
    */
    static public function getSrcFromIframe($html)
    {
        $array = array();
        preg_match( '/src="([^"]*)"/i', $html, $array ) ;
        if (isset($array[1])){
            return $array[1];
        }
        return '';
    }
    
    /*
     * Get youtube image
     */
    static function getYoutubeImage($url)
    {
        $arr = explode('/',$url);
        $code = end($arr);
        return 'http://img.youtube.com/vi/'.$code.'/0.jpg';
    }

    /*
    * Get youtube image
    */
    static function getYoutubeCode($url)
    {
        $arr = explode('/',$url);
        return end($arr);
    }
        
    
    static public function removeImg($content)
    {
        return preg_replace('/<img(.*)>/i','',$content,1);
    }
    
    static function getCCCC($str)
    {
        $re = "/<strong>(.*?)<\\/strong>/";
        
        
        preg_match_all($re, $str, $matches);
        

        $match = $matches[1];
        
              
        return $match;
    }

    

    /*
     * Download Image
     */
    static function downloadImageFromUrl($imagepath)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch,CURLOPT_URL, $imagepath);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Googlebot/2.1 (http://www.googlebot.com/bot.html)');
        $result=curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /*
     * Time since
     * Input: string datetime
     * Output Ex: 2 giờ trước, 1 ngày trước
     */
    static function timeSince($timestamp)
    {
        $datetime1=new DateTime("now");
        $datetime2=date_create($timestamp);
        $diff=date_diff($datetime1, $datetime2);
        $timemsg='';
        if($diff->y > 0){
            $timemsg = $diff->y .' năm';

        }
        else if($diff->m > 0){
            $timemsg = $diff->m . ' tháng ';
        }
        else if($diff->d > 0){
            $timemsg = $diff->d .' ngày';
        }
        else if($diff->h > 0){
            $timemsg = $diff->h .' giờ';
        }
        else if($diff->i > 0){
            $timemsg = $diff->i .' phút';
        }
        else if($diff->s > 0){
            $timemsg = $diff->s .' giây';
        }

        $timemsg = $timemsg.' trước';
        return $timemsg;
    }

    /*
     * Replace source
     */
    static function replaceFontTag($content){
        return preg_replace('#<font (.*?)>(.*?)</font>#is', '', $content);
    }
    
            
    
    
    /*
     * Replace source
     */
    static function replaceImgTag($content){
        return preg_replace('#<div class="thumbnailsbox">(.*?)</div>#is', '', $content);
    }
    /*
     * Replace source
     */
    static function replaceImgTag1($content){
        return preg_replace('#<img class="thumb30" (.*?)>#is', '', $content);
    }
    /*
     * Replace source
     */
    static function replaceSpanTag($content){
        return preg_replace('#<span (.*?)>(.*?)</span>#is', '', $content);
    }

    /**
     * Replaces double line-breaks with paragraph elements.
     *
     * A group of regex replaces used to identify text formatted with newlines and
     * replace double line-breaks with HTML paragraph tags. The remaining
     * line-breaks after conversion become <<br />> tags, unless $br is set to '0'
     * or 'false'.
     *
     * @since 0.71
     *
     * @param string $pee The text which has to be formatted.
     * @param bool $br Optional. If set, this will convert all remaining line-breaks after paragraphing. Default true.
     * @return string Text which has been converted into correct paragraph tags.
     */
    static function wpautop($pee, $br = true) {
        $pre_tags = array();

        if ( trim($pee) === '' )
            return '';

        $pee = $pee . "\n"; // just to make things a little easier, pad the end

        if ( strpos($pee, '<pre') !== false ) {
            $pee_parts = explode( '</pre>', $pee );
            $last_pee = array_pop($pee_parts);
            $pee = '';
            $i = 0;

            foreach ( $pee_parts as $pee_part ) {
                $start = strpos($pee_part, '<pre');

                // Malformed html?
                if ( $start === false ) {
                    $pee .= $pee_part;
                    continue;
                }

                $name = "<pre wp-pre-tag-$i></pre>";
                $pre_tags[$name] = substr( $pee_part, $start ) . '</pre>';

                $pee .= substr( $pee_part, 0, $start ) . $name;
                $i++;
            }

            $pee .= $last_pee;
        }

        $pee = preg_replace('|<br />\s*<br />|', "\n\n", $pee);
        // Space things out a little
        $allblocks = '(?:table|thead|tfoot|caption|col|colgroup|tbody|tr|td|th|div|dl|dd|dt|ul|ol|li|pre|select|option|form|map|area|blockquote|address|math|style|p|h[1-6]|hr|fieldset|legend|section|article|aside|hgroup|header|footer|nav|figure|figcaption|details|menu|summary)';
        $pee = preg_replace('!(<' . $allblocks . '[^>]*>)!', "\n$1", $pee);
        $pee = preg_replace('!(</' . $allblocks . '>)!', "$1\n\n", $pee);
        $pee = str_replace(array("\r\n", "\r"), "\n", $pee); // cross-platform newlines
        if ( strpos($pee, '<object') !== false ) {
            $pee = preg_replace('|\s*<param([^>]*)>\s*|', "<param$1>", $pee); // no pee inside object/embed
            $pee = preg_replace('|\s*</embed>\s*|', '</embed>', $pee);
        }
        $pee = preg_replace("/\n\n+/", "\n\n", $pee); // take care of duplicates
        // make paragraphs, including one at the end
        $pees = preg_split('/\n\s*\n/', $pee, -1, PREG_SPLIT_NO_EMPTY);
        $pee = '';
        foreach ( $pees as $tinkle )
            $pee .= '<p>' . trim($tinkle, "\n") . "</p>\n";
        $pee = preg_replace('|<p>\s*</p>|', '', $pee); // under certain strange conditions it could create a P of entirely whitespace
        $pee = preg_replace('!<p>([^<]+)</(div|address|form)>!', "<p>$1</p></$2>", $pee);
        $pee = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)\s*</p>!', "$1", $pee); // don't pee all over a tag
        $pee = preg_replace("|<p>(<li.+?)</p>|", "$1", $pee); // problem with nested lists
        $pee = preg_replace('|<p><blockquote([^>]*)>|i', "<blockquote$1><p>", $pee);
        $pee = str_replace('</blockquote></p>', '</p></blockquote>', $pee);
        $pee = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)!', "$1", $pee);
        $pee = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*</p>!', "$1", $pee);
        if ( $br ) {
            $pee = preg_replace_callback('/<(script|style).*?<\/\\1>/s', 'self::_autop_newline_preservation_helper', $pee);
            $pee = preg_replace('|(?<!<br />)\s*\n|', "<br />\n", $pee); // optionally make line breaks
            $pee = str_replace('<WPPreserveNewline />', "\n", $pee);
        }
        $pee = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*<br />!', "$1", $pee);
        $pee = preg_replace('!<br />(\s*</?(?:p|li|div|dl|dd|dt|th|pre|td|ul|ol)[^>]*>)!', '$1', $pee);
        $pee = preg_replace( "|\n</p>$|", '</p>', $pee );

        if ( !empty($pre_tags) )
            $pee = str_replace(array_keys($pre_tags), array_values($pre_tags), $pee);

        return $pee;
    }

    /**
     * Newline preservation help function for wpautop
     *
     * @since 3.1.0
     * @access private
     * @param array $matches preg_replace_callback matches array
     * @returns string
     */
    static function _autop_newline_preservation_helper( $matches ) {
        return str_replace("\n", "<WPPreserveNewline />", $matches[0]);
    }
}

