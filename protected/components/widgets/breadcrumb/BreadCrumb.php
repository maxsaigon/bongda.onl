<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nmthanh
 * Date: 7/10/13
 * Time: 2:28 PM
 * To change this template use File | Settings | File Templates.
 */
class BreadCrumb extends CWidget {

    public $crumbs = array();
    public $delimiter = ' / ';

    public function run() {
        $this->render('breadCrumb');
    }

}