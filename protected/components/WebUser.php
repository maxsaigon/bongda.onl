<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nmthanh
 * Date: 5/28/13
 * Time: 11:16 AM
 * To change this template use File | Settings | File Templates.
 */
class WebUser extends CWebUser
{
    /**
     * Get role
     * @return Int
     */
    public function getRole()
    {
        return $this->getState("roles");
    }

    /**
     * Get column name
     * @return value of this column
     */
    public function getColumnName($name)
    {
        $user = $this->getState("user");
        if (isset($user[$name]))
        {
            return $user[$name];
        }
        return false;

    }

    public function setColumnName($name, $value)
    {
        $user = $this->getState("user");
        $user->$name = $value;
        return  $this->setState('user', $user);
    }

    /**
     *  Check is supper admin
     */
//    public function isSAdmin()
//    {
//        if ($this->getRole() == SiteConstant::USER_TYPE_SADMIN)
//            return true;
//        return false;
//    }

    /**
     *  Check is admin
     */
 //   public function isAdmin()
//    {
//        if ($this->getRole() == SiteConstant::USER_TYPE_ADMIN)
//            return true;
 //       return false;
//    }

    /**
     *  Check is editor
     */
//    public function isEditor()
//    {
//        if ($this->getRole() == SiteConstant::USER_TYPE_EDITOR)
//            return true;
//        return false;
//    }

    /**
     *  Check is partner
     */
//    public function isPartner()
//    {
//        if ($this->getRole() == SiteConstant::USER_TYPE_PARTNER)
//            return true;
 //       return false;
//    }

    /**
     *  Check is Doctor
     */
//    public function isPartnerEditor()
//    {
//        if ($this->getRole() == SiteConstant::USER_TYPE_PARTNER_EDITOR)
//            return true;
//        return false;
//    }
    /**
     * Get current User
     */
//    public function getUser()
//    {
//        $model = new BackendUsers();
//    	$user = $model->findByPk(Yii::app()->user->id);
//    	return $user;
//    }

    /*
     * Check status
     */
//    public function isPending()
//    {
//        $status = $this->getColumnName('user_status');
 //       if ($status == SiteConstant::USER_PENDING)
//            return true;
//        return false;
//    }

    /*
    * Check status
    */
//    public function isActive()
//    {
//        $status = $this->getColumnName('user_status');
//        if ($status == SiteConstant::USER_ACTIVE)
//            return true;
//        return false;
//    }
    
    /*
     * Check is admin
     */
    public function isSAdmin()
    {
        $role = $this->getColumnName('user_role');
        if ($role == SiteConstant::ADMIN_TYPE)
        {
            return true;
        }
        return false;
    }

    /*
     * Check is mod
     */
    public function isMod()
    {
        $role = $this->getColumnName('user_role');
        if ($role == SiteConstant::MOD_TYPE)
        {
            return true;
        }
        return false;
    }

    /*
     * Check is editor
     */
    public function isEditor()
    {
        $role = $this->getColumnName('user_role');
        if ($role == SiteConstant::EDITOR_TYPE)
        {
            return true;
        }
        return false;
    }

    /*
     * Check is editor
     */
    public function isUser()
    {
        $role = $this->getColumnName('user_role');
        if ($role == 0)
        {
            return true;
        }
        return false;
    }
}
