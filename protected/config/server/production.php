<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nmthanh
 * Date: 7/30/13
 * Time: 2:51 PM
 * To change this template use File | Settings | File Templates.
 */
return array(
    'name'=>'Bóng Đá Portal',
    'modules'=>array(
        // uncomment the following to enable the Gii tool
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'123456789',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1','::1'),
        ),
        'hybridauth' => array(
            'baseUrl' => 'http://bongda.onl/hybridauth',
            'withYiiUser' => false, // Set to true if using yii-user
            "providers" => array (
                "google" => array (
                    "enabled" => true,
                    "keys"    => array ( "id" => "824548719513-qqes9e1pf1bpjle5cju504f15clh6pne.apps.googleusercontent.com", "secret" => "Q3AHBmNHMaf1kYE7HsoRVk_N" ),
                    "scope" => "https://www.googleapis.com/auth/userinfo.profile ". // optional
                    "https://www.googleapis.com/auth/userinfo.email" , // optional
                    "access_type" => "offline", // optional
                    "approval_prompt" => "force", // optional
                    "hd" => "fanthethao.com", // optional,
                    "display" => "popup"
                ),

                "facebook" => array (
                    "enabled" => true,
                    "keys"    => array ( "id" => '421416641273833', "secret" => "0430a0d59c3fc2694209666fa3ac61e1" ),
                    "scope"   => "email,publish_stream",
                    "display" => "popup"
                ),
            )
        ),
    ),
    'components'=>array(
//        'cache'=>array(
//            'class'=>'system.caching.CMemCache',
//            'servers'=>array(
//                array('host'=>'127.0.0.1', 'port'=>11211, 'weight'=>60)
//            ),
//        ),
        'clientScript' => array(
            'class' => 'application.extensions.yii-EClientScript.EClientScript',
            'combineScriptFiles' => true, // By default this is set to true, set this to true if you'd like to combine the script files
            'combineCssFiles' => true, // By default this is set to true, set this to true if you'd like to combine the css files
            'optimizeScriptFiles' => false, // @since: 1.1
            'optimizeCssFiles' => false, // @since: 1.1
            'optimizeInlineScript' => false, // @since: 1.6, This may case response slower
            'optimizeInlineCss' => false, // @since: 1.6, This may case response slower
        ),
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=max_bong_da',
            'emulatePrepare' => true,
            'schemaCachingDuration' => 3600,
            'username' => 'root',
            'password' => '865784',
            'charset' => 'utf8',
        ),
        'urlManager'=>array(
            'rules' => array(
                'gii'=>'gii',
                'gii/<controller:\w+>'=>'gii/<controller>',
                'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',
            )
        ),
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType'=>'smtp',
            'transportOptions'=>array(
                'host'=>'ssl://smtp.gmail.com',
                'username'=>'',
                'password'=>'',
                'port'=>'465',
            ),
            'viewPath' => 'application.views.mail',
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        //'site_url' => 'http://portal.winebuys.local/',
        'site_url' => 'http://bongda.onl/',
        'title' => 'Bóng Đá Backend',
        'description' => 'Bóng đá',
        'news_title' => "Bóng Đá Online",
        'news_description' => "Bóng Đá Online",
        'facebookId' => '421416641273833',
        'version' => '1.03.02',
        'download_source' => '/var/www/bongda.onl/download/posts/',
        'tag_source' => '/var/www/bongda.onl/download/tags/',
        'dimention' => array(
            array('width' => 500, 'height' => 280),
            array('width' => 285, 'height' => 159),
            array('width' => 600, 'height' => 400),
            array('width' => 600, 'height' => 222),
            array('width' => 350, 'height' => 350),
            array('width' => 170, 'height' => 110),
            array('width' => 200, 'height' => 200),
        ),
        'tag_dimention' => array(
            array('width' => 200, 'height' => 200),
        ),
        'home_source' => '/var/www/bongda.onl',
    )
);