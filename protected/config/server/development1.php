<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nmthanh
 * Date: 7/30/13
 * Time: 2:51 PM
 * To change this template use File | Settings | File Templates.
 */
return array(
    'name'=>'Bóng Đá Portal',
    'modules'=>array(
        // uncomment the following to enable the Gii tool
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'123456789',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1','::1'),
        ),
        'hybridauth' => array(
            'baseUrl' => 'http://bongda.local/index.php/hybridauth',
            'withYiiUser' => false, // Set to true if using yii-user
            "providers" => array (
                "google" => array (
                    "enabled" => true,
                    "keys"    => array ( "id" => "932500763219-7h8bpv3ms7c2hl1v6hpm0jnr4da3tqfk.apps.googleusercontent.com", "secret" => "bT9nSa8JHmhxoWxj-e1J559w" ),
                    "scope" => "https://www.googleapis.com/auth/userinfo.profile ". // optional
                        "https://www.googleapis.com/auth/userinfo.email" , // optional
                    "access_type" => "offline", // optional
                    "approval_prompt" => "force", // optional
                    "hd" => "fanthethao.com", // optional,
                    "display" => "popup"
                ),

                "facebook" => array (
                    "enabled" => true,
                    "keys"    => array ( "id" => '535984786491565', "secret" => "c08de068dff57329445aa8d6fd2c48d4" ),
                    "scope"   => "email,publish_stream",
                    "display" => "popup",
                ),
            ),
        ),

    ),
    'components'=>array(
        'clientScript' => array(
            'class' => 'application.extensions.yii-EClientScript.EClientScript',
            'combineScriptFiles' => false, // By default this is set to true, set this to true if you'd like to combine the script files
            'combineCssFiles' => false, // By default this is set to true, set this to true if you'd like to combine the css files
            'optimizeScriptFiles' => false, // @since: 1.1
            'optimizeCssFiles' => false, // @since: 1.1
            'optimizeInlineScript' => false, // @since: 1.6, This may case response slower
            'optimizeInlineCss' => false, // @since: 1.6, This may case response slower
        ),
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=bongda',
            'emulatePrepare' => true,
            'schemaCachingDuration' => 3600,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ),
        'urlManager'=>array(
            'rules' => array(
                'gii'=>'gii',
                'gii/<controller:\w+>'=>'gii/<controller>',
                'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',
            )
        ),
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType'=>'smtp',
            'transportOptions'=>array(
                'host'=>'ssl://smtp.gmail.com',
                'username'=>'',
                'password'=>'',
                'port'=>'465',
            ),
            'viewPath' => 'application.views.mail',
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        //'site_url' => 'http://portal.winebuys.local/',
        'site_url' => 'http://bongda.local/',
        'title' => 'Bóng Đá Backend',
        'description' => 'Bóng đá',
        'news_title' => "Bóng Đá Online",
        'news_description' => "Bóng Đá Online",
        'facebookId' => '558985237548387',
        'version' => '1.03.02',
        'download_source' => 'C:xampp/htdocs/bongda/download/posts/',
        'tag_source' => 'C:xampp/htdocs/bongda/download/tags/',
        'default_pass' => '535984786491565',
        'dimention' => array(
            array('width' => 500, 'height' => 280),
            array('width' => 285, 'height' => 159),
            array('width' => 600, 'height' => 400),
            array('width' => 600, 'height' => 222),
            array('width' => 350, 'height' => 350),
            array('width' => 170, 'height' => 110),
            array('width' => 200, 'height' => 200),
        ),
        'tag_dimention' => array(
            array('width' => 200, 'height' => 200),
        ),
        'home_source' => 'C:/xampp/htdocs/bongda',

    )
);