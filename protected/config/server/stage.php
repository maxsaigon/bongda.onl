<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nmthanh
 * Date: 7/30/13
 * Time: 2:51 PM
 * To change this template use File | Settings | File Templates.
 */
return array(
    'name'=>'Play-it Health',
    'modules'=>array(
        // uncomment the following to enable the Gii tool
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'123456789',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1','::1'),
        )
    ),
    'components'=>array(
//        'cache'=>array(
//            'class'=>'system.caching.CMemCache',
//            'servers'=>array(
//                array('host'=>'127.0.0.1', 'port'=>11211, 'weight'=>60)
//            ),
//        ),
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=xxxxxx',
            'emulatePrepare' => true,
            'schemaCachingDuration' => 3600,
            'username' => 'xxxxx',
            'password' => 'xxxxx',
            'charset' => 'utf8',
        ),
        'urlManager'=>array(
            'urlFormat'=>'path',
            'caseSensitive'=>false,
        )
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(

    )
);