<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',

	// preloading 'log' component
	'preload'=>array('log'),

    'import'=>array(
        'application.models.*',
        'application.commands.*',
        'application.components.*',
        'application.components.constant.*',
        'application.components.helper.*',
        'application.components.services.*',
        'application.controllers.*',
        'application.modules.backend.components.services.*',
        'application.modules.backend.models.*',
        'application.modules.console.components.services.*',
        'application.modules.console.models.*',
        'application.modules.frontend.components.services.*',
        'application.modules.frontend.models.*',
        'application.modules.api.components.services.*',
        'application.modules.api.models.*',
        'ext.MyLinkPager',
        'ext.yii-mail.YiiMailMessage',
        'ext.image.*',
        'ext.filemanager.*',
        'ext.CJuiDateTimePicker.*',
        'application.components.service.*',
        'application.helpers.*',
    ),

    'modules'=>array(
        'backend' => array(
            'class'=>'application.modules.backend.BackendModule',
        ),
        'console' => array(
            'class'=>'application.modules.console.ConsoleModule',
        )
    ),
    'commandMap'=>array(
        'migrate'=>array(
            'class'=>'system.cli.commands.MigrateCommand',
            'migrationPath'=>'application.migrations',
            'migrationTable'=>'tbl_migration',
            'connectionID'=>'db',
            'templateFile'=>'application.migrations.template',
        ),
        'url' => array(
            // alias of the path where you extracted the DocsCommand.php
            'class' => 'application.modules.console.commands.UrlCommand',
        ),
    ),

	// application components
	'components'=>array(
        'urlManager'=>array(
            'urlFormat'=>'path',
            'rules'=>array(
                '<module:\w+>/<controller:\w+>/<id:\d+>'=>'<module>/<controller>/view',
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
            )
        ),
		// uncomment the following to use a MySQL database

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
        'image'=>array(
            'class'=>'ext.image.CImageComponent',
            'driver'=>'GD',
        ),
	),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(

    )
);