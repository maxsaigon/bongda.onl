<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
        'application.commands.*',
		'application.components.*',
        'application.components.constant.*',
        'application.components.helper.*',
        'application.components.services.*',
        'application.controllers.*',
        'application.modules.backend.components.services.*',
        'application.modules.backend.models.*',
        'application.modules.console.components.services.*',
        'application.modules.console.models.*',
        'application.modules.frontend.components.services.*',
        'application.modules.frontend.models.*',
        'application.modules.api.components.services.*',
        'application.modules.api.models.*',
        'ext.MyLinkPager',
        'ext.yii-mail.YiiMailMessage',
        'ext.image.*',
        'ext.filemanager.*',
        'ext.CJuiDateTimePicker.*',
        'application.components.service.*',
        'application.modules.hybridauth.controllers.*',

    ),

    'modules'=>array(
        'frontend' => array(
            'class'=>'application.modules.frontend.FrontendModule',
        ),
        'backend' => array(
            'class'=>'application.modules.backend.BackendModule',
        ),
        'console' => array(
            'class'=>'application.modules.console.ConsoleModule',
        ),
        'api' => array(
            'class'=>'application.modules.api.ApiModule',
        ),
        'hybridauth' => array(
            'class'=>'application.modules.hybridauth.HybridauthModule',
        )

    ),

	// application components
	'components'=>array(
        'session' => array(
            'timeout' => 1800,// 30 minutes
        ),
        'mobileDetect' => array(
            'class' => 'ext.MobileDetect.MobileDetect'
        ),
        'request'=>array(
            'enableCookieValidation'=>true,
        ),
        'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'rules'=>array(
                '<id:\d+>' => '/frontend/site/detail',
                '<module:\w+>/<controller:\w+>/<id:\d+>'=>'<module>/<controller>/view',
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
                
                'backend' => '/backend/post/index',
            )
        ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin' => true,
            'class'=>'WebUser',
            //'loginUrl' => array('frontend/site/index'),
            'autoRenewCookie' => true,
            'authTimeout' => 31557600,
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=> 'frontend/site/index',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
        'image'=>array(
            'class'=>'application.extensions.image.CImageComponent',
            'driver'=>'GD',
        ),
	),
);