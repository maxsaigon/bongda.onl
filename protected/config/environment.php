<?php
/**
 * This class helps you to config your Yii application
 * environment.
 * Any comments please post a message in the forum
 * Enjoy it!
 *
 * @name Environment
 * @author Thanh Nguyen
 * @version 1.0
 */

class Environment {

    const DEVELOPMENT1 = 100;
    const DEVELOPMENT2 = 150;
    const TEST        = 200;
    const STAGE       = 300;
    const PRODUCTION  = 400;

    private $_mode = 0;
    private $_debug;
    private $_trace_level;
    private $_config;
    private $_console_config;


    /**
     * Returns the debug mode
     * @return Bool
     */
    public function getDebug() {
        return $this->_debug;
    }

    /**
     * Returns the trace level for YII_TRACE_LEVEL
     * @return int
     */
    public function getTraceLevel() {
        return $this->_trace_level;
    }

    /**
     * Returns the configuration array depending on the mode
     * you choose
     * @return array
     */
    public function getConfig() {
        return $this->_config;
    }

    /**
     * Returns the configuration array depending on the mode
     * you choose
     * @return array
     */
    public function getConsoleConfig() {
        return $this->_console_config;
    }


    /**
     * Initilizes the Environment class with the given mode
     * @param constant $mode
     */
    function __construct($mode, $type = 'app') {
        $this->_mode = $mode;
        if ($type == 'app')
            $this->setConfig();
        else
            $this->setConsoleConfig();
    }

    /**
     * Sets the configuration for the choosen environment
     * @param constant $mode
     */
    private function setConfig() {
        switch($this->_mode) {
            case self::DEVELOPMENT1:
                $this->_config      = array_merge_recursive ($this->_main(), $this->_development1());
                $this->_debug       = TRUE;
                $this->_trace_level = 3;
                break;
            case self::DEVELOPMENT2:
                $this->_config      = array_merge_recursive ($this->_main(), $this->_development2());
                $this->_debug       = TRUE;
                $this->_trace_level = 3;
                break;
            case self::TEST:
                $this->_config      = array_merge_recursive ($this->_main(), $this->_test());
                $this->_debug       = FALSE;
                $this->_trace_level = 0;
                break;
            case self::STAGE:
                $this->_config      = array_merge_recursive ($this->_main(), $this->_stage());
                $this->_debug       = TRUE;
                $this->_trace_level = 0;
                break;
            case self::PRODUCTION:
                $this->_config      = array_merge_recursive ($this->_main(), $this->_production());
                $this->_debug       = FALSE;
                $this->_trace_level = 0;
                break;
            default:
                $this->_config      = $this->_main();
                $this->_debug       = TRUE;
                $this->_trace_level = 0;
                break;
        }
    }

    /**
     * Sets the configuration for the choosen environment
     * @param constant $mode
     */
    private function setConsoleConfig() {
        switch($this->_mode) {
            case self::DEVELOPMENT1:
                $this->_console_config      = array_merge_recursive ($this->_console(), $this->_development1());
                break;
            case self::DEVELOPMENT2:
                $this->_console_config      = array_merge_recursive ($this->_console(), $this->_development2());
                break;
            case self::TEST:
                $this->_console_config      = array_merge_recursive ($this->_console(), $this->_test());
                break;
            case self::STAGE:
                $this->_console_config      = array_merge_recursive ($this->_console(), $this->_stage());
                break;
            case self::PRODUCTION:
                $this->_console_config      = array_merge_recursive ($this->_console(), $this->_production());
                break;
            default:
                $this->_console_config      = $this->_console();
                break;
        }
    }


    /**
     * Main configuration
     * This is the general configuration that uses all environments
     */
    private function _main() {
        return require 'main.php';
    }

    /**
     * Main configuration
     * This is the general configuration that uses all environments
     */
    private function _console() {
        return require 'console.php';
    }


    /**
     * Development configuration
     * Usage:
     * - Local website
     * - Local DB
     * - Show all details on each error.
     * - Gii module enabled
     */
    private function _development1 () {

        return require 'server/development1.php';
    }

    /**
     * Development configuration
     * Usage:
     * - Local website
     * - Local DB
     * - Show all details on each error.
     * - Gii module enabled
     */
    private function _development2 () {

        return require 'server/development2.php';
    }


    /**
     * Test configuration
     * Usage:
     * - Local website
     * - Local DB
     * - Standard production error pages (404,500, etc.)
     * @var array
     */
    private function _test() {
        return require 'server/test.php';
    }

    /**
     * Stage configuration
     * Usage:
     * - Online website
     * - Production DB
     * - All details on error
     */
    private function _stage() {
        return require 'server/stage.php';
    }

    /**
     * Production configuration
     * Usage:
     * - online website
     * - Production DB
     * - Standard production error pages (404,500, etc.)
     */
    private function _production() {
        return require 'server/production.php';
    }
}// END Environment Class