<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/16/14
 * Time: 12:17 AM
 */

class m140415_030303_tag_convert extends CDbMigration {
    public function up()
    {
        //add is_deleted to qa_posts
        $sql = "
        CREATE TABLE IF NOT EXISTS `tag_convert` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `tag_source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `tag_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
        ";
        $this->execute($sql);
    }
} 