<?php

class m140808_150000_rm_duplicate extends CDbMigration
{
    public function up(){

        $sql = "
 
            ALTER IGNORE TABLE url ADD UNIQUE INDEX dupidx (tile);

        ";
        $this->execute($sql);
    }
} 