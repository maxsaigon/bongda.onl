<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/16/14
 * Time: 12:07 AM
 */

class m140415_020202_hot extends CDbMigration {
    public function up()
    {
        //add is_deleted to qa_posts
        $sql = "ALTER TABLE url ADD is_hot tinyint(1) DEFAULT NULL AFTER created;";
        $this->execute($sql);
    }
} 