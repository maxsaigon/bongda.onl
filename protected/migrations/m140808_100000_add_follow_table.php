<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 3/22/14
 * Time: 11:22 AM
 */

class m140808_100000_add_follow_table extends CDbMigration
{
    public function up(){

        $sql = "
            CREATE TABLE IF NOT EXISTS `users_follow` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `user_id` int(11) NOT NULL,
              `tag_id` int(11) NOT NULL,
              `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
              `created` datetime DEFAULT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
        ";
        $this->execute($sql);
    }
} 