<?php
class m140724_150000_add_rememberme extends CDbMigration
{
    public function up()
    {
        // Add more filed
        $sql = "ALTER TABLE users ADD rememberMe INT(10) DEFAULT 0;";
        $this->execute($sql);
    }
}

?>