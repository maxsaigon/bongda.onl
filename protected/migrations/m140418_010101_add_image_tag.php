<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/18/14
 * Time: 12:48 AM
 */

class m140418_010101_add_image_tag extends CDbMigration {
    public function up()
    {
        //add is_deleted to qa_posts
        $sql = "ALTER TABLE tag_convert ADD tag_image varchar(500) DEFAULT NULL AFTER tag_des;";
        $this->execute($sql);

        $sql = "update tag_convert set tag_image='images/tags/01.jpg'";
        $this->execute($sql);
    }
} 