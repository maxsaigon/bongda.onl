<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 5/18/14
 * Time: 10:11 PM
 */

class m140518_020202_user_profile extends CDbMigration {
    public function up(){
        $sql = "
            CREATE TABLE IF NOT EXISTS `users` (
              `user_id` int(11) NOT NULL AUTO_INCREMENT,
              `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `password_salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `object_sha` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'to check if hybridauth user profile object has changed from last time, if yes we update the user profile here ',
              `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `profileurl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `websiteurl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `photourl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `displayname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `age` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `birthday` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `birthmonth` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `birthyear` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `emailverified` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `region` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              PRIMARY KEY (`user_id`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=139 ;
        ";
        $this->execute($sql);
    }
} 