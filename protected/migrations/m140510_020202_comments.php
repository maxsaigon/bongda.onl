<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 5/10/14
 * Time: 2:34 PM
 */

class m140510_020202_comments extends CDbMigration {
    public function up(){
        $sql = "
            CREATE TABLE IF NOT EXISTS `comments` (
              `comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
              `comment_parent` int(11) NOT NULL,
              `comment_content` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
              `comment_user_id` int(11) NOT NULL,
              `comment_like` int(11) NOT NULL,
              `comment_dislike` int(11) NOT NULL,
              `comment_created` datetime DEFAULT NULL,
              `comment_updated` datetime DEFAULT NULL,
              PRIMARY KEY (`comment_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
        ";
        $this->execute($sql);

        $sql = "
            CREATE TABLE IF NOT EXISTS `comment_like` (
              `like_id` bigint(20) NOT NULL AUTO_INCREMENT,
              `user_id` int(11) NOT NULL,
              `comment_id`  bigint(20) NOT NULL,
              `status` int(1) NOT NULL,
              `like_created` datetime DEFAULT NULL,
              PRIMARY KEY (`like_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
        ";
        $this->execute($sql);
    }
} 