<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 5/3/14
 * Time: 1:12 AM
 */

class m140503_020202_tag extends CDbMigration {
    public function up()
    {
        //add is_deleted to qa_posts
        $sql = "ALTER TABLE tag ADD tag_image varchar(500) DEFAULT NULL AFTER Tag;";
        $this->execute($sql);

        $sql = "ALTER TABLE tag ADD tag_order INT DEFAULT 0 AFTER tag_image;";
        $this->execute($sql);

        $sql = "update tag set tag_image='images/tags/01.jpg'";
        $this->execute($sql);
    }
} 