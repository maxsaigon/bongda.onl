<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 3/22/14
 * Time: 10:48 AM
 */

class m140322_090909_init extends CDbMigration {
    public function up(){
        $sql = "
            CREATE TABLE IF NOT EXISTS `Site` (
              `Id` bigint(20) NOT NULL AUTO_INCREMENT,
              `SiteName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
              `Url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
              `IsActived` tinyint(1) NOT NULL,
              `Interval` int(11) NOT NULL COMMENT 'interval to run schedule',
              PRIMARY KEY (`Id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
        ";
        $this->execute($sql);
    }
} 