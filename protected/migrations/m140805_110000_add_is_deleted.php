<?php
class m140805_110000_add_is_deleted extends CDbMigration {
    public function up()
    {
        //add is_deleted to qa_posts
        $sql = "ALTER TABLE url ADD is_deleted int(11) DEFAULT 0 ;";
        $this->execute($sql);
    }
}