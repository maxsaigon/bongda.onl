<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 4/25/14
 * Time: 12:48 AM
 */

class m140424_090909_review extends CDbMigration {
    public function up()
    {
        //add is_deleted to qa_posts
        $sql = "ALTER TABLE url ADD is_reviewed tinyint(1) DEFAULT 0 AFTER is_hot;";
        $this->execute($sql);
    }
}