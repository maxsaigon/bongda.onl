<?php
/**
 * Created by PhpStorm.
 * User: Thanhf.NguyeenX
 * Date: 3/22/14
 * Time: 11:22 AM
 */

class m140322_092222_update_category extends CDbMigration
{
    public function up(){
        $sql = "
            CREATE TABLE IF NOT EXISTS `category` (
              `Id` bigint(20) NOT NULL AUTO_INCREMENT,
              `SiteId` bigint(20) NOT NULL,
              `Title` longtext COLLATE utf8_unicode_ci NOT NULL,
              `DateCreated` varchar(255) COLLATE utf8_unicode_ci NOT NULL
              PRIMARY KEY (`Id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
        ";
        $this->execute($sql);

        $sql = "
            CREATE TABLE IF NOT EXISTS `url` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `site_id` int(11) NOT NULL,
              `tile` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
              `content` text COLLATE utf8_unicode_ci NOT NULL,
              `feature_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              `created` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
        ";
        $this->execute($sql);
    }
} 