<?php
class m140731_090000_add_post_type extends CDbMigration {
    public function up()
    {
        //add is_deleted to qa_posts
        $sql = "ALTER TABLE url ADD type varchar(500) DEFAULT 'news' AFTER tile;";
        $this->execute($sql);

        $sql = "update url set type='news'";
        $this->execute($sql);
    }
} 