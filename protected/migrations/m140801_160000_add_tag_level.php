<?php
class m140801_160000_add_tag_level extends CDbMigration {
    public function up()
    {
        //add is_deleted to qa_posts
        $sql = "ALTER TABLE tag ADD tag_level int(11) DEFAULT 3 AFTER tag_order;";
        $this->execute($sql);
    }
}