/**
 * Created with JetBrains PhpStorm.
 * User: nmthanh
 * Date: 11/27/13
 * Time: 9:25 AM
 * To change this template use File | Settings | File Templates.
 */
var global = function(){};
global.prototype.constructor = function()
{
    var that = this;
    // hide content until customSelect already
    $('.loading').css('visibility', 'hidden');
    
    $(document).ready(function(){
        that.login();
        that.register();
    	// fix IE button inside a tag
    	$("a > button").bind('click', function() { 
    	    location.href = $(this).closest("a").attr("href");
    	});
    	// website
    	$('input[placeholder=Website]').keydown(function(e) {
    	    var oldvalue = $(this).val();
    	    var field = this;
    	    setTimeout(function () {
    	        if(field.value.indexOf('http://') !== 0) {
    	        	if(oldvalue.indexOf('http://') !== 0){
    	        		$(field).val('http://' + field.value);
    	        	}else {
    	        		$(field).val(oldvalue);
    	        	}
    	        } 
    	    }, 1);
    	});
    });
    $(window).load(function(){
    	// show content if customSelect already
    	$('.loading').css('visibility', 'visible');
    	//make version always bottom
    	if($('body').height() == $(document).height()) {
    		$('html, body').css('height', '100%');
        	$('body').css({'position': 'relative', 'width': '100%'});
        	$('.version').css({'position': 'absolute', 'bottom': '10px'});
    	}
    });
}

global.prototype.notify = function(type, message)
{
    if (type){
        jSuccess(message+' !!!',{
            autoHide : true, // added in v2.0
            TimeShown : 2000,
            HorizontalPosition : 'center',
            VerticalPosition: 'center'
        });
    }else{
        jError(message+' !!!',{
            autoHide : true, // added in v2.0
            TimeShown : 2000,
            HorizontalPosition : 'center',
            VerticalPosition: 'center'
        });
    }
}


global.prototype.login = function()
{
    var that = this;
    $("#loginform").validate({
        rules: {
            user: {
                required: true
            },
            pass: {
                required: true,
                minlength: 6
            }
        },
        messages: {
            user: {
                required: "Vui lòng nhập tên đăng nhập"
            },
            pass: {
                required: "Vui lòng nhập mật khẩu",
                minlength: "Mật khẩu ít nhất 6 kí tự "
            }
        },
        submitHandler: function(form) {
            $('.modal-body').showLoading();
            var url = $('#loginform').attr('data-action');
            var data = $(form).serialize();
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType: "json",
                success: function(data){
                    if (data.status != true)
                    {
                        that.notify(data.status, data.msg);
                    }else{
                        window.location.href = data.msg;
                    }
                    return false
                },error: function(error){
                    that.notify(false, 'Lỗi kết nối');
                    return false;
                }
            });
            $('.modal-body').hideLoading();
            return false;
        }
    });

    $('#open-register, #open-forgot').click(function(e){
        $('button.close').click();
        e.preventDefault();
    });
}

global.prototype.register = function()
{
    // Action to LoginController -> register action
    var that = this;
    $("#registerform").validate({
        rules: {
            name:{
                required: true
            },
            pass: {
                required: true,
                minlength: 6
            },
            'repeat-pass':{
                equalTo : "#password-input"
            },
            user_email: {
                required: true,
                email   : true,
                remote: {
                    url: VALIDATE_EMAI_URL,
                    type: "post",
                    data: {
                        email: function() {
                            return $("#email-input").val();
                        }
                    }
                }
            }
        },
        messages: {
            name:{
                required: "Vui lòng nhập tên hiển thị"
            },
            pass: {
                required: "Vui lòng nhập mật khẩu",
                minlength: "Mật khẩu ít nhất 6 kí tự"
            },
            'repeat-pass':{
                equalTo : "Mật khẩu lặp lại phải trùng với mật khẩu ban đầu"
            },
            user_email: {
                required: "Vui lòng nhập email",
                email:"Email không hợp lệ",
                remote: "Email này đã tồn tại trong hệ thống"
            }
        }
    });
}

global.prototype.validateEmail = function (email)
{
    var isEmail_re       = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
    return String(s).search (email) != -1;
}

global.prototype.addExtent = function(url, width, height)
{
    url = url.replace('.jpg','-'+width+'x'+height+'.jpg');
    url = url.replace('.gif','-'+width+'x'+height+'.gif');
    url = url.replace('.png','-'+width+'x'+height+'.png');
    url = url.replace('.jpeg','-'+width+'x'+height+'.jpeg');
    return url;
}
