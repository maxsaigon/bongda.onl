// add method validate for zip code
$.validator.addMethod("zipcode",function (value, element){
	// Reg allow number + (-) char
	var numericReg = /^([0-9\-]+)$/;
	// Reg format Zip code
	//var numericRegFormat = /^([0-9]){5}(-([0-9]){4})?$/;
	var numericRegFormat = /^([0-9]){5}$/;
	if(numericReg.test(value)) {
		return numericRegFormat.test(value);
	}
	// replace character if not number/-
	value = value.replace(/\D/g, '');
	$('#'+element.id).val(value);
	// replace character in case PASTE
	if(!(numericReg.test(value))) {
		$('#'+element.id).val(value.replace(/\D/g, ''));
	}
	
	return numericRegFormat.test(value);
});

// add method validate for limit number input
jQuery.validator.addMethod("numberonly", function(value, element) {
	var numericReg = /^[0-9]+$/i;
	if(numericReg.test(value)) {
		return true;
	}
	// replace last character if not number
	value = value.substring(0,value.length - 1);
	$('#'+element.id).val(value.replace(/\D/g, ''));
	
	return numericReg.test(value);
}); 

//add method validate for website as www.website.com
jQuery.validator.addMethod("website", function(value, element) {
	var numericReg = /^((http:\/\/w{3}\.[0-9a-zA-Z-.]+\.[0-9a-zA-Z]+)?(http:\/\/(?!www)[0-9a-zA-Z-.]+\.[0-9a-zA-Z-.]+\.[0-9a-zA-Z]+)?(http:\/\/w{3}\.[0-9a-zA-Z-.]+\.[0-9a-zA-Z]+\/[0-9a-zA-Z-]+)?)?$/i;
	return numericReg.test(value);
});
