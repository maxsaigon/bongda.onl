(function ($) {
  $.extend($.summernote.lang, {
    'de-DE': {
      font: {
        bold: 'In đậm',
        italic: 'Chữ nghiêng',
        underline: 'Gạch chân',
        strike: 'Durchgestrichen',
        clear: 'Zurücksetzen',
        height: 'Chiều cao',
        size: 'Kích thước'
      },
      image: {
        image: 'Ảnh',
        insert: 'Chèn vào',
        resizeFull: 'Kích thước gốc',
        resizeHalf: '1/2',
        resizeQuarter: '1/4',
        floatLeft: 'Canh bên trái',
        floatRight: 'Canh bên phải',
        floatNone: 'Kein Textfluss',
        selectFromFiles: 'Chọn file có sẵn',
        url: 'URL'
      },
      link: {
        link: 'Đường dẫn',
        insert: 'Chèn vào',
        unlink: 'Hủy',
        edit: 'Chỉnh sửa'
      }

    }
  });
})(jQuery);
